package bolt

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strconv"

	deep "github.com/patrikeh/go-deep"
	"github.com/patrikeh/go-deep/training"
)

func getDataset(path string, readlength int) training.Examples {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*10000)

	var count int
	var Predata training.Examples
	for {

		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		// pt := stringToPrintTrain(string(line))
		text := string(line)
		pt := tsvtoPrintTrain(&text)
		var target float64
		if pt.MLTrainPositive {
			target = 1
		}

		dataset := parseToTrainFloat64(string(line), readlength, "DUP")
		targetA := []float64{target}

		exi := training.Example{}
		exi.Input = dataset
		exi.Response = targetA
		// fmt.Println(exi)
		Predata = append(Predata, exi)
		count++

	}
	return Predata
}

func startML() {
	fmt.Println("DUP")
	// executeMLtask([]int{6, 6, 1})
	// executeMLtask([]int{8, 8, 1})
	// executeMLtask([]int{16, 16, 1})
	executeMLtask([]int{12, 12, 1})
	// executeMLtask([]int{14, 14, 1})
	// executeMLtask([]int{16, 16, 1})
}

func executeMLtask(layout []int) {
	fmt.Println("+++++ Start task ", layout, " +++++")
	falseData := getDataset("/data/users/wichadak/kan/golibrary/src/github.com/sakkayaphab/trainingdata/dup/false", 101)
	trueData := getDataset("/data/users/wichadak/kan/golibrary/src/github.com/sakkayaphab/trainingdata/dup/true", 101)

	// falseData := getDataset("/data/users/wichadak/kan/golibrary/src/github.com/sakkayaphab/trainingdata/del/false", 101)
	// trueData := getDataset("/data/users/wichadak/kan/golibrary/src/github.com/sakkayaphab/trainingdata/del/true", 101)

	// falseData := getDataset("/data/users/wichadak/kan/golibrary/src/github.com/sakkayaphab/trainingdata/inv/invfalse", 101)
	// trueData := getDataset("/data/users/wichadak/kan/golibrary/src/github.com/sakkayaphab/trainingdata/inv/invtrue", 101)

	// falseData := getDataset("/data/users/wichadak/kan/originaldataset/trainins/false", 101)
	// trueData := getDataset("/data/users/wichadak/kan/originaldataset/trainins/true", 101)

	// falseData := getDataset("/data/users/wichadak/kan/originaldataset/traintra/false", 101)
	// trueData := getDataset("/data/users/wichadak/kan/originaldataset/traintra/true", 101)

	fmt.Println(">>", len(falseData))
	sFalseData := splitData(5, falseData)
	fmt.Println(">>", len(trueData))
	sTrueData := splitData(5, trueData)
	for i := 0; i < len(sFalseData); i++ {
		fmt.Println("------------------", i, "------------------")
		fmt.Println("F:", len(sFalseData[i]), "T:", len(sTrueData[i]))
		var dataTest training.Examples
		dataTest = append(dataTest, sFalseData[i]...)
		dataTest = append(dataTest, sTrueData[i]...)

		var dataTrain training.Examples
		for j := 0; j < len(sFalseData); j++ {
			if j != i {
				fmt.Println("Merge F:", len(sFalseData[i]), "T:", len(sTrueData[i]))
				dataTrain = append(dataTrain, sFalseData[j]...)
				dataTrain = append(dataTrain, sTrueData[j]...)
			}
		}

		model := buildModel(dataTrain, "/data/users/wichadak/kan/nn/"+strconv.Itoa(i), layout)
		dataTrain = nil

		testModel(dataTest, model)

		// testModelFromFile("/data/users/wichadak/kan/training/verifydel/20xdel", model, "20xdel", 125)
		// testModelFromFile("/data/users/wichadak/kan/training/verifydel/50xdel", model, "50xdel", 125)

		// testModelFromFile("/data/users/wichadak/kan/training/verifyinv/20xinv", model, "20xinv", 125)
		// testModelFromFile("/data/users/wichadak/kan/training/verifyinv/50xinv", model, "50xinv", 125)

		// testModelFromFile("/data/users/wichadak/kan/training/verifydup/20xdup", model, "20xdup", 125)
		// testModelFromFile("/data/users/wichadak/kan/training/verifydup/50xdup", model, "50xdup", 125)
		// testModelFromFile("/data/users/wichadak/kan/training/verifydup/100xdup", model, "100xdup", 125)
		fmt.Println("#############################")
	}

	return
}

func testModelFromFile(filepath string, dat []byte, name string, readlength int) {
	odd := getDataset(filepath, readlength)
	// var odd training.Examples
	neural, err := deep.Unmarshal(dat)
	if err != nil {
		panic("cannot load model snv")
	}

	matchAll := len(odd)
	// allFalse := 0
	matchTrue1 := 0
	matchTrue2 := 0
	matchFalse := 0
	matchFalse2 := 0

	for _, data := range odd {
		// allFalse++
		if data.Response[0] == 1 {
			a := neural.Predict(data.Input)
			if a[0] >= 0.5 {
				matchTrue1++
			} else {
				matchFalse++
				// fmt.Println(data[i], a)
			}
		} else {
			a := neural.Predict(data.Input)
			if a[0] < 0.5 {
				matchTrue2++
			} else {
				matchFalse2++
				// fmt.Println(data[i], a)
			}
		}
	}
	fmt.Println(name+" = Total variants:", matchAll, "true correct:", matchTrue1, "true incorrect:", matchFalse, "||", matchTrue1, "false corrent:", matchTrue2, "false incorrect:", matchFalse2,
		"Precision true:", float64(matchTrue1)/float64(matchTrue1+matchFalse), "Precision false:", float64(matchTrue2)/float64(matchFalse2+matchTrue2), "Validation:", float64(matchTrue1+matchTrue2)/float64(matchFalse2+matchTrue2+matchTrue1+matchFalse),
	)
}

func testModel(odd training.Examples, dat []byte) {
	neural, err := deep.Unmarshal(dat)
	if err != nil {
		panic("cannot load model snv")
	}

	matchAll := len(odd)
	// allFalse := 0
	matchTrue1 := 0
	matchTrue2 := 0
	matchFalse := 0
	matchFalse2 := 0

	for _, data := range odd {
		// allFalse++
		if data.Response[0] == 1 {
			a := neural.Predict(data.Input)
			if a[0] >= 0.5 {
				matchTrue1++
			} else {
				matchFalse++
				// fmt.Println(data[i], a)
			}
		} else {
			a := neural.Predict(data.Input)
			if a[0] < 0.5 {
				matchTrue2++
			} else {
				matchFalse2++
				// fmt.Println(data[i], a)
			}
		}
	}

	fmt.Println("Total variants:", matchAll, "true correct:", matchTrue1, "true incorrect:", matchFalse, "||", matchTrue1, "false corrent:", matchTrue2, "false incorrect:", matchFalse2,
		"Precision true:", float64(matchTrue1)/float64(matchTrue1+matchFalse), "Precision false:", float64(matchTrue2)/float64(matchFalse2+matchTrue2), "Validation:", float64(matchTrue1+matchTrue2)/float64(matchFalse2+matchTrue2+matchTrue1+matchFalse),
	)

}

func buildModel(data training.Examples, path string, layout []int) []byte {
	data.Shuffle()

	var neural *deep.Neural

	neural = deep.NewNeural(&deep.Config{
		Inputs:     len(data[0].Input),
		Layout:     layout,
		Activation: deep.ActivationSigmoid,
		Mode:       deep.ModeBinary,
		Bias:       true,
	})
	fmt.Println(layout)
	iteration := 3000
	fmt.Println("iteration:", iteration)
	trainer := training.NewBatchTrainer(training.NewAdam(0.05, 0.4, 0.4, 0), 100, len(data), 5000)
	trainer.Train(neural, data, data, iteration)

	a, err := neural.Marshal()
	if err != nil {
		panic(err)
	}

	fmt.Println(string(a))

	f, err := os.OpenFile(path, os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	err = ioutil.WriteFile(path, a, 0666)
	if err != nil {
		panic(err)
	}

	return a
}

func splitData(split int, datas training.Examples) []training.Examples {
	divided := len(datas) / split
	var count int
	var tempData training.Examples
	var allData []training.Examples
	for _, data := range datas {
		tempData = append(tempData, data)
		count++
		if count > divided {
			allData = append(allData, tempData)
			tempData = nil
			count = 0
		}

		if count == divided {
			allData = append(allData, tempData)
			tempData = nil
			count = 0
		}
	}

	return allData
}
