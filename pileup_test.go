package bolt

import (
	"testing"
)

func TestTSVtoString(t *testing.T) {
	text := printTrain{}
	text.SVType = "DEL"
	text.Orientation = "L"
	text.Chr = "chr1"
	text.Pos = 10
	text.ChrEnd = "chr2"
	text.End = 20
	// mapq := []int{60, 60, 60}
	// text.M = mapq

	tsv := ptsToTSV(&text)
	ob := tsvtoPrintTrain(&tsv)

	if ob.Chr != text.Chr {
		t.Errorf("TSV error")
	}

	if ob.Pos != text.Pos {
		t.Errorf("TSV error")
	}

	if ob.End != text.End {
		t.Errorf("TSV error")
	}

	// if len(ob.MapQRP) != len(text.MapQRP) {
	// 	t.Errorf("TSV error")
	// }
}

func TestIsInLimitRange(t *testing.T) {
	tables := []struct {
		Pos      int
		End      int
		Expected bool
	}{
		{10000, 10, true},
		{0, 1000, true},
		{1000, 10000, true},
		{0, 300000, true},
		{300000, 0, true},
		{0, 300001, false},
		{300001, 0, false},
		{0, 1000000, false},
		{1000000, 0, false},
	}

	for _, table := range tables {
		total := isInLimitRange(table.Pos, table.End)
		// fmt.Println(total)
		if total != table.Expected {
			t.Errorf("reciprocal validation was incorrect, Value: %d, Expected: %d", table.Pos, table.End)
		}
	}
}
