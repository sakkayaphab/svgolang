package bolt

import (
	"bytes"
	"encoding/json"
	"strconv"
	"strings"
)

func printPileUp(evidence *evidentCollector, x *breakPointState, jbs *jobsSplitter, task *Task) printTrain {

	switch x.SVType {
	case "DEL":
		return pileupDEL(evidence, x, jbs, task)
	case "DUP":
		return pileupDUP(evidence, x, jbs, task)
	case "INV":
		return pileupINV(evidence, x, jbs, task)
	}
	panic("pileup empty pt")
}

func isInLimitRange(x, y int) bool {

	if abs(x-y) > 300000 {
		return false
	} else {
		return true
	}
}

func pileupDEL(evidence *evidentCollector, x *breakPointState, jbs *jobsSplitter, task *Task) printTrain {
	var pt printTrain

	switch x.Orientation {
	case "F":

		rCapture, rUncapture := getReadPileupPoint(evidence.Read, x.orderhit, x.POS)

		pt.SVType = "DEL"
		pt.Orientation = "F"
		pt.Chr = evidence.CHR
		pt.Pos = x.POS
		pt.ChrEnd = evidence.CHR
		pt.End = x.END
		pt.Hit = x.HIT
		pt.LongMaxHit = getLonggestHit(x)
		pt.LongMinHit = getMinLongHit(x)
		pt.LongAvgHit = getAVGLongHit(x)
		pt.MaxScHit = getmaxScHit(x)
		pt.MinScHit = getMinScHit(x)
		pt.AvgScHit = getAvgScHit(x)
		pt.SumScOppSide = getSumScSide(rUncapture, "F")
		pt.ScMaxOppSide = getScMaxSide(rUncapture, "F")
		pt.ScMinOppSide = getScMinSide(rUncapture, "F")
		pt.ScAvgOppSide = getAVGScSide(rUncapture, "F")
		pt.SumSCOverPointOppSide = getSumSCOverPoint(rUncapture, "L", x.POS)
		pt.SumScSameSide = getSumScSide(rUncapture, "L")
		pt.ScMaxSameSide = getScMaxSide(rUncapture, "L")
		pt.ScMinSameSide = getScMinSide(rUncapture, "L")
		pt.ScAvgSameSide = getAVGScSide(rUncapture, "L")
		pt.SumSCOverPointSameSide = getSumSCOverPoint(rUncapture, "F", x.POS)
		pt.ReadDepth = getDepthRead(rCapture, rUncapture)
		pt.ReadDepthNextPos = getReadDepthwithoutSC(evidence.Read, x.orderhit, x.POS+1)
		pt.ReadDepthPreviousPos = getReadDepthwithoutSC(evidence.Read, x.orderhit, x.POS-1)
		pt.CaptureMapQMax = getMapQMax(rCapture)
		pt.CaptureMapQMin = getMapQMin(rCapture)
		pt.CaptureMapQAvg = int(getMapQAvg(rCapture))
		pt.UncaptureMapQMax = getMapQMax(rUncapture)
		pt.UncaptureMapQMin = getMapQMin(rUncapture)
		pt.UncaptureMapQAvg = int(getMapQAvg(rUncapture))
		// only sc
		rUncaptureSC := getOnlySCRead(rUncapture)
		pt.UncaptureSCMapQMax = getMapQMax(rUncaptureSC)
		pt.UncaptureSCMapQMin = getMapQMin(rUncaptureSC)
		pt.UncaptureSCMapQAvg = int(getMapQAvg(rUncaptureSC))
		pt.ReadPairCount = x.ReadPairCount
		pt.MatchOrientation = getMatchOrientation(x)
		pt.MaxMapQRP = getMaxIntArray(evidence.MapQRP)
		pt.AvgMapQRP = getAvgIntArray(evidence.MapQRP)
		pt.MinMapQRP = getMinIntArray(evidence.MapQRP)
		return pt
		// printTrainData(pt, task.OutputPath+temppath+"del/"+jbs.CHROM)
	case "L":
		rCapture, rUncapture := getReadPileupPoint(evidence.Read, x.orderhit, x.END)

		pt.SVType = "DEL"
		pt.Orientation = "L"
		pt.Chr = evidence.CHR
		pt.Pos = x.POS
		pt.ChrEnd = evidence.CHR
		pt.End = x.END
		pt.Hit = x.HIT
		pt.LongMaxHit = getLonggestHit(x)
		pt.LongMinHit = getMinLongHit(x)
		pt.LongAvgHit = getAVGLongHit(x)
		pt.MaxScHit = getmaxScHit(x)
		pt.MinScHit = getMinScHit(x)
		pt.AvgScHit = getAvgScHit(x)
		pt.SumScOppSide = getSumScSide(rUncapture, "L")
		pt.ScMaxOppSide = getScMaxSide(rUncapture, "L")
		pt.ScMinOppSide = getScMinSide(rUncapture, "L")
		pt.ScAvgOppSide = getAVGScSide(rUncapture, "L")
		pt.SumSCOverPointOppSide = getSumSCOverPoint(rUncapture, "F", x.END)
		pt.SumScSameSide = getSumScSide(rUncapture, "F")
		pt.ScMaxSameSide = getScMaxSide(rUncapture, "F")
		pt.ScMinSameSide = getScMinSide(rUncapture, "F")
		pt.ScAvgSameSide = getAVGScSide(rUncapture, "F")
		pt.SumSCOverPointSameSide = getSumSCOverPoint(rUncapture, "L", x.END)
		pt.ReadDepth = getDepthRead(rCapture, rUncapture)
		pt.ReadDepthNextPos = getReadDepthwithoutSC(evidence.Read, x.orderhit, x.END+1)
		pt.ReadDepthPreviousPos = getReadDepthwithoutSC(evidence.Read, x.orderhit, x.END-1)
		pt.CaptureMapQMax = getMapQMax(rCapture)
		pt.CaptureMapQMin = getMapQMin(rCapture)
		pt.CaptureMapQAvg = int(getMapQAvg(rCapture))
		pt.UncaptureMapQMax = getMapQMax(rUncapture)
		pt.UncaptureMapQMin = getMapQMin(rUncapture)
		pt.UncaptureMapQAvg = int(getMapQAvg(rUncapture))
		// only sc
		rUncaptureSC := getOnlySCRead(rUncapture)
		pt.UncaptureSCMapQMax = getMapQMax(rUncaptureSC)
		pt.UncaptureSCMapQMin = getMapQMin(rUncaptureSC)
		pt.UncaptureSCMapQAvg = int(getMapQAvg(rUncaptureSC))
		pt.ReadPairCount = x.ReadPairCount
		pt.MatchOrientation = getMatchOrientation(x)
		// printTrainData(pt, task.OutputPath+temppath+"del/"+jbs.CHROM)
		pt.MaxMapQRP = getMaxIntArray(evidence.MapQRP)
		pt.MinMapQRP = getMinIntArray(evidence.MapQRP)
		pt.AvgMapQRP = getAvgIntArray(evidence.MapQRP)
		return pt
	}

	panic("pileup empty pt")

}

func pileupDUP(evidence *evidentCollector, x *breakPointState, jbs *jobsSplitter, task *Task) printTrain {
	switch x.Orientation {
	case "F":
		rCapture, rUncapture := getReadPileupPoint(evidence.Read, x.orderhit, x.POS)

		var pt printTrain
		pt.SVType = "DUP"
		pt.Orientation = "F"
		pt.Chr = evidence.CHR
		pt.Pos = x.POS
		pt.ChrEnd = evidence.CHR
		pt.End = x.END
		pt.Hit = x.HIT
		pt.LongMaxHit = getLonggestHit(x)
		pt.LongMinHit = getMinLongHit(x)
		pt.LongAvgHit = getAVGLongHit(x)
		pt.MaxScHit = getmaxScHit(x)
		pt.MinScHit = getMinScHit(x)
		pt.AvgScHit = getAvgScHit(x)
		pt.SumScOppSide = getSumScSide(rUncapture, "L")
		pt.ScMaxOppSide = getScMaxSide(rUncapture, "L")
		pt.ScMinOppSide = getScMinSide(rUncapture, "L")
		pt.ScAvgOppSide = getAVGScSide(rUncapture, "L")
		pt.SumSCOverPointOppSide = getSumSCOverPoint(rUncapture, "L", x.POS)
		pt.SumScSameSide = getSumScSide(rUncapture, "F")
		pt.ScMaxSameSide = getScMaxSide(rUncapture, "F")
		pt.ScMinSameSide = getScMinSide(rUncapture, "F")
		pt.ScAvgSameSide = getAVGScSide(rUncapture, "F")
		pt.ReadDepth = getDepthRead(rCapture, rUncapture)
		pt.ReadDepthNextPos = getReadDepthwithoutSC(evidence.Read, x.orderhit, x.POS+1)
		pt.ReadDepthPreviousPos = getReadDepthwithoutSC(evidence.Read, x.orderhit, x.POS-1)
		pt.SumSCOverPointSameSide = getSumSCOverPoint(rUncapture, "F", x.POS)
		pt.CaptureMapQMax = getMapQMax(rCapture)
		pt.CaptureMapQMin = getMapQMin(rCapture)
		pt.CaptureMapQAvg = int(getMapQAvg(rCapture))
		pt.UncaptureMapQMax = getMapQMax(rUncapture)
		pt.UncaptureMapQMin = getMapQMin(rUncapture)
		pt.UncaptureMapQAvg = int(getMapQAvg(rUncapture))
		// only sc
		rUncaptureSC := getOnlySCRead(rUncapture)
		pt.UncaptureSCMapQMax = getMapQMax(rUncaptureSC)
		pt.UncaptureSCMapQMin = getMapQMin(rUncaptureSC)
		pt.UncaptureSCMapQAvg = int(getMapQAvg(rUncaptureSC))
		pt.ReadPairCount = x.ReadPairCount
		pt.MatchOrientation = getMatchOrientation(x)

		pt.MaxMapQRP = getMaxIntArray(evidence.MapQRP)
		pt.MinMapQRP = getMinIntArray(evidence.MapQRP)
		pt.AvgMapQRP = getAvgIntArray(evidence.MapQRP)
		return pt
		// printTrainData(pt, task.OutputPath+temppath+"dup/"+jbs.CHROM)

	case "L":
		rCapture, rUncapture := getReadPileupPoint(evidence.Read, x.orderhit, x.END)
		var pt printTrain
		pt.SVType = "DUP"
		pt.Orientation = "L"
		pt.Chr = evidence.CHR
		pt.Pos = x.POS
		pt.ChrEnd = evidence.CHR
		pt.End = x.END
		pt.Hit = x.HIT
		pt.LongMaxHit = getLonggestHit(x)
		pt.LongMinHit = getMinLongHit(x)
		pt.LongAvgHit = getAVGLongHit(x)
		pt.MaxScHit = getmaxScHit(x)
		pt.MinScHit = getMinScHit(x)
		pt.AvgScHit = getAvgScHit(x)
		pt.SumScOppSide = getSumScSide(rUncapture, "F")
		pt.ScMaxOppSide = getScMaxSide(rUncapture, "F")
		pt.ScMinOppSide = getScMinSide(rUncapture, "F")
		pt.ScAvgOppSide = getAVGScSide(rUncapture, "F")
		pt.SumSCOverPointOppSide = getSumSCOverPoint(rUncapture, "F", x.END)
		pt.SumScSameSide = getSumScSide(rUncapture, "L")
		pt.ScMaxSameSide = getScMaxSide(rUncapture, "L")
		pt.ScMinSameSide = getScMinSide(rUncapture, "L")
		pt.ScAvgSameSide = getAVGScSide(rUncapture, "L")
		pt.SumSCOverPointSameSide = getSumSCOverPoint(rUncapture, "L", x.END)
		pt.ReadDepth = getDepthRead(rCapture, rUncapture)
		pt.ReadDepthNextPos = getReadDepthwithoutSC(evidence.Read, x.orderhit, x.END+1)
		pt.ReadDepthPreviousPos = getReadDepthwithoutSC(evidence.Read, x.orderhit, x.END-1)
		pt.CaptureMapQMax = getMapQMax(rCapture)
		pt.CaptureMapQMin = getMapQMin(rCapture)
		pt.CaptureMapQAvg = int(getMapQAvg(rCapture))
		pt.UncaptureMapQMax = getMapQMax(rUncapture)
		pt.UncaptureMapQMin = getMapQMin(rUncapture)
		pt.UncaptureMapQAvg = int(getMapQAvg(rUncapture))
		// only sc
		rUncaptureSC := getOnlySCRead(rUncapture)
		pt.UncaptureSCMapQMax = getMapQMax(rUncaptureSC)
		pt.UncaptureSCMapQMin = getMapQMin(rUncaptureSC)
		pt.UncaptureSCMapQAvg = int(getMapQAvg(rUncaptureSC))
		pt.ReadPairCount = x.ReadPairCount
		pt.MatchOrientation = getMatchOrientation(x)
		// printTrainData(pt, task.OutputPath+temppath+"dup/"+jbs.CHROM)
		pt.MaxMapQRP = getMaxIntArray(evidence.MapQRP)
		pt.MinMapQRP = getMinIntArray(evidence.MapQRP)
		pt.AvgMapQRP = getAvgIntArray(evidence.MapQRP)
		return pt
	}
	panic("pileup empty pt")
}

func pileupINV(evidence *evidentCollector, x *breakPointState, jbs *jobsSplitter, task *Task) printTrain {
	switch x.Orientation {
	case "F":
		rCapture, rUncapture := getReadPileupPoint(evidence.Read, x.orderhit, x.POS)

		var pt printTrain
		pt.SVType = "INV"
		pt.Orientation = "F"
		pt.Chr = evidence.CHR
		pt.Pos = x.POS
		pt.ChrEnd = evidence.CHR
		pt.End = x.END
		pt.Hit = x.HIT
		pt.LongMaxHit = getLonggestHit(x)
		pt.LongMinHit = getMinLongHit(x)
		pt.LongAvgHit = getAVGLongHit(x)
		pt.MaxScHit = getmaxScHit(x)
		pt.MinScHit = getMinScHit(x)
		pt.AvgScHit = getAvgScHit(x)
		pt.SumScOppSide = getSumScSide(rUncapture, "")
		pt.ScMaxOppSide = getScMaxSide(rUncapture, "")
		pt.ScMinOppSide = getScMinSide(rUncapture, "")
		pt.ScAvgOppSide = getAVGScSide(rUncapture, "")
		pt.SumSCOverPointOppSide = getSumSCOverPoint(rUncapture, "", x.POS)
		pt.SumScSameSide = getSumScSide(rUncapture, "")
		pt.ScMaxSameSide = getScMaxSide(rUncapture, "")
		pt.ScMinSameSide = getScMinSide(rUncapture, "")
		pt.ScAvgSameSide = getAVGScSide(rUncapture, "")
		pt.SumSCOverPointSameSide = getSumSCOverPoint(rUncapture, "", x.POS)
		pt.ReadDepth = getDepthRead(rCapture, rUncapture)
		pt.ReadDepthNextPos = getReadDepthwithoutSC(evidence.Read, x.orderhit, x.POS+1)
		pt.ReadDepthPreviousPos = getReadDepthwithoutSC(evidence.Read, x.orderhit, x.POS-1)
		pt.CaptureMapQMax = getMapQMax(rCapture)
		pt.CaptureMapQMin = getMapQMin(rCapture)
		pt.CaptureMapQAvg = int(getMapQAvg(rCapture))
		pt.UncaptureMapQMax = getMapQMax(rUncapture)
		pt.UncaptureMapQMin = getMapQMin(rUncapture)
		pt.UncaptureMapQAvg = int(getMapQAvg(rUncapture))
		// only sc
		rUncaptureSC := getOnlySCRead(rUncapture)
		pt.UncaptureSCMapQMax = getMapQMax(rUncaptureSC)
		pt.UncaptureSCMapQMin = getMapQMin(rUncaptureSC)
		pt.UncaptureSCMapQAvg = int(getMapQAvg(rUncaptureSC))
		pt.ReadPairCount = x.ReadPairCount
		pt.MatchOrientation = getMatchOrientation(x)

		pt.MaxMapQRP = getMaxIntArray(evidence.MapQRP)
		pt.MinMapQRP = getMinIntArray(evidence.MapQRP)
		pt.AvgMapQRP = getAvgIntArray(evidence.MapQRP)
		return pt
		// printTrainData(pt, task.OutputPath+temppath+"inv/"+jbs.CHROM)
	case "L":
		rCapture, rUncapture := getReadPileupPoint(evidence.Read, x.orderhit, x.END)

		var pt printTrain
		pt.SVType = "INV"
		pt.Orientation = "L"
		pt.Chr = evidence.CHR
		pt.Pos = x.POS
		pt.ChrEnd = evidence.CHR
		pt.End = x.END
		pt.Hit = x.HIT
		pt.LongMaxHit = getLonggestHit(x)
		pt.LongMinHit = getMinLongHit(x)
		pt.LongAvgHit = getAVGLongHit(x)
		pt.MaxScHit = getmaxScHit(x)
		pt.MinScHit = getMinScHit(x)
		pt.AvgScHit = getAvgScHit(x)
		pt.SumScOppSide = getSumScSide(rUncapture, "")
		pt.ScMaxOppSide = getScMaxSide(rUncapture, "")
		pt.ScMinOppSide = getScMinSide(rUncapture, "")
		pt.ScAvgOppSide = getAVGScSide(rUncapture, "")
		pt.SumSCOverPointOppSide = getSumSCOverPoint(rUncapture, "", x.END)
		pt.SumScSameSide = getSumScSide(rUncapture, "")
		pt.ScMaxSameSide = getScMaxSide(rUncapture, "")
		pt.ScMinSameSide = getScMinSide(rUncapture, "")
		pt.ScAvgSameSide = getAVGScSide(rUncapture, "")
		pt.SumSCOverPointSameSide = getSumSCOverPoint(rUncapture, "", x.END)
		pt.ReadDepth = getDepthRead(rCapture, rUncapture)
		pt.ReadDepthNextPos = getReadDepthwithoutSC(evidence.Read, x.orderhit, x.END+1)
		pt.ReadDepthPreviousPos = getReadDepthwithoutSC(evidence.Read, x.orderhit, x.END-1)
		pt.CaptureMapQMax = getMapQMax(rCapture)
		pt.CaptureMapQMin = getMapQMin(rCapture)
		pt.CaptureMapQAvg = int(getMapQAvg(rCapture))
		pt.UncaptureMapQMax = getMapQMax(rUncapture)
		pt.UncaptureMapQMin = getMapQMin(rUncapture)
		pt.UncaptureMapQAvg = int(getMapQAvg(rUncapture))
		// only sc
		rUncaptureSC := getOnlySCRead(rUncapture)
		pt.UncaptureSCMapQMax = getMapQMax(rUncaptureSC)
		pt.UncaptureSCMapQMin = getMapQMin(rUncaptureSC)
		pt.UncaptureSCMapQAvg = int(getMapQAvg(rUncaptureSC))
		pt.ReadPairCount = x.ReadPairCount
		pt.MatchOrientation = getMatchOrientation(x)

		pt.MaxMapQRP = getMaxIntArray(evidence.MapQRP)
		pt.MinMapQRP = getMinIntArray(evidence.MapQRP)
		pt.AvgMapQRP = getAvgIntArray(evidence.MapQRP)
		// printTrainData(pt, task.OutputPath+temppath+"inv/"+jbs.CHROM)
		return pt
	}
	panic("pileup empty pt")
}

func printTrainListtoTSV(pt *[]printTrain) []string {
	var texts []string
	for _, x := range *pt {
		texts = append(texts, ptsToTSV(&x))
	}

	return texts
}

func ptsToTSV(pt *printTrain) string {

	// {"SVType":"DEL","Orientation":"F","Chr":"chr1","Pos":24288749,"ChrEnd":"chr1","End":77483292,"Hit":5
	// ,"LongMaxHit":58,"LongMinHit":6,"LongAvgHit":32,"MaxScHit":58,"MinScHit":6,"AvgScHit":32,"SumScOppSide":0
	// ,"ScMaxOppSide":0,"ScMinOppSide":0,"ScAvgOppSide":0,"SumSCOverPointOppSide":0,"SumScSameSide":0,"ScMaxSameSide":0
	// ,"ScMinSameSide":0,"ScAvgSameSide":0,"SumSCOverPointSameSide":0,"ReadDepth":28,"ReadDepthNextPos":23
	var text string

	text = pt.SVType + "\t" +
		pt.Orientation + "\t" +
		pt.Chr + "\t" +
		strconv.Itoa(pt.Pos) + "\t" +
		pt.ChrEnd + "\t" +
		strconv.Itoa(pt.End) + "\t" +
		strconv.Itoa(pt.Hit) + "\t" +
		strconv.Itoa(pt.LongMaxHit) + "\t" +
		strconv.Itoa(pt.LongMinHit) + "\t" +
		strconv.Itoa(pt.LongAvgHit) + "\t" +
		strconv.Itoa(pt.MaxScHit) + "\t" +
		strconv.Itoa(pt.MinScHit) + "\t" +
		strconv.Itoa(pt.AvgScHit) + "\t" +
		strconv.Itoa(pt.SumScOppSide) + "\t" +

		strconv.Itoa(pt.ScMaxOppSide) + "\t" +
		strconv.Itoa(pt.ScMinOppSide) + "\t" +
		strconv.Itoa(pt.ScAvgOppSide) + "\t" +
		strconv.Itoa(pt.SumSCOverPointOppSide) + "\t" +
		strconv.Itoa(pt.SumScSameSide) + "\t" +
		strconv.Itoa(pt.ScMaxSameSide) + "\t" +

		strconv.Itoa(pt.ScMinSameSide) + "\t" +
		strconv.Itoa(pt.ScAvgSameSide) + "\t" +
		strconv.Itoa(pt.SumSCOverPointSameSide) + "\t" +
		strconv.Itoa(pt.ReadDepth) + "\t" +
		strconv.Itoa(pt.ReadDepthNextPos) + "\t" +
		// ,"ReadDepthPreviousPos":23,"CaptureMapQMax":0,"CaptureMapQMin":0,"CaptureMapQAvg":0,"UncaptureMapQMax":40

		strconv.Itoa(pt.ReadDepthPreviousPos) + "\t" +
		strconv.Itoa(pt.CaptureMapQMax) + "\t" +
		strconv.Itoa(pt.CaptureMapQMin) + "\t" +
		strconv.Itoa(pt.CaptureMapQAvg) + "\t" +
		strconv.Itoa(pt.UncaptureMapQMax) + "\t" +
		// ,"UncaptureMapQMin":0,"UncaptureMapQAvg":26,"UncaptureSCMapQMax":0,"UncaptureSCMapQMin":0,"UncaptureSCMapQAvg":0
		strconv.Itoa(pt.UncaptureMapQMin) + "\t" +
		strconv.Itoa(pt.UncaptureMapQAvg) + "\t" +
		strconv.Itoa(pt.UncaptureSCMapQMax) + "\t" +
		strconv.Itoa(pt.UncaptureSCMapQMin) + "\t" +
		strconv.Itoa(pt.UncaptureSCMapQAvg) + "\t" +
		// ,"ReadPairCount":6,"MLTrainPositive":false,"MatchOrientation":"","MapQRP":[0,0,0,0,0,0]}

		strconv.Itoa(pt.ReadPairCount) + "\t" +
		pt.MatchOrientation + "\t" +
		strconv.Itoa(pt.MaxMapQRP) + "\t" +
		strconv.Itoa(pt.AvgMapQRP) + "\t" +
		strconv.Itoa(pt.MinMapQRP) + "\t" +
		strconv.FormatBool(pt.MLTrainPositive)

	return text

}

func stringToStringPTMarshal(pt *string) string {
	bytes, err := json.Marshal(tsvtoPrintTrain(pt))
	if err != nil {
		panic(err)
	}
	return string(bytes)
}

func ptsToStringMarshal(pt *printTrain) string {
	bytes, err := json.Marshal(pt)
	if err != nil {
		panic(err)
	}
	return string(bytes)
}

func tsvtoPrintTrain(text *string) printTrain {
	var pt printTrain
	s := strings.Split(*text, "\t")

	pt.SVType = s[0]
	pt.Orientation = s[1]
	pt.Chr = s[2]
	pos, err := strconv.Atoi(s[3])
	if err != nil {
		panic(err)
	}
	pt.Pos = pos
	pt.ChrEnd = s[4]
	end, err := strconv.Atoi(s[5])
	if err != nil {
		panic(err)
	}
	pt.End = end

	hit, err := strconv.Atoi(s[6])
	if err != nil {
		panic(err)
	}
	pt.Hit = hit

	// ,"LongMaxHit":58,"LongMinHit":6,"LongAvgHit":32,"MaxScHit":58,"MinScHit":6,"AvgScHit":32,"SumScOppSide":0 //13
	LongMaxHit, err := strconv.Atoi(s[7])
	if err != nil {
		panic(err)
	}
	pt.LongMaxHit = LongMaxHit

	LongMinHit, err := strconv.Atoi(s[8])
	if err != nil {
		panic(err)
	}
	pt.LongMinHit = LongMinHit

	LongAvgHit, err := strconv.Atoi(s[9])
	if err != nil {
		panic(err)
	}
	pt.LongAvgHit = LongAvgHit

	MaxScHit, err := strconv.Atoi(s[10])
	if err != nil {
		panic(err)
	}
	pt.MaxScHit = MaxScHit

	MinScHit, err := strconv.Atoi(s[11])
	if err != nil {
		panic(err)
	}
	pt.MinScHit = MinScHit

	AvgScHit, err := strconv.Atoi(s[12])
	if err != nil {
		panic(err)
	}
	pt.AvgScHit = AvgScHit

	SumScOppSide, err := strconv.Atoi(s[13])
	if err != nil {
		panic(err)
	}
	pt.SumScOppSide = SumScOppSide

	// ,"ScMaxOppSide":0,"ScMinOppSide":0,"ScAvgOppSide":0,"SumSCOverPointOppSide":0,"SumScSameSide":0,"ScMaxSameSide":0 //19
	ScMaxOppSide, err := strconv.Atoi(s[14])
	if err != nil {
		panic(err)
	}
	pt.ScMaxOppSide = ScMaxOppSide

	ScMinOppSide, err := strconv.Atoi(s[15])
	if err != nil {
		panic(err)
	}
	pt.ScMinOppSide = ScMinOppSide

	ScAvgOppSide, err := strconv.Atoi(s[16])
	if err != nil {
		panic(err)
	}
	pt.ScAvgOppSide = ScAvgOppSide

	SumSCOverPointOppSide, err := strconv.Atoi(s[17])
	if err != nil {
		panic(err)
	}
	pt.SumSCOverPointOppSide = SumSCOverPointOppSide

	SumScSameSide, err := strconv.Atoi(s[18])
	if err != nil {
		panic(err)
	}
	pt.SumScSameSide = SumScSameSide

	ScMaxSameSide, err := strconv.Atoi(s[19])
	if err != nil {
		panic(err)
	}
	pt.ScMaxSameSide = ScMaxSameSide

	// ,"ScMinSameSide":0,"ScAvgSameSide":0,"SumSCOverPointSameSide":0,"ReadDepth":28,"ReadDepthNextPos":23 //24

	ScMinSameSide, err := strconv.Atoi(s[20])
	if err != nil {
		panic(err)
	}
	pt.ScMinSameSide = ScMinSameSide

	ScAvgSameSide, err := strconv.Atoi(s[21])
	if err != nil {
		panic(err)
	}
	pt.ScAvgSameSide = ScAvgSameSide

	SumSCOverPointSameSide, err := strconv.Atoi(s[22])
	if err != nil {
		panic(err)
	}
	pt.SumSCOverPointSameSide = SumSCOverPointSameSide

	ReadDepth, err := strconv.Atoi(s[23])
	if err != nil {
		panic(err)
	}
	pt.ReadDepth = ReadDepth

	ReadDepthNextPos, err := strconv.Atoi(s[24])
	if err != nil {
		panic(err)
	}
	pt.ReadDepthNextPos = ReadDepthNextPos

	// ,"ReadDepthPreviousPos":23,"CaptureMapQMax":0,"CaptureMapQMin":0,"CaptureMapQAvg":0,"UncaptureMapQMax":40 //29
	ReadDepthPreviousPos, err := strconv.Atoi(s[25])
	if err != nil {
		panic(err)
	}
	pt.ReadDepthPreviousPos = ReadDepthPreviousPos

	CaptureMapQMax, err := strconv.Atoi(s[26])
	if err != nil {
		panic(err)
	}
	pt.CaptureMapQMax = CaptureMapQMax

	CaptureMapQMin, err := strconv.Atoi(s[27])
	if err != nil {
		panic(err)
	}
	pt.CaptureMapQMin = CaptureMapQMin

	CaptureMapQAvg, err := strconv.Atoi(s[28])
	if err != nil {
		panic(err)
	}
	pt.CaptureMapQAvg = CaptureMapQAvg

	UncaptureMapQMax, err := strconv.Atoi(s[29])
	if err != nil {
		panic(err)
	}
	pt.UncaptureMapQMax = UncaptureMapQMax

	// ,"UncaptureMapQMin":0,"UncaptureMapQAvg":26,"UncaptureSCMapQMax":0,"UncaptureSCMapQMin":0,"UncaptureSCMapQAvg":0 //34
	UncaptureMapQMin, err := strconv.Atoi(s[30])
	if err != nil {
		panic(err)
	}
	pt.UncaptureMapQMin = UncaptureMapQMin

	UncaptureMapQAvg, err := strconv.Atoi(s[31])
	if err != nil {
		panic(err)
	}
	pt.UncaptureMapQAvg = UncaptureMapQAvg

	UncaptureSCMapQMax, err := strconv.Atoi(s[32])
	if err != nil {
		panic(err)
	}
	pt.UncaptureSCMapQMax = UncaptureSCMapQMax

	UncaptureSCMapQMin, err := strconv.Atoi(s[33])
	if err != nil {
		panic(err)
	}
	pt.UncaptureSCMapQMin = UncaptureSCMapQMin

	UncaptureSCMapQAvg, err := strconv.Atoi(s[34])
	if err != nil {
		panic(err)
	}
	pt.UncaptureSCMapQAvg = UncaptureSCMapQAvg
	// ,"ReadPairCount":6,"MLTrainPositive":false,"MatchOrientation":"","MapQRP":[0,0,0,0,0,0]} //38
	ReadPairCount, err := strconv.Atoi(s[35])
	if err != nil {
		panic(err)
	}
	pt.ReadPairCount = ReadPairCount

	pt.MatchOrientation = s[36]

	MaxMapQRP, err := strconv.Atoi(s[37])
	if err != nil {
		panic(err)
	}
	pt.MaxMapQRP = MaxMapQRP

	AvgMapQRP, err := strconv.Atoi(s[38])
	if err != nil {
		panic(err)
	}
	pt.AvgMapQRP = AvgMapQRP

	MinMapQRP, err := strconv.Atoi(s[39])
	if err != nil {
		panic(err)
	}
	pt.MinMapQRP = MinMapQRP

	// pt.MapQRP = stringIntArrayToIntArray(s[37])

	MLTrainPositive, _ := strconv.ParseBool(s[40])
	if err != nil {
		panic(err)
	}
	pt.MLTrainPositive = MLTrainPositive

	return pt
}

func stringIntArrayToIntArray(value string) []int {
	var temp []int
	s := strings.Split(value, ",")
	for _, x := range s {
		i, err := strconv.Atoi(x)
		if err != nil {
			panic(err)
		}
		temp = append(temp, i)
	}

	return temp

}

func arrayIntToStringComma(value []int) string {
	var buffer bytes.Buffer

	for n, x := range value {
		if n != 0 {
			buffer.WriteString(",")
		}
		buffer.WriteString(strconv.Itoa(x))
	}

	return buffer.String()
}
