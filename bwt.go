package bolt

import (
	"bytes"
	"sort"

	"github.com/biogo/hts/sam"
)

type bWTScore struct {
	Pos         int
	Miss        int
	HIT         int
	F2L         bool
	SoftClipped int
}

func seqReverseComplement(seq string) string {
	var buffer bytes.Buffer
	for _, x := range seq {
		if string(x) == "G" {
			buffer.WriteString("C")
		} else if string(x) == "C" {
			buffer.WriteString("G")
		} else if string(x) == "T" {
			buffer.WriteString("A")
		} else {
			buffer.WriteString("T")
		}
	}
	return reverse(buffer.String())
}

func reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

func (bwt *bWT) locateReadINV(read *sam.Record, hit int) []bWTScore {
	MaxMissMatch := maxMissMatchCalculate(hit)
	// var MaxMissMatch int
	var missMatch int
	var LastMatch bool
	s := []rune(seqReverseComplement(string(read.Seq.Expand())))
	// s := []rune(string(nseq))
	SizeS := len(s)
	// fmt.Println("bwt.Size", bwt.Size)
	var allScore []bWTScore
	//search first column
	for i := 0; i < bwt.Size; i++ {
		missMatch = 0
		LastMatch = false
		if bwt.Index[i].L == s[0] {
			var score bWTScore
			score.Pos = bwt.Size - bwt.Index[i].SignPos - 2
			// score.Pos = bwt.Index[i].SignPos + 1 - SizeS
			// score.Pos = bwt.Size - bwt.Index[i].SignPos - 2
			// score.Pos = bwt.Index[i].SignPos - bwt.Size + 1
			// Pos := score.Pos
			// vv, ok := read.BWTScore.Load(score.Pos)
			// if ok {
			// 	allScore = append(allScore, vv.(BWTScore))
			// 	continue
			// }
			// fmt.Println(bwt.Index[i].SignPos)
			// fmt.Println("1>", string(bwt.Index[i].L), string(s[0]))
			// fmt.Println(score)
			// score.CurrentScore += 1
			score.HIT++
			// score.SummaryScore += score.CurrentScore
			l2fJump := bwt.Index[i].F2Lindex
			for j := 1; j < SizeS; j++ {
				if bwt.Index[l2fJump].L == s[j] {
					// fmt.Println(">>", string(bwt.Index[l2fJump].L), string(s[j]), bwt.Index[i].F2Lindex)
					// score.CurrentScore += 1
					score.HIT++
					// score.SummaryScore += score.CurrentScore
					l2fJump = bwt.Index[l2fJump].F2Lindex
					LastMatch = true
				} else if missMatch < MaxMissMatch && LastMatch {
					//if more than softclipped
					if hit <= score.HIT {
						// if hit <= score.HIT {
						score.F2L = true
						// }
						score.SoftClipped = hit
						// score.HIT--
						// score.Pos = bwt.Size - (score.HIT + Pos)
						allScore = append(allScore, score)

						break
					}
					// fmt.Println("------00000", score.HIT)
					//normal
					// score.CurrentScore += 1
					score.HIT++
					// score.SummaryScore += score.CurrentScore
					l2fJump = bwt.Index[l2fJump].F2Lindex
					missMatch++
					LastMatch = false
				} else {
					if score.HIT >= hit {
						if LastMatch {
							if hit <= score.HIT {
								score.F2L = true
							}
							score.SoftClipped = hit
							// score.Pos = bwt.Size - (score.HIT + Pos)
							allScore = append(allScore, score)
							// fmt.Println("OO", score)
						} else {
							if hit <= score.HIT {
								score.F2L = true
							}

							score.SoftClipped = hit - 1
							score.HIT--
							// score.Pos = bwt.Size - (score.HIT + Pos)
							allScore = append(allScore, score)
							// if softClipLast <= score.HIT {
							// 	score.F2L = true
							// }

							// score.SoftClipped = softClipLast
							// score.Pos = bwt.Size - (score.HIT + Pos)
							// allScore = append(allScore, score)
							// fmt.Println("OO", score)
						}
					}

					// } else {
					// 	if score.HIT > 10 {
					// 		score.SoftClipped = softClipLast
					// 		score.Pos = bwt.Size - (score.HIT + Pos) - 1
					// 		fmt.Println("OO", score)
					// 	} else {
					// 		fmt.Println("MOO", score)
					// 	}

					// }

					// if score.HIT > 10 {
					// 	score.SoftClipped = softClipLast
					// 	score.Pos = bwt.Size - (score.HIT + Pos) - 1
					// 	fmt.Println("OO", score)
					// }
					break
				}

				if j+1 >= SizeS {
					if score.HIT >= hit {
						// if softClipLast < score.HIT {
						score.F2L = true
						// }

						// score.Pos = bwt.Size - (score.HIT + Pos)
						// read.BWTScore.Store(score.Pos, score)
						allScore = append(allScore, score)
					}
				}
			}
		}
	}

	sort.Slice(allScore, func(i, j int) bool { return (allScore)[i].HIT > (allScore)[j].HIT })

	return allScore

}

func (bwt *bWT) locateReverseReadINV(read *sam.Record, hit int) []bWTScore {
	MaxMissMatch := maxMissMatchCalculate(hit)
	var missMatch int
	var LastMatch bool
	s := []rune(seqReverseComplement(string(read.Seq.Expand())))
	SizeS := len(s)
	// fmt.Println("bwt.Size", bwt.Size)
	var allScore []bWTScore

	//search first column
	for i := 0; i < bwt.Size; i++ {
		missMatch = 0
		LastMatch = false
		// if i != 743 {
		// 	continue
		// }
		// fmt.Println(i)
		// fmt.Println(string(bwt.Index[i].F))
		// fmt.Println("------")
		// fmt.Println("lll", i, string(bwt.Index[i].F), string(s[SizeS-1]))
		if bwt.Index[i].F == s[SizeS-1] {
			var score bWTScore
			score.Pos = bwt.Index[i].SignPos - 1
			Pos := bwt.Index[i].SignPos - 1

			// if read.Rec.Pos < Pos && Pos > read.Rec.Pos+read.Rec.Seq.Length {
			// 	continue
			// }
			// fmt.Println(string(bwt.Index[i].F), string(s[SizeS-1]), missMatch, MaxMissMatch, LastMatch)
			// score.CurrentScore += 1
			score.HIT++
			// score.SummaryScore += score.CurrentScore
			l2fJump := bwt.Index[i].L2FindexRev
			for j := 1; j < SizeS; j++ {

				if bwt.Index[l2fJump].F == s[SizeS-1-j] {
					// fmt.Println(">>", string(bwt.Index[l2fJump].F), string(s[SizeS-1-j]))
					LastMatch = true
					// score.CurrentScore += 1
					score.HIT++
					// score.SummaryScore += score.CurrentScore
					l2fJump = bwt.Index[l2fJump].L2FindexRev

				} else if missMatch < MaxMissMatch && LastMatch {

					//if more than softclipped
					if score.HIT >= hit {
						// if hit <= score.HIT {
						score.F2L = true
						// }
						score.SoftClipped = hit
						// score.HIT--

						allScore = append(allScore, score)
						// fmt.Println("1", score)

						break
					}
					//normal
					// score.CurrentScore += 1
					score.HIT++
					// score.SummaryScore += score.CurrentScore
					l2fJump = bwt.Index[l2fJump].L2FindexRev
					missMatch++
					LastMatch = false

				} else {

					if score.HIT >= hit {
						if LastMatch {
							if hit <= score.HIT {
								score.F2L = true
							}
							score.SoftClipped = hit
							allScore = append(allScore, score)
							// fmt.Println("OO", score)
						} else {
							if hit <= score.HIT {
								score.F2L = true
							}

							score.SoftClipped = hit - 1
							score.HIT--
							allScore = append(allScore, score)
						}
						// fmt.Println("2", score)
					}
					// fmt.Println(score)
					break
				}

				if j+1 >= SizeS {
					if score.HIT >= hit {
						score.F2L = true

						score.Pos = bwt.Size - (score.HIT + Pos) - 1
						// read.BWTScore.Store(score.Pos, score)
						allScore = append(allScore, score)
					}
					// break
				}
			}
		}

	}

	sort.Slice(allScore, func(i, j int) bool { return (allScore)[i].HIT > (allScore)[j].HIT })
	// for _, x := range allScore {
	// 	fmt.Println(x)
	// }

	return allScore
}

func (bwt *bWT) locateRead(read *sam.Record, hit int) []bWTScore {
	MaxMissMatch := maxMissMatchCalculate(hit)
	var missMatch int
	// var softClipLast int
	var LastMatch bool
	// if read.Cigar[0].Type().String() == "S" {
	// 	softClipLast = read.Cigar[0].Len()
	// }
	s := []rune(string(read.Seq.Expand()))
	SizeS := len(s)
	// fmt.Println("bwt.Size", bwt.Size)
	var allScore []bWTScore
	//search first column
	for i := 0; i < bwt.Size; i++ {
		missMatch = 0
		LastMatch = false
		// fmt.Println("lll", i, string(bwt.Index[i].L), string(s[0]))
		if bwt.Index[i].L == s[0] {
			var score bWTScore
			// score.Pos = bwt.Index[i].SignPos + 1 - SizeS
			Pos := bwt.Size - bwt.Index[i].SignPos - 2
			score.Pos = bwt.Size - bwt.Index[i].SignPos - 2
			// vv, ok := read.BWTScore.Load(score.Pos)
			// if ok {
			// 	allScore = append(allScore, vv.(BWTScore))
			// 	continue
			// }
			// fmt.Println(bwt.Index[i].SignPos)
			// fmt.Println("1>", string(bwt.Index[i].L), string(s[0]))
			// fmt.Println(score)
			// score.CurrentScore += 1
			score.HIT++
			// score.SummaryScore += score.CurrentScore
			l2fJump := bwt.Index[i].F2Lindex
			for j := 1; j < SizeS; j++ {
				if bwt.Index[l2fJump].L == s[j] {
					// fmt.Println(">>", string(bwt.Index[l2fJump].L), string(s[j]), bwt.Index[i].F2Lindex)
					// score.CurrentScore += 1
					score.HIT++
					// score.SummaryScore += score.CurrentScore
					l2fJump = bwt.Index[l2fJump].F2Lindex
					LastMatch = true
					// missMatch = 0
					// } else if missMatch <= MaxMissMatch {
					// 	missMatch++
					// 	score.CurrentScore -= 1
					// 	score.HIT++
					// 	score.SummaryScore += score.CurrentScore
					// 	l2fJump = bwt.Index[l2fJump].F2Lindex
				} else if missMatch < MaxMissMatch && LastMatch {
					//if more than softclipped
					if hit <= score.HIT {
						// if hit <= score.HIT {
						score.F2L = true
						// }
						score.SoftClipped = hit
						// score.HIT--
						score.Pos = bwt.Size - (score.HIT + Pos) - 1
						allScore = append(allScore, score)
						break
					}
					//normal
					// score.CurrentScore += 1
					score.HIT++
					// score.SummaryScore += score.CurrentScore
					l2fJump = bwt.Index[l2fJump].F2Lindex
					missMatch++
					LastMatch = false
				} else {
					if score.HIT >= hit {
						if LastMatch {
							if hit <= score.HIT {
								score.F2L = true
							}
							score.SoftClipped = hit
							score.Pos = bwt.Size - (score.HIT + Pos) - 1
							allScore = append(allScore, score)
							// fmt.Println("OO", score)
						} else {
							if hit <= score.HIT {
								score.F2L = true
							}

							score.SoftClipped = hit - 1
							score.HIT--
							score.Pos = bwt.Size - (score.HIT + Pos) - 1
							allScore = append(allScore, score)
							// if softClipLast <= score.HIT {
							// 	score.F2L = true
							// }

							// score.SoftClipped = softClipLast
							// score.Pos = bwt.Size - (score.HIT + Pos)
							// allScore = append(allScore, score)
							// fmt.Println("OO", score)
						}
					}

					// } else {
					// 	if score.HIT > 10 {
					// 		score.SoftClipped = softClipLast
					// 		score.Pos = bwt.Size - (score.HIT + Pos) - 1
					// 		fmt.Println("OO", score)
					// 	} else {
					// 		fmt.Println("MOO", score)
					// 	}

					// }

					// if score.HIT > 10 {
					// 	score.SoftClipped = softClipLast
					// 	score.Pos = bwt.Size - (score.HIT + Pos) - 1
					// 	fmt.Println("OO", score)
					// }
					break
				}

				if j+1 >= SizeS {
					if score.HIT >= hit {
						score.F2L = true
						// read.BWTScore.Store(score.Pos, score)
						allScore = append(allScore, score)
					}
					// fmt.Println(score)
					// fmt.Println("-----")
				}
			}
		}
		// fmt.Println("-----")
	}

	sort.Slice(allScore, func(i, j int) bool { return (allScore)[i].HIT > (allScore)[j].HIT })

	return allScore

}

func (bwt *bWT) locateReverseRead(read *sam.Record, hit int) []bWTScore {
	MaxMissMatch := maxMissMatchCalculate(hit)

	// var softClipLast int
	var LastMatch bool
	// if read.Rec.Cigar[0].Type().String() == "S" {
	// 	softClipLast = read.Rec.Cigar[0].Len()
	// }
	s := []rune(string(read.Seq.Expand()))
	SizeS := len(s)
	// fmt.Println("bwt.Size", bwt.Size)
	var allScore []bWTScore
	//search first column
	for i := 0; i < bwt.Size; i++ {
		var missMatch int
		LastMatch = false
		if bwt.Index[i].F == s[SizeS-1] {
			var score bWTScore
			Pos := bwt.Index[i].SignPos - 1
			// score.CurrentScore += 1
			score.HIT++
			// score.SummaryScore += score.CurrentScore
			l2fJump := bwt.Index[i].L2FindexRev
			for j := 1; j < SizeS; j++ {
				if bwt.Index[l2fJump].F == s[SizeS-1-j] {
					// score.CurrentScore += 1
					score.HIT++
					// score.SummaryScore += score.CurrentScore
					l2fJump = bwt.Index[l2fJump].L2FindexRev
					LastMatch = true
				} else if missMatch < MaxMissMatch && LastMatch {
					//if more than softclipped
					if hit <= score.HIT {
						score.F2L = true
						score.SoftClipped = hit
						score.Pos = bwt.Size - (score.HIT + Pos) - 1
						allScore = append(allScore, score)
						break
					}
					//normal
					// score.CurrentScore += 1
					score.HIT++
					// score.SummaryScore += score.CurrentScore
					l2fJump = bwt.Index[l2fJump].L2FindexRev
					missMatch++
					LastMatch = false
				} else {
					if score.HIT >= hit {
						if LastMatch {
							if hit <= score.HIT {
								score.F2L = true
							}
							score.SoftClipped = hit
							score.Pos = bwt.Size - (score.HIT + Pos) - 1
							allScore = append(allScore, score)
							// fmt.Println("OO", score)
						} else {
							if hit <= score.HIT {
								score.F2L = true
							}

							score.SoftClipped = hit - 1
							score.HIT--
							score.Pos = bwt.Size - (score.HIT + Pos) - 1
							allScore = append(allScore, score)
							// if softClipLast <= score.HIT {
							// 	score.F2L = true
							// }

							// score.SoftClipped = softClipLast
							// score.Pos = bwt.Size - (score.HIT + Pos)
							// allScore = append(allScore, score)
							// fmt.Println("OO", score)
						}
					}

					// } else {
					// 	if score.HIT > 10 {
					// 		score.SoftClipped = softClipLast
					// 		score.Pos = bwt.Size - (score.HIT + Pos) - 1
					// 		fmt.Println("OO", score)
					// 	} else {
					// 		fmt.Println("MOO", score)
					// 	}

					// }

					// if score.HIT > 10 {
					// 	score.SoftClipped = softClipLast
					// 	score.Pos = bwt.Size - (score.HIT + Pos) - 1
					// 	fmt.Println("OO", score)
					// }
					break
				}

				if j+1 >= SizeS {
					if score.HIT >= hit {
						// if softClipLast < score.HIT {
						score.F2L = true
						// }

						score.Pos = bwt.Size - (score.HIT + Pos) - 1
						// read.BWTScore.Store(score.Pos, score)
						allScore = append(allScore, score)
					}
				}
			}
		}

	}

	sort.Slice(allScore, func(i, j int) bool { return (allScore)[i].HIT > (allScore)[j].HIT })
	// for _, x := range allScore {
	// 	fmt.Println(x)
	// }

	return allScore
}

// func (bwt *bWT) chainFinding(s *[]rune, indexPos int) {
// 	var score bWTScore
// 	if bwt.Index[indexPos].F == (*s)[0] {
// 		score.CurrentScore += 1
// 		score.SummaryScore += score.CurrentScore
// 	} else {
// 		return
// 	}

// }

func newBWT() *bWT {
	return &bWT{}
}

func (bwt *bWT) rotations(s *string) *[]rotation {
	var out []string
	var rotations []rotation
	ss := *s + *s
	sizeS := len(*s)
	for i := 0; i < sizeS; i++ {
		out = append(out, ss[i:i+sizeS])
		rotations = append(rotations, rotation{Seq: ss[i : i+sizeS], SignPos: sizeS - 1 - i})
		// fmt.Println(ss[i:i+sizeS], i)
	}
	return &rotations
}

type rotation struct {
	Seq     string
	SignPos int
}

func (bwt *bWT) bwm(sTxt *string) *[]rotation {
	rota := bwt.rotations(sTxt)
	sort.Slice(*rota, func(i, j int) bool { return (*rota)[i].Seq < (*rota)[j].Seq })
	return rota
}

type bWT struct {
	Size  int
	Index []indexBWT
}

type indexBWT struct {
	F           rune
	Findex      int
	L           rune
	Lindex      int
	L2FindexRev int
	F2Lindex    int
	SignPos     int
}

type l2F struct {
	Base   rune
	Bindex int
}

func (bwt *bWT) Transform(s string) {
	s += "$"
	bwm := bwt.bwm(&s)

	sizeChr := len(s)

	var index []indexBWT
	fPos := make(map[rune]int)
	lPos := make(map[rune]int)
	var fCount, lCount int
	// fPos[]
	var runeF []rune
	var runeL []rune

	l2f := make(map[l2F]int)
	f2l := make(map[l2F]int)
	var countindex int
	for _, x := range *bwm {
		runeF = []rune((x).Seq[0:1])
		runeL = []rune((x).Seq[sizeChr-1 : sizeChr])
		fCount = fPos[runeF[0]]
		lCount = lPos[runeL[0]]

		indexSingle := indexBWT{F: runeF[0], Findex: fCount, L: runeL[0], Lindex: lCount, SignPos: x.SignPos}
		index = append(index, indexSingle)
		l2f[l2F{Base: runeF[0], Bindex: fCount}] = countindex
		f2l[l2F{Base: runeL[0], Bindex: lCount}] = countindex
		countindex++
		fPos[runeF[0]]++
		lPos[runeL[0]]++
	}

	for i := 0; i < len(index); i++ {
		index[i].L2FindexRev = l2f[l2F{Base: index[i].L, Bindex: index[i].Lindex}]
		index[i].F2Lindex = f2l[l2F{Base: index[i].F, Bindex: index[i].Findex}]
		// fmt.Println(string(index[i].F), string(index[i].L), index[i].L2FindexRev, index[i].F2Lindex)
	}
	bwt.Index = append(bwt.Index, index...)
	bwt.Size = len(index)

}

func maxMissMatchCalculate(readmax int) int {
	if readmax < 10 {
		return 0
	} else if readmax < 20 {
		return 1
	} else if readmax < 40 {
		return 2
	} else if readmax < 60 {
		return 3
	}
	return 4
}
