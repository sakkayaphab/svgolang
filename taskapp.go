package bolt

func (task *Task) getReferenceEND(name string) int {
	var pos int
	for i := 0; i < len(task.sampleStats.rangeCHROM); i++ {
		if task.sampleStats.rangeCHROM[i].Name == name {
			pos = task.sampleStats.rangeCHROM[i].END
			break
		}
	}
	return pos
}
