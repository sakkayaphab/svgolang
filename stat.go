package bolt

// func getInsertSizeFromRead(read *sam.Record) float64 {
// 	// if read.Pos

// 	return 0
// }

func getInsertSizeFromPosEnd(pos, end, readlength int) int {
	if pos <= end {
		return int(end + readlength - pos)
	}
	return int(pos + readlength - end)
}
