package bolt

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"runtime/debug"
	"sort"
	"strings"
)

// Convert bolt result to vcf
func getListsfileTrain(path string) []string {
	path = removeBackSlash(path)
	pathtemp := path + "/temp"
	pathSVdirLists := getPathDirSV(pathtemp)
	return pathSVdirLists
}

func sortTSVfiles(path string, task *Task) {
	path = removeBackSlash(path)
	dirlists := getListsfileTrain(path)
	// fmt.Println(dirlists)
	for _, pathSVdirList := range dirlists {
		pathLists := getListFileSVType(pathSVdirList)
		for _, y := range pathLists {
			// fmt.Println(y)
			sortToMultipleFiles(y, path)
		}
	}
}

func sortToMultipleFiles(path string, mainPath string) {
	pts := getTrainDataFromFilesOnly(path)
	sort.Slice(pts, func(i, j int) bool { return (pts)[i].Pos < (pts)[j].Pos })
	removeTrainDataDuplication(pts)
	writePTSliststoFile(pts, mainPath)
	runtime.GC()
	debug.FreeOSMemory()
}

func writePTSliststoFile(pts []*printTrain, filename string) {
	var svtype string
	var chrname string
	if len(pts) != 0 {
		svtype = strings.ToLower((pts)[0].SVType)
		chrname = (pts)[0].Chr
	}
	filepath := filename + "/analysis/" + svtype + "/" + chrname + ".txt"
	var _, err = os.Stat(filepath)
	// create file if not exists
	if os.IsNotExist(err) {
		var file, err = os.Create(filepath)
		if err != nil {
			return
		}
		defer file.Close()
	} else {
		err = os.Remove(filepath)
		if err != nil {
			return
		}

		var file, err = os.Create(filepath)
		if err != nil {
			return
		}
		defer file.Close()
	}

	f, err := os.OpenFile(filepath, os.O_APPEND|os.O_WRONLY, 0600)
	defer f.Close()
	if err != nil {
		panic(err)
	}

	w := bufio.NewWriter(f)

	for _, x := range pts {
		fmt.Fprintln(w, ptsToTSV(x))
	}
	w.Flush()
}

func removeTrainDataDuplication(pts []*printTrain) []*printTrain {

	var bucket []*printTrain
	var Pos, End int
	var Orentation string
	for _, pt := range pts {
		if pt.Orientation == Orentation && checkBetween(pt.Pos, Pos, 20, 20) && checkBetween(pt.End, End, 20, 20) {
			continue
		} else {
			Orentation = pt.Orientation
			Pos = pt.Pos
			End = pt.End
		}
		bucket = append(bucket, pt)
	}

	return bucket
}

func getTrainDataFromFilesOnly(path string) []*printTrain {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*10000)

	var bucket []*printTrain
	var Pos, End int
	var Orentation string
	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		pt := arrayBytesToPrintTrain(&line)
		if pt.Orientation == Orentation && checkBetween(pt.Pos, Pos, 20, 20) && checkBetween(pt.End, End, 20, 20) {
			continue
		} else {
			Orentation = pt.Orientation
			Pos = pt.Pos
			End = pt.End
		}

		bucket = append(bucket, &pt)
	}

	return bucket
}

func arrayBytesToPrintTrain(bytes *[]byte) printTrain {
	text := string(*bytes)
	return tsvtoPrintTrain(&text)
}
