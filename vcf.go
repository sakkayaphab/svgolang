package bolt

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"time"
)

type vCFFormat struct {
	CHROM  string
	POS    int
	ID     string
	REF    string
	ALT    string
	QUAL   int
	FILTER string
	INFO   string
}

func writeVCFHeader(filename string) {
	var _, err = os.Stat(filename)

	// create file if not exists
	if os.IsNotExist(err) {
		var file, err = os.Create(filename)
		if err != nil {
			return
		}
		defer file.Close()
	}

	f, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0600)
	defer f.Close()
	if err != nil {
		panic(err)
	}

	w := bufio.NewWriter(f)
	// fmt.Println()
	fmt.Fprintln(w, "##fileformat=VCFv4.3")
	t := time.Now()
	dateText := "##fileDate=" + t.Format("20060102")
	// t := time.Now()
	// fmt.Println(t.Format("20060102"))
	fmt.Fprintln(w, dateText)
	fmt.Fprintln(w, "##source=myImputationProgramV3.1")
	fmt.Fprintln(w, "##reference=file:///seq/references/1000GenomesPilot-NCBI36.fasta")
	fmt.Fprintln(w, `##INFO=<ID=NS,Number=1,Type=Integer,Description="Number of Samples With Data">`)
	fmt.Fprintln(w, `##INFO=<ID=DP,Number=1,Type=Integer,Description="Total Depth">`)
	fmt.Fprintln(w, `##INFO=<ID=AF,Number=A,Type=Float,Description="Allele Frequency">`)
	fmt.Fprintln(w, `##INFO=<ID=AA,Number=1,Type=String,Description="Ancestral Allele">`)
	fmt.Fprintln(w, `##INFO=<ID=SVTYPE,Number=1,Type=String,Description="Type of structural variant">`)
	fmt.Fprintln(w, `##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">`)
	fmt.Fprintln(w, `##FORMAT=<ID=GQ,Number=1,Type=Integer,Description="Genotype Quality">`)
	fmt.Fprintln(w, `##FORMAT=<ID=DP,Number=1,Type=Integer,Description="Read Depth">`)
	fmt.Fprintln(w, `##FORMAT=<ID=HQ,Number=2,Type=Integer,Description="Haplotype Quality">`)
	fmt.Fprintln(w, "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO")

	w.Flush()

}

func writeVCF(filename string, vcf *[]vCFFormat) {
	var _, err = os.Stat(filename)

	// create file if not exists
	if os.IsNotExist(err) {
		var file, err = os.Create(filename)
		if err != nil {
			return
		}
		defer file.Close()
	}

	f, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0600)
	defer f.Close()
	if err != nil {
		panic(err)
	}

	w := bufio.NewWriter(f)

	// fmt.Fprintln(w, "#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO")

	for i := 0; i < len(*vcf); i++ {
		// if (*vcf)[i].ALT != "" {
		// 	fmt.Println((*vcf)[i].ALT)
		// }

		tempQual := "."
		pos := strconv.Itoa((*vcf)[i].POS)
		fmt.Fprintln(w, (*vcf)[i].CHROM+"\t"+pos+"\t"+(*vcf)[i].ID+"\t"+(*vcf)[i].REF+"\t"+(*vcf)[i].ALT+"\t"+tempQual+"\t"+(*vcf)[i].FILTER+"\t"+(*vcf)[i].INFO)
	}
	w.Flush()
}
