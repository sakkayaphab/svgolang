package bolt

import (
	"fmt"
	"io"
	"os"
	"sync"

	"github.com/biogo/hts/bam"
	"github.com/biogo/hts/bgzf"
	"github.com/biogo/hts/sam"
)

type readContainer struct {
	Read []*sam.Record
	// SCRead []*sam.Record
}

type filterMarkDuplicate struct {
	LastSizeCigar int
	LastMapQ      byte
	LastPos       int
	LastEnd       int
}

func readBam(jbs chan jobsSplitter, bamfile string, wgChr *sync.WaitGroup, tasknumber int, task *Task) {
	defer wgChr.Done()
	var bamread io.Reader
	fRead, err := os.Open(bamfile)
	checkPanic(err)
	bamread = fRead

	bamRead, err := bam.NewReader(bamread, 0)
	checkPanic(err)
	defer bamRead.Close()

	defer fRead.Close()
	var freememCount int
	for {
		select {
		case jbs := <-jbs:
			var wgSV sync.WaitGroup
			if jbs.CHROM == "close" {
				wgSV.Wait()
				fmt.Println(jbs.CHROM, ": Closed")
				return
			}
			bamRead.Seek(jbs.Chunk.Begin)
			// Debug
			if task.mode == "DEBUG" {
				offset := findOffset(jbs.CHROM, 220440788-10000, 220440788+10000, task)
				bamRead.Seek(offset)
			}

			FlagStat := []uint16{2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1}

			CloseSVChannel := make(chan bool)

			//Find TRAN
			variantTranC := make(chan readContainer)
			wgSV.Add(1)
			go findTranslocation(variantTranC, CloseSVChannel, &wgSV, jbs, task)

			// //Find DUP
			variantDUPC := make(chan readContainer)
			wgSV.Add(1)
			go findDuplication(variantDUPC, CloseSVChannel, &wgSV, jbs, task)

			//Find INDEL
			// indelC := make(chan []*read)
			// CloseINDEL := make(chan bool)
			// go calculateINDELRead(indelC, CloseSVChannel, &wgState, jbs, task)

			// find SNV
			// variantSNVC := make(chan []*read)
			// CloseSNV := make(chan bool)
			// go calculateCheckSNVREAD(variantSNVC, CloseSVChannel, &wgState, jbs, task)

			//Find Deletion
			variantDELC := make(chan readContainer)
			wgSV.Add(1)
			go findDeletion(variantDELC, CloseSVChannel, &wgSV, jbs, task)

			//Find Insert
			variantINSC := make(chan readContainer)
			wgSV.Add(1)
			go findInsertion(variantINSC, CloseSVChannel, &wgSV, jbs, task)

			//find INV
			variantINVC := make(chan readContainer)
			wgSV.Add(1)
			go findInversion(variantINVC, CloseSVChannel, &wgSV, jbs, task)

			CountTemp := 0
			var tempread []*sam.Record
			// var sc []*sam.Record
			var fmarkdup filterMarkDuplicate
			for {
				rec, err := bamRead.Read()
				if err == io.EOF {
					break
				}
				checkPanic(err)
				// break
				if bamRead.LastChunk().Begin.Block >= jbs.Chunk.End.Block && bamRead.LastChunk().Begin.File >= jbs.Chunk.End.File {
					break
				}

				//Mark Duplication
				if checkfilterMarkDuplicate(&fmarkdup, rec) {
					continue
				}

				//Find Unmapped Read
				filter := filterReadBeforePE(rec, &FlagStat)
				if !filter {
					continue
				}

				// Remove unnecessary info  some read
				removeUnnecessaryInfoRead(rec)

				tempread = append(tempread, rec)
				// tempread = append(tempread, nil)

				// if getNumberSoftClippedFirst(rec) > 2 || getNumberSoftClippedLast(rec) > 2 {
				// 	sc = append(sc, rec)
				// }

				CountTemp++
				freememCount++
				if CountTemp > 500 {
					rc := readContainer{
						Read: tempread,
						// SCRead: sc,
					}

					if task.mode == "DEBUG" {
						// variantDUPC <- rc
						variantDELC <- rc
						// variantINVC <- rc

					} else {
						// variantSNVC <- temp
						// indelC <- temp

						variantTranC <- rc
						variantDELC <- rc
						variantINVC <- rc
						variantDUPC <- rc
						variantINSC <- rc
					}

					CountTemp = 0
					tempread = nil
					// sc = nil
				}

				// if freememCount > 500000 {
				// 	runtime.GC()
				// 	debug.FreeOSMemory()
				// 	freememCount = 0
				// }
			}

			fmt.Println("✓", jbs.CHROM)

			//Close goroutine
			for i := 0; i < 5; i++ {
				CloseSVChannel <- true
			}

			// return

		}
	}
}

func getSeqSCFirst(rec *sam.Record) {
	if getRealEnd(rec)-getRealPos(rec) != rec.Seq.Length {
		return
	}
	if len(rec.Cigar) == 2 {
		scFirst := getNumberSoftClippedFirst(rec)
		if scFirst >= 3 {
			fmt.Println(string(rec.Seq.Expand())[:scFirst], len(string(rec.Seq.Expand())[:scFirst]))
		}
	}
}

func getSeqSCLast(rec *sam.Record) {
	fmt.Println(rec)
	if getRealEnd(rec)-getRealPos(rec) != rec.Seq.Length {
		return
	}
	if len(rec.Cigar) == 2 {
		scLast := getNumberSoftClippedLast(rec)
		if scLast >= 3 {
			fmt.Println(string(rec.Seq.Expand())[rec.Seq.Length-scLast:], len(string(rec.Seq.Expand())[rec.Seq.Length-scLast:]))
		}
	}
}

type variants struct {
	CHR         string
	POS         int
	REF         string
	ALT         string
	SVTYPE      string
	END         int
	POSREAD     int
	MATEPOS     int
	MATECHR     string
	SoftClipped []softClipped
}

type softClipped struct {
	Pos    int
	Locate string
	Length int
	Seq    string
}

func removeUnnecessaryInfoRead(rec *sam.Record) {
	if len(rec.Cigar) == 1 {
		var new []sam.Doublet
		rec.Seq.Seq = new
		var qual []byte
		rec.Qual = qual
		var auxf sam.AuxFields
		rec.AuxFields = auxf
		rec.Name = ""
	}

}

func skipThisRead(rec *sam.Record) bool {
	if getNumberSoftClippedFirst(rec) >= 3 {
		return false
	} else if getNumberSoftClippedLast(rec) >= 3 {
		return false
	}
	return true
}

func isDiscordantRead(rec *sam.Record, FlagStat *[]uint16, task *Task) bool {
	if isReverseComplementProblem(rec, FlagStat) {
		return true
	}

	if isTranlocation(rec) {
		return true
	}

	if getInsertSize(rec, FlagStat, task) > int(task.sampleStats.InsertSizeMean)+int(task.sampleStats.InsertSizeSD)+50 {
		return true
	}

	if getInsertSize(rec, FlagStat, task) < int(task.sampleStats.InsertSizeMean)-int(task.sampleStats.InsertSizeSD)-50 {
		return true
	}

	return false
}

func isTranlocation(rec *sam.Record) bool {
	if rec.Ref.Name() != rec.MateRef.Name() {
		return true
	}
	return false
}

func getInsertSize(rec *sam.Record, FlagStat *[]uint16, task *Task) int {
	if getRealPos(rec) > rec.MatePos {
		return getRealEnd(rec) - rec.MatePos
	}
	return rec.MatePos + task.sampleStats.ReadLength - getRealPos(rec)

}

func getRealPos(rec *sam.Record) int {
	return rec.Pos - getNumberSoftClippedFirst(rec)
}

func getRealEnd(rec *sam.Record) int {
	return rec.End() + getNumberSoftClippedLast(rec)
}

func isReverseComplementProblem(rec *sam.Record, FlagStat *[]uint16) bool {
	var firstR, lastR bool
	TempFlag := uint16(rec.Flags)
	for i := 0; i < len(*FlagStat); i++ {
		if TempFlag >= (*FlagStat)[i] {
			TempFlag = TempFlag - (*FlagStat)[i]
			if (*FlagStat)[i] == 16 {
				firstR = true
			} else if (*FlagStat)[i] == 32 {
				lastR = true
			}
		}
	}

	if firstR == lastR {
		return true
	}
	return false
}

func filterReadBeforePE(rec *sam.Record, FlagStat *[]uint16) bool {

	TempFlag := uint16(rec.Flags)
	for i := 0; i < len(*FlagStat); i++ {
		if TempFlag >= (*FlagStat)[i] {
			TempFlag = TempFlag - (*FlagStat)[i]
			if (*FlagStat)[i] == 4 {
				return false
			} else if (*FlagStat)[i] == 512 {
				return false
			} else if (*FlagStat)[i] == 1024 {
				return false
			} else if (*FlagStat)[i] == 256 {
				return false
			}
		}
	}
	return true

}

func haveFilterReadByFlagCode(rec *sam.Record, FlagStat *[]uint16, code uint16) bool {

	TempFlag := uint16(rec.Flags)
	for i := 0; i < len(*FlagStat); i++ {
		if TempFlag >= (*FlagStat)[i] {
			TempFlag = TempFlag - (*FlagStat)[i]
			if (*FlagStat)[i] == code {
				return true
			}
		}
	}

	return false
}

func checkfilterMarkDuplicate(fmd *filterMarkDuplicate, rec *sam.Record) bool {
	var matchDup bool
	if fmd.LastSizeCigar == len(rec.Cigar) {
		if fmd.LastMapQ == rec.MapQ {
			if fmd.LastPos == rec.Pos {
				if fmd.LastEnd == rec.End() {
					matchDup = true
				}
			}
		}
	}

	if matchDup {
		return true
	}
	fmd.LastSizeCigar = len(rec.Cigar)
	fmd.LastMapQ = rec.MapQ
	fmd.LastPos = rec.Pos
	fmd.LastEnd = rec.End()

	return false
}

func checkPanic(err error) {
	if err != nil {
		panic(err)
	}
}

func findOffset(chr string, pos, end int, task *Task) bgzf.Offset {
	bamread, err := os.Open(task.SamplePath)
	if err != nil {
		panic(err)
	}
	defer bamread.Close()

	bamRead, err := bam.NewReader(bamread, 0)
	if err != nil {
		panic(err)
	}
	defer bamRead.Close()

	fidx, err := os.Open(task.SamplePath + ".bai")
	if err != nil {
		panic(err)
	}
	defer fidx.Close()

	bamindex, err := bam.ReadIndex(fidx)
	if err != nil {
		panic(err)
	}

	var RefsPos int
	for i, x := range (bamRead).Header().Refs() {
		if x.Name() == chr {
			RefsPos = i
			break
		}
	}
	chuck, err := bamindex.Chunks(bamRead.Header().Refs()[RefsPos], pos, end)
	if err != nil {
		panic(err)
	}

	return chuck[0].Begin
}
