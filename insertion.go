package bolt

import (
	"sync"

	"github.com/biogo/hts/sam"
)

func findInsertion(variantsC chan readContainer, calculateFinish chan bool, wgSV *sync.WaitGroup, jbs jobsSplitter, task *Task) {
	defer wgSV.Done()
	var eviCollect []evidentCollector

	SyncRecordFinish := make(chan bool)
	eviCollectBroadcast := make(chan evidentCollector)
	var wgSP sync.WaitGroup
	for i := 0; i < task.CallerConfig.MaxConcurrentOfSR; i++ {
		wgSP.Add(1)
		go findInsBreakPoint(eviCollectBroadcast, SyncRecordFinish, &wgSP, jbs, task)
	}
	createFile(task.OutputPath + temppath + "ins/" + jbs.CHROM + ".txt")
	FlagStat := []uint16{2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1}

	// var count int
	var count int
	var id int
	for {
		select {
		case variantList := <-variantsC:
			for m := 0; m < len(variantList.Read); m++ {
				count++

				//append all read to evidence
				for i := 0; i < len(eviCollect); i++ {
					// if eviCollect[i].FirstIn {
					eviCollect[i].Read = append(eviCollect[i].Read, variantList.Read...)
					// }
				}
				//find evidence
				for m := 0; m < len(variantList.Read); m++ {

					sizeEvidenceIS := len(eviCollect)
					for i := 0; i < sizeEvidenceIS; i++ {
						if eviCollect[i].POS+int(float32(task.sampleStats.InsertSizeMean)) < variantList.Read[m].Pos {
							if eviCollect[i].LEN >= 10 {

								eviCollectBroadcast <- eviCollect[i]
								// fmt.Println("BC-F", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							}
						}

						if len(eviCollect[i].Read) >= task.CallerConfig.LimitCacheReadPerEvidence {
							if eviCollect[i].LEN >= 2 {

								// fmt.Println("BC-F", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

								eviCollectBroadcast <- eviCollect[i]
								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							} else {
								// fmt.Println("BC-F-D", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							}
						}
					}
				}

				var FlagNextUnmapped bool
				TempFlag := uint16(variantList.Read[m].Flags)
				for i := 0; i < len(FlagStat); i++ {
					if TempFlag >= FlagStat[i] {
						TempFlag = TempFlag - FlagStat[i]
						if FlagStat[i] == 8 {
							FlagNextUnmapped = true
							// fmt.Println(variantList.Read[m].MatePos)
						}
					}
				}

				if FlagNextUnmapped {
					sizeEvidenceIS := len(eviCollect)
					var added bool
					for i := 0; i < sizeEvidenceIS; i++ {
						//grouping
						max := task.sampleStats.ReadLength + int(task.sampleStats.InsertSizeMean)
						if checkBetween(eviCollect[i].POS, variantList.Read[m].Pos, max, max) {
							if !eviCollect[i].Submited {
								eviCollect[i].LEN++
								eviCollect[i].MapQRP = append(eviCollect[i].MapQRP, int(variantList.Read[m].MapQ))

								added = true
								break
							}
						}
					}

					if !added {
						id++
						eviTemp := evidentCollector{ID: id, InsertSize: 0,
							CHR:     jbs.CHROM,
							POS:     variantList.Read[m].Pos,
							MATEPOS: 0,
							LEN:     1}
						eviTemp.MapQRP = append(eviTemp.MapQRP, int(variantList.Read[m].MapQ))
						eviTemp.Read = append(eviTemp.Read, variantList.Read...)
						eviCollect = append(eviCollect, eviTemp)
					}
				}

				if variantList.Read[m].Ref.Name() != variantList.Read[m].MateRef.Name() {
					continue
				}

				// IF NOT UNMAPPED
				if variantList.Read[m].Pos <= variantList.Read[m].MatePos && variantList.Read[m].Strand() == 1 {

					insertSize := getInsertSize(variantList.Read[m], &FlagStat, task)
					if !FlagNextUnmapped {
						if variantList.Read[m].Ref.Name() != variantList.Read[m].MateRef.Name() {
							continue
						}
						if variantList.Read[m].Pos < variantList.Read[m].MatePos {
							insertSize = getInsertSizeFromPosEnd(variantList.Read[m].Pos, variantList.Read[m].MatePos, task.sampleStats.ReadLength)

						} else if variantList.Read[m].Pos > variantList.Read[m].MatePos {
							insertSize = getInsertSizeFromPosEnd(variantList.Read[m].Pos, variantList.Read[m].MatePos, task.sampleStats.ReadLength)
						}
					}

					if insertSize <= task.sampleStats.InsertSizeMean-task.sampleStats.InsertSizeSD-50 || FlagNextUnmapped {
						sizeEvidenceIS := len(eviCollect)
						var added bool
						for i := 0; i < sizeEvidenceIS; i++ {
							//grouping
							max := task.sampleStats.ReadLength + int(task.sampleStats.InsertSizeMean)
							if checkBetween(eviCollect[i].POS, variantList.Read[m].Pos, max, max) {
								eviCollect[i].MATEPOS = variantList.Read[m].MatePos
								if !eviCollect[i].Submited {
									eviCollect[i].MapQRP = append(eviCollect[i].MapQRP, int(variantList.Read[m].MapQ))
									eviCollect[i].LEN++
									added = true
									break
								}
							}
						}

						if !added {
							id++
							eviTemp := evidentCollector{ID: id, InsertSize: insertSize,
								CHR:     jbs.CHROM,
								POS:     variantList.Read[m].Pos,
								MATEPOS: variantList.Read[m].MatePos,
								LEN:     1}
							eviTemp.MapQRP = append(eviTemp.MapQRP, int(variantList.Read[m].MapQ))
							eviTemp.Read = append(eviTemp.Read, variantList.Read...)
							eviCollect = append(eviCollect, eviTemp)
						}
					}
				}

			}

		case <-calculateFinish:
			for i := 0; i < task.CallerConfig.MaxConcurrentOfSR; i++ {
				SyncRecordFinish <- true
			}

			eviCollectBroadcast = nil
			wgSP.Wait()

			return
		}
	}
}

func findInsBreakPoint(variantsC chan evidentCollector, calculateFinish chan bool, wg *sync.WaitGroup, jbs jobsSplitter, task *Task) {
	defer wg.Done()

	FlagStat := []uint16{2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1}

	for {
		select {
		case evidence := <-variantsC:

			var FirstSegmentPos, LastSegmentPos []int
			for _, read := range evidence.Read {

				if evidence.MATEPOS == 0 {
					var FlagNextUnmapped bool
					var FlagFirstSegment bool
					var FlagLastSegment bool
					TempFlag := uint16(read.Flags)
					for i := 0; i < len(FlagStat); i++ {
						if TempFlag >= FlagStat[i] {
							TempFlag = TempFlag - FlagStat[i]
							if FlagStat[i] == 8 {
								FlagNextUnmapped = true
							} else if FlagStat[i] == 64 {
								FlagFirstSegment = true
							} else if FlagStat[i] == 128 {
								FlagLastSegment = true
							}
						}
					}

					if !FlagNextUnmapped {
						if read.Pos < read.Pos+500 {
							insertSize := int(((read.Pos + 500) + task.sampleStats.ReadLength) - read.Pos)
							//check if there insert size is abnormal
							if insertSize <= task.sampleStats.InsertSizeMean+50 {
								FirstSegmentPos = append(FirstSegmentPos, read.Pos)
							}
						} else if read.Pos > read.Pos+500 {
							insertSize := int((read.Pos + task.sampleStats.ReadLength - (read.Pos + 500)))
							if insertSize <= task.sampleStats.InsertSizeMean+50 {
								LastSegmentPos = append(LastSegmentPos, read.Pos)
							}
						}
					} else {
						if FlagFirstSegment && FlagNextUnmapped {
							FirstSegmentPos = append(FirstSegmentPos, read.Pos)
						} else if FlagLastSegment && FlagNextUnmapped {
							LastSegmentPos = append(LastSegmentPos, read.End())
						}
					}
				} else {
					var FlagNextUnmapped bool
					var FlagFirstSegment bool
					var FlagLastSegment bool
					TempFlag := uint16(read.Flags)
					for i := 0; i < len(FlagStat); i++ {
						if TempFlag >= FlagStat[i] {
							TempFlag = TempFlag - FlagStat[i]
							if FlagStat[i] == 8 {
								FlagNextUnmapped = true
							} else if FlagStat[i] == 64 {
								FlagFirstSegment = true
							} else if FlagStat[i] == 128 {
								FlagLastSegment = true
							}
						}
					}

					if !FlagNextUnmapped {
						if read.Pos < read.MatePos {
							insertSize := int((read.MatePos + task.sampleStats.ReadLength) - read.Pos)
							//check if there insert size is abnormal
							if insertSize <= task.sampleStats.InsertSizeMean+50 {
								FirstSegmentPos = append(FirstSegmentPos, read.Pos)
							}
						} else if read.Pos > read.MatePos {
							insertSize := int((read.Pos + task.sampleStats.ReadLength - read.MatePos))
							if insertSize <= task.sampleStats.InsertSizeMean+50 {
								LastSegmentPos = append(LastSegmentPos, read.Pos)
							}
						}
					} else {
						if FlagFirstSegment && FlagNextUnmapped {
							FirstSegmentPos = append(FirstSegmentPos, read.Pos)
						} else if FlagLastSegment && FlagNextUnmapped {
							LastSegmentPos = append(LastSegmentPos, read.End())
						}
					}
				}

			}

			FirstSegmentPosTotal := 0
			countFirstSegment := 0
			for _, x := range FirstSegmentPos {
				FirstSegmentPosTotal = FirstSegmentPosTotal + x
				countFirstSegment++
			}

			LastSegmentPosTotal := 0
			countLastSegment := 0
			for _, x := range LastSegmentPos {
				LastSegmentPosTotal = LastSegmentPosTotal + x
				countLastSegment++
			}

			if countFirstSegment == 0 || countLastSegment == 0 {
				continue
			}

			AvgFirstSegmentPos := FirstSegmentPosTotal / countFirstSegment
			AvgLastSegmentPos := LastSegmentPosTotal / countLastSegment
			var orderhit []int
			_, rUncapture := getReadPileupPoint(evidence.Read, orderhit, (AvgFirstSegmentPos+AvgLastSegmentPos)/2)

			bpStart := make(map[int]int)
			bpEnd := make(map[int]int)

			var tempRuncaptureSC []*sam.Record
			for _, read := range rUncapture {
				if read.Cigar[0].Type().String() == "S" {
					if read.Cigar[0].Len() > 4 {
						bpStart[read.Pos]++
						tempRuncaptureSC = append(tempRuncaptureSC, read)
					}
				} else if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {
					if read.Cigar[len(read.Cigar)-1].Len() > 4 {
						bpEnd[read.End()]++
						tempRuncaptureSC = append(tempRuncaptureSC, read)
					}
				}
			}

			// Find Pos from max count pos
			var bpStartCount, bpStartPos int
			for pos, count := range bpStart {
				if bpStartCount < count {
					bpStartPos = pos
					bpStartCount = count
				}
			}

			// Find End from max count end
			var bpEndCount, bpEndPos int
			for pos, count := range bpEnd {
				if bpEndCount < count {
					bpEndPos = pos
					bpEndCount = count
				}
			}

			// Find average position between pos and end
			avgBPPos := (bpStartPos + bpEndPos) / 2

			var scPointReadPos []*sam.Record
			var scPointReadEnd []*sam.Record

			var MaxMapQ int
			MinMapQ := 1000
			var sumMapQ int
			var hit int
			for _, read := range tempRuncaptureSC {
				if read.Cigar[0].Type().String() == "S" {
					if read.Pos == bpStartPos {
						scPointReadPos = append(scPointReadPos, read)
						if MaxMapQ < int(read.MapQ) {
							MaxMapQ = int(read.MapQ)
						}
						if MinMapQ > int(read.MapQ) {
							MinMapQ = int(read.MapQ)
						}
						if MinMapQ == 1000 {
							MinMapQ = 0
						}
						sumMapQ += int(read.MapQ)
						hit++
					}
				} else if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {
					if read.End() == bpStartPos {
						scPointReadEnd = append(scPointReadEnd, read)
						if MaxMapQ < int(read.MapQ) {
							MaxMapQ = int(read.MapQ)
						}
						if MinMapQ > int(read.MapQ) {
							MinMapQ = int(read.MapQ)
						}
						if MinMapQ == 1000 {
							MinMapQ = 0
						}
						sumMapQ += int(read.MapQ)
						hit++
					}
				}
			}

			if getSumScSide(tempRuncaptureSC, "F") == 0 {
				break
			}
			if getSumScSide(tempRuncaptureSC, "L") == 0 {
				break
			}

			if getSumScSide(tempRuncaptureSC, "F") >= 3 && getSumScSide(tempRuncaptureSC, "L") >= 3 && hit >= 6 {
				var pt printTrain
				pt.Orientation = "F"
				pt.Chr = evidence.CHR
				pt.Pos = avgBPPos
				pt.ChrEnd = evidence.CHR
				pt.End = evidence.MATEPOS
				pt.Hit = hit
				pt.ReadDepth = len(rUncapture)
				pt.CaptureMapQMax = MaxMapQ
				pt.CaptureMapQMin = MinMapQ
				pt.CaptureMapQAvg = int(sumMapQ / pt.Hit)
				pt.ReadPairCount = evidence.LEN
				pt.SVType = "INS"
				pt.MatchOrientation = ""
				// pt.MapQRP = evidence.MapQRP

				pt.MaxMapQRP = getMaxIntArray(evidence.MapQRP)
				pt.AvgMapQRP = getAvgIntArray(evidence.MapQRP)
				pt.MinMapQRP = getMinIntArray(evidence.MapQRP)

				printTrainData(pt, task.OutputPath+temppath+"ins/"+jbs.CHROM)
			}

		case <-calculateFinish:
			return
		}
	}
}
