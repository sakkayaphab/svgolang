package bolt

type iNDEL struct {
	CHR     string
	POS     int
	REF     string
	ALT     string
	SVTYPE  string
	END     int
	POSREAD int
	ENDREAD int
}

type indelCount struct {
	INDEL iNDEL
	Len   int
}

// func calculateINDELRead(variantsC chan []*read, calculateFinish chan bool, wg *sync.WaitGroup, jbs jobsSplitter, task *Task) {
// 	defer wg.Done()
// 	wg.Add(1)
// 	var indelcount []indelCount
// 	var vcf []vCFFormat
// 	var first bool
// 	var EndPosRead int
// 	var count int
// 	var found bool
// 	createFile(task.OutputPath + "/indel/" + jbs.CHROM)

// 	for {
// 		select {
// 		case variantList := <-variantsC:
// 			for m := 0; m < len(variantList); m++ {
// 				var skipCigarSEQ int
// 				var skipPos int
// 				var indel []iNDEL

// 				for i := 0; i < len(variantList[m].Rec.Cigar); i++ {
// 					if variantList[m].Rec.Cigar[i].Type().String() == "I" {
// 						seq := string(variantList[m].Rec.Seq.Expand())[skipCigarSEQ : skipCigarSEQ+variantList[m].Rec.Cigar[i].Len()]
// 						indel = append(indel, iNDEL{variantList[m].Rec.Ref.Name(), variantList[m].Rec.Pos + skipPos, "", seq, "INS", variantList[m].Rec.Pos + skipPos + variantList[m].Rec.Cigar[i].Len(), variantList[m].Rec.Pos, variantList[m].Rec.End()})
// 						skipCigarSEQ = skipCigarSEQ + variantList[m].Rec.Cigar[i].Len()
// 						skipPos = skipPos + variantList[m].Rec.Cigar[i].Len()
// 					} else if variantList[m].Rec.Cigar[i].Type().String() == "D" {
// 						// fmt.Println(rec, skipCigar)
// 						seq := string(variantList[m].Rec.Seq.Expand())[skipCigarSEQ-1 : skipCigarSEQ]
// 						indel = append(indel, iNDEL{variantList[m].Rec.Ref.Name(), variantList[m].Rec.Pos + skipPos, "", seq, "DEL", variantList[m].Rec.Pos + skipPos + variantList[m].Rec.Cigar[i].Len(), variantList[m].Rec.Pos, variantList[m].Rec.End()})
// 						skipPos = skipPos + variantList[m].Rec.Cigar[i].Len()
// 					} else if variantList[m].Rec.Cigar[i].Type().String() == "H" {

// 					} else if i == 0 && variantList[m].Rec.Cigar[i].Type().String() == "S" {
// 						skipCigarSEQ = skipCigarSEQ + variantList[m].Rec.Cigar[i].Len()
// 					} else {
// 						skipCigarSEQ = skipCigarSEQ + variantList[m].Rec.Cigar[i].Len()
// 						skipPos = skipPos + variantList[m].Rec.Cigar[i].Len()
// 					}
// 				}

// 				//Merge SNV
// 				if len(indel) != 0 {
// 					for nn := 0; nn < len(indel); nn++ {
// 						// fmt.Println(len(indelcount), len(vcf))
// 						// fmt.Println(indel, len(indelcount), EndPosRead)
// 						// found = true
// 						found = false
// 						if !first {
// 							indelcount = append(indelcount, indelCount{indel[nn], 1})
// 							first = true
// 							// EndPosRead = indel.POSREAD + Readlength
// 							EndPosRead = indel[nn].ENDREAD
// 							count++
// 							continue
// 						}

// 						if indel[nn].POS > EndPosRead {
// 							vcf = append(vcf, saveIndelIntoVCF(&indelcount, task)...)
// 							indelcount = []indelCount{}
// 							// fmt.Println(">>>>>>")
// 							if count >= 100 {
// 								writeVCF(task.OutputPath+"/indel/"+jbs.CHROM, &vcf)
// 								vcf = []vCFFormat{}
// 								// vcf = vcf[:0]
// 							}

// 							// indelcount = indelcount[:0]
// 							//new Append
// 							indelcount = append(indelcount, indelCount{indel[nn], 1})
// 							// EndPosRead = indel.POSREAD + Readlength
// 							EndPosRead = indel[nn].ENDREAD
// 							count = len(vcf)
// 						}

// 						for r := 0; r < len(indelcount); r++ {
// 							if indelcount[r].INDEL.POS == indel[nn].POS && indelcount[r].INDEL.END == indel[nn].END && indelcount[r].INDEL.SVTYPE == indel[nn].SVTYPE {
// 								indelcount[r].Len++
// 								indelcount[r].INDEL.POSREAD = indel[nn].POSREAD
// 								// EndPosRead = indel.POSREAD + Readlength
// 								EndPosRead = indel[nn].ENDREAD
// 								found = true
// 								break
// 							}
// 						}

// 						if !found {
// 							indelcount = append(indelcount, indelCount{indel[nn], 1})
// 							count++
// 						}
// 					}
// 				}

// 			}
// 		case <-calculateFinish:
// 			vcf = append(vcf, saveIndelIntoVCF(&indelcount, task)...)

// 			writeVCF(task.OutputPath+"/indel/"+jbs.CHROM, &vcf)
// 			return
// 		}
// 	}
// }

// func checkINDEL(indelC chan *iNDEL, rec *sam.Record) {
// 	var skipCigarSEQ int
// 	var skipPos int
// 	var indel []iNDEL

// 	for i := 0; i < len(rec.Cigar); i++ {
// 		if rec.Cigar[i].Type().String() == "I" {
// 			seq := string(rec.Seq.Expand())[skipCigarSEQ : skipCigarSEQ+rec.Cigar[i].Len()]
// 			indel = append(indel, iNDEL{rec.Ref.Name(), rec.Pos + skipPos, "", seq, "INS", rec.Pos + skipPos + rec.Cigar[i].Len(), rec.Pos, rec.End()})
// 			skipCigarSEQ = skipCigarSEQ + rec.Cigar[i].Len()
// 			skipPos = skipPos + rec.Cigar[i].Len()
// 		} else if rec.Cigar[i].Type().String() == "D" {
// 			// fmt.Println(rec, skipCigar)
// 			seq := string(rec.Seq.Expand())[skipCigarSEQ-1 : skipCigarSEQ]
// 			indel = append(indel, iNDEL{rec.Ref.Name(), rec.Pos + skipPos, "", seq, "DEL", rec.Pos + skipPos + rec.Cigar[i].Len(), rec.Pos, rec.End()})
// 			skipPos = skipPos + rec.Cigar[i].Len()
// 		} else if rec.Cigar[i].Type().String() == "H" {

// 		} else if i == 0 && rec.Cigar[i].Type().String() == "S" {
// 			skipCigarSEQ = skipCigarSEQ + rec.Cigar[i].Len()
// 		} else {
// 			skipCigarSEQ = skipCigarSEQ + rec.Cigar[i].Len()
// 			skipPos = skipPos + rec.Cigar[i].Len()
// 		}
// 	}

// 	if len(indel) != 0 {
// 		for i := 0; i < len(indel); i++ {
// 			indelC <- &indel[i]
// 		}
// 	}
// }

// func saveIndelIntoVCF(indelcount *[]indelCount, task *Task) []vCFFormat {
// 	var vcf []vCFFormat
// 	sort.Slice(*indelcount, func(i, j int) bool { return (*indelcount)[i].INDEL.POS < (*indelcount)[j].INDEL.POS })

// 	for i := 0; i < len(*indelcount); i++ {
// 		//set Filter indel
// 		if (*indelcount)[i].Len >= 4 {
// 			var REF string

// 			if (*indelcount)[i].INDEL.SVTYPE == "DEL" {
// 				REF = GetSequence((*indelcount)[i].INDEL.CHR, (*indelcount)[i].INDEL.POS, (*indelcount)[i].INDEL.END, task)
// 			} else {
// 				REF = GetSequence((*indelcount)[i].INDEL.CHR, (*indelcount)[i].INDEL.POS, (*indelcount)[i].INDEL.POS+1, task)
// 			}

// 			INFO := "SVTYPE=" + (*indelcount)[i].INDEL.SVTYPE + ";END=" + strconv.Itoa((*indelcount)[i].INDEL.END) + ";SVLEN=" + strconv.Itoa((*indelcount)[i].INDEL.END-(*indelcount)[i].INDEL.POS)
// 			vcf = append(vcf, vCFFormat{(*indelcount)[i].INDEL.CHR, (*indelcount)[i].INDEL.POS, ".", REF, (*indelcount)[i].INDEL.ALT, -1, ".", INFO})
// 		}
// 	}
// 	return vcf
// }

// func getNewSEQ(rec *sam.Record) *string {
// 	var newSEQ string
// 	var seqPos int
// 	var decrease int
// 	// fmt.Println(string(rec.Seq.Expand()), rec.Cigar.String())
// 	for i := 0; i < len(rec.Cigar); i++ {
// 		if rec.Cigar[i].Type().String() == "D" {
// 			for j := 0; j < rec.Cigar[i].Len(); j++ {
// 				newSEQ += "D"
// 				decrease++
// 			}
// 		} else if rec.Cigar[i].Type().String() == "H" {

// 		} else {
// 			newSEQ += string(rec.Seq.Expand())[seqPos : seqPos+rec.Cigar[i].Len()]
// 		}
// 	}
// 	return &newSEQ
// }
