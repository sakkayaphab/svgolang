# Bolt
<!-- [![Latest version of bolt](https://img.shields.io/badge/bolt-v0.8.13-blue.svg)](https://github.com/sakkayaphab/bolt) -->
[![GoDoc](https://img.shields.io/badge/go-documentation-blue.svg)](https://godoc.org/github.com/sakkayaphab/bolt)
[![License](https://img.shields.io/packagist/l/doctrine/orm.svg)](https://github.com/sakkayaphab/bolt/blob/master/LICENSE)
[![Go Report Card](https://goreportcard.com/badge/github.com/sakkayaphab/bolt)](https://goreportcard.com/report/github.com/sakkayaphab/bolt)
[![Build Status](https://travis-ci.org/sakkayaphab/bolt.svg?branch=master)](https://travis-ci.org/sakkayaphab/bolt)

## ----------------------------
## this project move to https://github.com/sakkayaphab/bolt
## ----------------------------


## Installation
Install  [Go environment]

To build and run

```sh
go get -u github.com/sakkayaphab/bolt
cd $GOPATH/src/github.com/sakkayaphab/bolt
make
./bolt
```



## Usage

```
bolt all -b {bam file} -r {reference file} -o {output directory}
```

For more information, see `bolt --help`.

## License

[MIT](LICENSE)


[Go environment]: https://golang.org/doc/install
[Bolt release page]: https://golang.org/doc/install


