package main

// package main

import (
	"fmt"
	"log"
	"os"

	"github.com/sakkayaphab/boltgo"
	"github.com/urfave/cli"
)

func main() {
	// runtime.GOMAXPROCS(60)

	app := cli.NewApp()
	app.Name = "bolt"
	app.Usage = "a variant caller for structural variantion"
	app.Version = "0.8.14"

	app.Commands = []cli.Command{
		{
			Name:  "all",
			Usage: "predict all SV types",
			Flags: []cli.Flag{
				cli.StringSliceFlag{Name: "b", Usage: "input bam file"},
				cli.StringSliceFlag{Name: "r", Usage: "input reference file"},
				cli.StringSliceFlag{Name: "o", Usage: "output directory"},
			},
			Action: func(c *cli.Context) error {

				task := bolt.New()
				bamfile := c.String("b")
				fastafile := c.String("r")
				output := "temp"

				if c.String("o") == "[]" {
					output = "temp"
				} else {
					output = c.String("o")
					output = output[1 : len(output)-1]
				}
				bamfile = bamfile[1 : len(bamfile)-1]
				fastafile = fastafile[1 : len(fastafile)-1]

				if bamfile == "" {
					log.Fatal("No BAM file")
					return nil
				}
				if fastafile == "" {
					log.Fatal("No FASTA file")
					return nil
				}

				task.SamplePath = bamfile
				task.ReferencePath = fastafile
				if output == "" {
					fmt.Println("Output emtry")
					return nil
				}
				task.OutputPath = output

				task.Execute()

				return nil
			},
		}, {
			Name:  "train",
			Usage: "train a new model",
			Flags: []cli.Flag{
				cli.StringSliceFlag{Name: "o", Usage: "output directory"},
			},
			Action: func(c *cli.Context) error {

				task := bolt.New()
				output := "temp"
				task.OutputPath = output
				task.ExecuteDebug()

				return nil
			},
		}, {
			Name:  "debug",
			Usage: "debug mode for developer",
			Flags: []cli.Flag{
				cli.StringSliceFlag{Name: "o", Usage: "output directory"},
			},
			Action: func(c *cli.Context) error {

				task := bolt.New()
				output := "temp"
				task.OutputPath = output
				task.ExecuteDebug()

				return nil
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
