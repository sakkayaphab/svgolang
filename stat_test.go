package bolt

import "testing"

func TestGETINSERTSIZEFROMPOSEND(t *testing.T) {
	tables := []struct {
		pos        int
		end        int
		readlength int
		Expected   int
	}{
		{100000, 101000, 100, 1100},
		{99000, 100000, 100, 1100},
	}

	for _, table := range tables {
		total := getInsertSizeFromPosEnd(table.pos, table.end, table.readlength)
		if total != table.Expected {
			t.Errorf("reciprocal validation was incorrect, pos: %d,%d,%d, Expected: %d", table.pos, table.end, table.readlength, table.Expected)
		}
	}
}
