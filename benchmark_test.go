package bolt

import "testing"

func TestRECIPROCALVALIDATION(t *testing.T) {
	tables := []struct {
		Pos        int
		End        int
		overlapped int
		TargetPos  int
		TargetEnd  int
		Expected   bool
	}{
		{10000, 20000, 70, 10000, 20000, true},

		{12000, 21000, 70, 10000, 20000, true},
		{15000, 25000, 70, 10000, 20000, false},

		{10000, 20000, 70, 12000, 21000, true},
		{5000, 14000, 70, 12000, 21000, false},

		{10000, 20000, 70, 12000, 19000, true},
		{10000, 25000, 70, 12000, 19000, false},

		{12000, 19000, 70, 10000, 20000, true},
		{14000, 15000, 70, 10000, 20000, false},
	}

	for _, table := range tables {
		total := reciprocalValidation(table.Pos, table.End, table.overlapped, table.TargetPos, table.TargetEnd)
		if total != table.Expected {
			t.Errorf("reciprocal validation was incorrect, pos: %d, end: %d, overlapped:%d, TargetPos:%d, TargetEnd:%d", table.Pos, table.End, table.overlapped, table.TargetPos, table.TargetEnd)
		}
	}
}

func TestGETONLYCHRPOSITION(t *testing.T) {
	tables := []struct {
		text             string
		expectedChr      string
		expectedPosition int
	}{
		{"G[chr1:9978787[", "chr1", 9978787},
		{"]chr11:77433705]T", "chr11", 77433705},
	}

	for _, table := range tables {
		chr, pos := getTRAOnlyChrPosition(table.text)
		if chr != table.expectedChr && pos != table.expectedPosition {
			t.Errorf("reciprocal validation was incorrect, text: %s, chr: %s, pos:%d but return %s %d", table.text, table.expectedChr, table.expectedPosition, chr, pos)
		}
	}
}
