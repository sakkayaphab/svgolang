package bolt

import (
	"fmt"
	"sort"
	"strings"
	"sync"
)

func findInversion(variantsC chan readContainer, calculateFinish chan bool, wgSV *sync.WaitGroup, jbs jobsSplitter, task *Task) {
	defer wgSV.Done()
	FlagStat := []uint16{2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1}
	var eviCollect []evidentCollector
	SyncRecordFinish := make(chan bool)
	eviCollectBroadcast := make(chan evidentCollector)
	var wgSP sync.WaitGroup
	var muResult sync.Mutex

	for i := 0; i < task.CallerConfig.MaxConcurrentOfSR; i++ {
		wgSP.Add(1)
		go findINVBreakPoint(eviCollectBroadcast, SyncRecordFinish, &wgSP, jbs, task, &muResult)
	}
	createFile(task.OutputPath + temppath + "inv/" + jbs.CHROM + ".txt")
	for {
		select {
		case variantList := <-variantsC:

			//append all read to evidence
			for i := 0; i < len(eviCollect); i++ {
				// if eviCollect[i].FirstIn {
				eviCollect[i].Read = append(eviCollect[i].Read, variantList.Read...)
				// }
			}

			for m := 0; m < len(variantList.Read); m++ {

				//find evidence
				for m := 0; m < len(variantList.Read); m++ {
					// if FlagMultiSegment {
					sizeEvidenceIS := len(eviCollect)
					for i := 0; i < sizeEvidenceIS; i++ {
						if eviCollect[i].FirstIn {
							if eviCollect[i].POS+int(float32(task.sampleStats.InsertSizeMean)) < variantList.Read[m].Pos {
								if eviCollect[i].LEN >= 6 && eviCollect[i].InsertSize > 1000 {

									eviCollectBroadcast <- eviCollect[i]
									// fmt.Println("BC-F", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

									eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
									sizeEvidenceIS--
									i--
									continue
								} else if eviCollect[i].LEN >= 10 && eviCollect[i].InsertSize <= 1000 {
									eviCollectBroadcast <- eviCollect[i]
									// fmt.Println("BC-F", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

									eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
									sizeEvidenceIS--
									i--
									continue
								}

							}

							if len(eviCollect[i].Read) >= task.CallerConfig.LimitCacheReadPerEvidence {
								if eviCollect[i].LEN >= 2 {

									// fmt.Println("BC-F", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

									eviCollectBroadcast <- eviCollect[i]
									eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
									sizeEvidenceIS--
									i--
									continue
								} else if eviCollect[i].LEN >= 2 && eviCollect[i].InsertSize > 1000 {

									// fmt.Println("BC-F", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

									eviCollectBroadcast <- eviCollect[i]
									eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
									sizeEvidenceIS--
									i--
									continue
								} else {
									eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
									sizeEvidenceIS--
									i--
									continue
								}

							}

							// Lastin
						} else {
							if eviCollect[i].POS+int(float32(task.sampleStats.ReadLength)*2) < variantList.Read[m].Pos {
								if eviCollect[i].LEN >= 2 && eviCollect[i].InsertSize > 1000 {

									eviCollectBroadcast <- eviCollect[i]

									// fmt.Println("BC-L", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].POS-eviCollect[i].MATEPOS, eviCollect[i].InsertSize)

									eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
									sizeEvidenceIS--
									i--
									continue
								} else if eviCollect[i].LEN >= 2 && eviCollect[i].InsertSize <= 1000 {
									eviCollectBroadcast <- eviCollect[i]

									// fmt.Println("BC-L", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].POS-eviCollect[i].MATEPOS, eviCollect[i].InsertSize)

									eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
									sizeEvidenceIS--
									i--
									continue
								}
							}

							if len(eviCollect[i].Read) >= task.CallerConfig.LimitCacheReadPerEvidence {
								if eviCollect[i].LEN >= 2 {
									eviCollectBroadcast <- eviCollect[i]
									// fmt.Println("BC-L", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].POS-eviCollect[i].MATEPOS)

									eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
									sizeEvidenceIS--
									i--
									continue

								} else {
									eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
									sizeEvidenceIS--
									i--
									continue
								}
							}
						}
					}
				}

				//Find Unmapped Read
				var FlagBeingReverse bool
				var FlagNextReverse bool
				var FlagFirstSegment bool
				var FlagLastSegment bool

				TempFlag := uint16(variantList.Read[m].Flags)
				for i := 0; i < len(FlagStat); i++ {
					if TempFlag >= FlagStat[i] {
						TempFlag = TempFlag - FlagStat[i]
						if FlagStat[i] == 64 {
							FlagFirstSegment = true
						} else if FlagStat[i] == 128 {
							FlagLastSegment = true
						} else if FlagStat[i] == 16 {
							FlagBeingReverse = true
						} else if FlagStat[i] == 32 {
							FlagNextReverse = true
						} else if FlagStat[i] == 1 {
							// FlagMultiSegment = true
						}
					}
				}

				if variantList.Read[m].Pos < variantList.Read[m].MatePos && FlagFirstSegment {
					if FlagBeingReverse == FlagNextReverse {
						var added bool
						max := 2000
						for i := 0; i < len(eviCollect); i++ {
							if checkBetween(eviCollect[i].POS, variantList.Read[m].Pos, max, max) && checkBetween(eviCollect[i].MATEPOS, variantList.Read[m].MatePos, max, max) {
								if !eviCollect[i].FirstIn {
									continue
								}
								if !eviCollect[i].Submited {
									eviCollect[i].LEN++
									eviCollect[i].MapQRP = append(eviCollect[i].MapQRP, int(variantList.Read[m].MapQ))
									added = true
									break
								}
							}
						}

						if !added {
							if !isInLimitRange(variantList.Read[m].Pos, variantList.Read[m].MatePos) {
								continue
							}
							eviTemp := evidentCollector{
								CHR:     jbs.CHROM,
								POS:     variantList.Read[m].Pos,
								MATEPOS: variantList.Read[m].MatePos,
								LEN:     1}
							eviTemp.FirstIn = true
							eviTemp.MapQRP = append(eviTemp.MapQRP, int(variantList.Read[m].MapQ))

							eviTemp.Read = append(eviTemp.Read, variantList.Read...)
							eviCollect = append(eviCollect, eviTemp)
						}
					}
				} else {
					if FlagBeingReverse == FlagNextReverse && FlagLastSegment {
						var added bool
						max := 2000
						for i := 0; i < len(eviCollect); i++ {
							if checkBetween(eviCollect[i].POS, variantList.Read[m].Pos, max, max) && checkBetween(eviCollect[i].MATEPOS, variantList.Read[m].MatePos, max, max) {
								if eviCollect[i].FirstIn {
									continue
								}
								if !eviCollect[i].Submited {
									eviCollect[i].LEN++
									eviCollect[i].MapQRP = append(eviCollect[i].MapQRP, int(variantList.Read[m].MapQ))
									added = true
									break
								}
							}
						}

						if !added {
							if !isInLimitRange(variantList.Read[m].Pos, variantList.Read[m].MatePos) {
								continue
							}
							eviTemp := evidentCollector{
								CHR:     jbs.CHROM,
								POS:     variantList.Read[m].Pos,
								MATEPOS: variantList.Read[m].MatePos,
								LEN:     1}
							eviTemp.MapQRP = append(eviTemp.MapQRP, int(variantList.Read[m].MapQ))
							eviTemp.Read = append(eviTemp.Read, variantList.Read...)
							eviCollect = append(eviCollect, eviTemp)
						}
					}
				}
			}
		case <-calculateFinish:
			for i := 0; i < task.CallerConfig.MaxConcurrentOfSR; i++ {
				SyncRecordFinish <- true
			}

			eviCollectBroadcast = nil
			wgSP.Wait()

			return
		}
	}
}

func findINVBreakPoint(variantsC chan evidentCollector, calculateFinish chan bool, wg *sync.WaitGroup, jbs jobsSplitter, task *Task, muResult *sync.Mutex) {
	defer wg.Done()
	var count int
	var tempListPT []printTrain
	for {
		select {
		case variant := <-variantsC:
			// if variant.POS != 25835068 {
			// 	continue
			// }
			// fmt.Println("BC-L", variant.POS, variant.MATEPOS, "Read:", len(variant.Read), "LEN", variant.LEN, "INS", variant.POS-variant.MATEPOS, variant.InsertSize)

			var ptlist []printTrain
			if variant.FirstIn {
				ptlist = findINVBreakPointFirstIn(&variant, jbs, task)
			} else {
				ptlist = findINVBreakPointLastIn(&variant, jbs, task)
			}

			if ptlist != nil {
				tempListPT = append(tempListPT, ptlist...)
				count++
			}

			if count > 20 {
				printTrainDataList(tempListPT, task.OutputPath+temppath+"inv/"+jbs.CHROM, muResult)
				tempListPT = nil
				count = 0
			}

		case <-calculateFinish:
			printTrainDataList(tempListPT, task.OutputPath+temppath+"inv/"+jbs.CHROM, muResult)
			tempListPT = nil
			count = 0
			return
		}
	}
}

func findINVBreakPointLastIn(evidence *evidentCollector, jbs jobsSplitter, task *Task) []printTrain {

	var startPosAlign, endPosAlign int
	startPosAlign = evidence.MATEPOS - task.sampleStats.ReadLength*3 - int(task.sampleStats.InsertSizeSD)
	endPosAlign = evidence.MATEPOS + task.sampleStats.ReadLength*4 + int(task.sampleStats.InsertSizeSD)

	if endPosAlign > evidence.POS {
		endPosAlign = evidence.POS - task.sampleStats.ReadLength
	}

	if startPosAlign < 0 {
		startPosAlign = 0
	}

	maxReferenceEND := task.getReferenceEND(evidence.CHR)

	if maxReferenceEND <= 0 {
		return nil
	}

	if endPosAlign > maxReferenceEND {
		endPosAlign = maxReferenceEND
	}

	if endPosAlign <= 0 {
		return nil
	}

	if endPosAlign-startPosAlign < 100 {
		fmt.Println("#CODE0x1INV: endPosAlign-startPosAlign < ?")
		return nil
	}

	seq, err := task.fastaReader.GetSequence(evidence.CHR, startPosAlign, endPosAlign)
	if err != nil {
		return nil
	}
	seq = strings.ToUpper(seq)
	bwt := newBWT()

	bwt.Transform(seq)
	bp := make(map[simpleBreakpoint]breakpointinfo)

	for orderhit, read := range evidence.Read {

		if len(read.Cigar) < 2 {
			continue
		}

		if getNumberSoftClippedLast(read) >= 3 {
			score := bwt.locateReadINV(read, getNumberSoftClippedLast(read))

			for _, x := range score {

				if x.HIT >= 4 {

				} else {
					continue
				}
				// fmt.Println("SCL", read, x, startPosAlign, endPosAlign, ">", startPosAlign+x.Pos+x.HIT, read.End()+1)

				oneBP := bp[simpleBreakpoint{startPosAlign + x.Pos + x.HIT, read.End() + 1}]
				if read.End()+1 < startPosAlign+x.Pos+x.HIT {
					continue
					// fmt.Println("SCL", read, x, startPosAlign, endPosAlign, ">", startPosAlign+x.Pos+x.HIT, read.End()+1)
				}

				oneBP.hit++
				oneBP.longhit = append(oneBP.longhit, x.HIT)
				oneBP.softclippedhit = append(oneBP.softclippedhit, getNumberSoftClippedFirst(read))
				oneBP.matchright = true
				oneBP.orderhit = append(oneBP.orderhit, orderhit)

				bp[simpleBreakpoint{startPosAlign + x.Pos + x.HIT, read.End() + 1}] = oneBP
				// break
			}
		} else if getNumberSoftClippedFirst(read) >= 3 {
			score := bwt.locateReverseReadINV(read, getNumberSoftClippedFirst(read))

			for _, x := range score {

				if x.HIT >= 4 {

				} else {
					continue
				}
				// fmt.Println("SCF", read, x, startPosAlign, endPosAlign, ":", endPosAlign-x.Pos-x.HIT, read.Pos+1)

				oneBP := bp[simpleBreakpoint{endPosAlign - x.Pos - x.HIT, read.Pos + 1}]
				if endPosAlign-x.Pos-x.HIT > read.Pos+1 {
					continue
				}
				// fmt.Println("SCF", read, x, startPosAlign, endPosAlign, ":", endPosAlign-x.Pos-x.HIT, read.Pos+1)

				oneBP.hit++
				oneBP.longhit = append(oneBP.longhit, x.HIT)
				oneBP.softclippedhit = append(oneBP.softclippedhit, getNumberSoftClippedLast(read))
				oneBP.orderhit = append(oneBP.orderhit, orderhit)

				bp[simpleBreakpoint{endPosAlign - x.Pos - x.HIT, read.Pos + 1}] = oneBP
				// break
			}
		}

	}

	var bks []breakPointState
	for i, x := range bp {
		if checkAddBreakpointState(&x, "INV") {
			bks = append(bks, breakPointState{
				POS:            i.POS,
				END:            i.END,
				HIT:            x.hit,
				INSERTSIZE:     i.END - i.POS,
				ALLDEPTH:       x.alldepth,
				longhit:        x.longhit,
				softclippedhit: x.softclippedhit,
				orderhit:       x.orderhit,
				ReadPairCount:  evidence.LEN,
				matchleft:      x.matchleft,
				matchright:     x.matchright,
			})
		}
	}

	sort.Slice(bks, func(i, j int) bool { return (bks)[i].HIT > (bks)[j].HIT })
	var PTresults []printTrain
	for _, x := range bks {
		if x.HIT >= 2 {

		} else {
			continue
		}

		if getMaxIntArray(x.longhit) < 6 {
			continue
		}

		// if !isInLimitRange(x.POS, x.END) {
		// 	fmt.Println("#CODE0x2INVLast", "isInLimitRange INV", x.POS, x.END)
		// 	continue
		// }

		x.SVType = "INV"
		x.MapQRP = evidence.MapQRP
		x.Orientation = "L"

		aPT := printPileUp(evidence, &x, &jbs, task)
		PTresults = append(PTresults, aPT)

	}

	return PTresults
}

func findINVBreakPointFirstIn(evidence *evidentCollector, jbs jobsSplitter, task *Task) []printTrain {

	var startPosAlign, endPosAlign int
	// Align
	endPosAlign = evidence.MATEPOS + task.sampleStats.ReadLength*6 + int(task.sampleStats.InsertSizeSD)
	startPosAlign = evidence.MATEPOS - task.sampleStats.ReadLength - int(task.sampleStats.InsertSizeSD)
	if startPosAlign < evidence.POS {
		startPosAlign = evidence.POS
	}

	if startPosAlign < 0 {
		startPosAlign = 0
	}

	maxReferenceEND := task.getReferenceEND(evidence.CHR)

	if maxReferenceEND <= 0 {
		return nil
	}

	if endPosAlign > maxReferenceEND {
		endPosAlign = maxReferenceEND
	}

	if startPosAlign < 0 {
		startPosAlign = 0
	}

	if startPosAlign > endPosAlign {
		return nil
	}

	if endPosAlign-startPosAlign < 100 {
		fmt.Println("#CODE0x1INV: endPosAlign-startPosAlign < ?")
		return nil
	}

	seq, err := task.fastaReader.GetSequence(evidence.CHR, startPosAlign, endPosAlign)
	if err != nil {
		return nil
	}
	seq = strings.ToUpper(seq)
	bwt := newBWT()

	// if 94126164 != startPosAlign {
	// 	return
	// }

	// fmt.Println(evidence.CHR, startPosAlign, endPosAlign)

	bwt.Transform(seq)
	bp := make(map[simpleBreakpoint]breakpointinfo)

	for orderhit, read := range evidence.Read {

		if len(read.Cigar) < 2 {
			continue
		}

		if len(read.Seq.Seq) == 0 {
			continue
		}

		// if read.Pos < evidence.POS-task.sampleStats.ReadLength {
		// 	continue
		// }

		// if read.Pos > evidence.POS+task.sampleStats.ReadLength*6 {
		// 	continue
		// }

		// plusFirstSoftclipped := 10
		// if read.Cigar[0].Type().String() == "S" {
		// 	plusFirstSoftclipped = read.Cigar[0].Len()
		// }

		// sizeSoftClipped := 0
		// if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {
		// 	sizeSoftClipped = read.Cigar[len(read.Cigar)-1].Len()
		// }

		// score := bwt.locateReverseReadINV(read, plusFirstSoftclipped)
		var score []bWTScore
		if getNumberSoftClippedFirst(read) >= 3 {
			// fmt.Println(read)
			score = bwt.locateReverseReadINV(read, getNumberSoftClippedFirst(read))

			for _, x := range score {

				if x.HIT >= 4 {

				} else {
					continue
				}

				// fmt.Println(string(read.Seq.Expand()))
				// fmt.Println("---->", read.Pos, endPosAlign-x.Pos-x.HIT+1, x)
				// if read.Seq.Length-x.HIT+read.Pos-plusFirstSoftclipped == 248849865 {
				// 	fmt.Println(read)
				// }
				// bp[simpleBreakpoint{read.Rec.Seq.Length - x.HIT + read.Rec.Pos - plusFirstSoftclipped, startPosAlign + x.Pos + 1}]++

				// fmt.Println("F", read.Pos, endPosAlign-x.Pos-x.HIT+1, ":", startPosAlign, endPosAlign, ">>", read)
				oneBP := bp[simpleBreakpoint{read.Pos, endPosAlign - x.Pos - x.HIT + 1}]

				oneBP.hit++
				oneBP.longhit = append(oneBP.longhit, x.HIT)
				oneBP.softclippedhit = append(oneBP.softclippedhit, getNumberSoftClippedFirst(read))
				oneBP.matchleft = true
				// oneBP.read = append(oneBP.read, read)

				oneBP.orderhit = append(oneBP.orderhit, orderhit)
				// oneBP.alldepth = bufferReadSize

				bp[simpleBreakpoint{read.Pos, endPosAlign - x.Pos - x.HIT + 1}] = oneBP
				// break
			}
		} else if getNumberSoftClippedLast(read) >= 3 {

			score = bwt.locateReadINV(read, getNumberSoftClippedLast(read))
			// fmt.Println(score)
			for _, x := range score {

				if x.HIT >= 4 {

				} else {
					continue
				}

				// fmt.Println(string(read.Seq.Expand()))

				// if read.End() == 94124885 {
				// 	fmt.Println("---->", read.End(), startPosAlign+x.Pos+x.HIT+1, x)
				// 	fmt.Println(read)
				// }
				// if read.Seq.Length-x.HIT+read.Pos-plusFirstSoftclipped == 248849865 {
				// 	fmt.Println(read)
				// }
				// bp[simpleBreakpoint{read.Rec.Seq.Length - x.HIT + read.Rec.Pos - plusFirstSoftclipped, startPosAlign + x.Pos + 1}]++

				oneBP := bp[simpleBreakpoint{read.End(), startPosAlign + x.Pos + x.HIT + 1}]

				// if startPosAlign+x.Pos+x.HIT+1 < read.End() {

				// }

				oneBP.hit++
				oneBP.longhit = append(oneBP.longhit, x.HIT)
				oneBP.softclippedhit = append(oneBP.softclippedhit, getNumberSoftClippedLast(read))
				oneBP.matchright = true
				// oneBP.read = append(oneBP.read, read)

				oneBP.orderhit = append(oneBP.orderhit, orderhit)
				// oneBP.alldepth = bufferReadSize

				bp[simpleBreakpoint{read.End(), startPosAlign + x.Pos + x.HIT + 1}] = oneBP
			}

		}

	}

	var bks []breakPointState
	for i, x := range bp {
		if checkAddBreakpointState(&x, "INV") {
			bks = append(bks, breakPointState{
				POS:            i.POS,
				END:            i.END,
				HIT:            x.hit,
				INSERTSIZE:     i.END - i.POS,
				ALLDEPTH:       x.alldepth,
				longhit:        x.longhit,
				softclippedhit: x.softclippedhit,
				orderhit:       x.orderhit,
				ReadPairCount:  evidence.LEN,
				matchleft:      x.matchleft,
				matchright:     x.matchright,
			})
		}
	}

	sort.Slice(bks, func(i, j int) bool { return (bks)[i].HIT > (bks)[j].HIT })
	var PTresults []printTrain
	for _, x := range bks {

		if x.HIT >= 2 {

		} else {
			continue
		}

		if x.END-x.POS < 50 {
			continue
		}

		// if getMaxIntArray(x.longhit) < 6 {
		// 	continue
		// }

		if !isInLimitRange(x.POS, x.END) {
			fmt.Println("#CODE0x2INVFirst", "isInLimitRange INV", x.POS, x.END)
			continue
		}
		x.SVType = "INV"
		x.Orientation = "F"
		x.MapQRP = evidence.MapQRP

		aPT := printPileUp(evidence, &x, &jbs, task)
		PTresults = append(PTresults, aPT)
	}
	return PTresults

}
