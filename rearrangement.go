package bolt

import "github.com/biogo/hts/sam"

func getReadPileupPoint(reads []*sam.Record, ignoreread []int, pos int) ([]*sam.Record, []*sam.Record) {
	var rCapture []*sam.Record
	var rUncapture []*sam.Record
	for number, read := range reads {

		if foundOrderRead(number, ignoreread) {
			rCapture = append(rCapture, read)
			continue
		} else {

			realPos := read.Pos - getNumberSoftClippedFirst(read)
			realEnd := read.End() + getNumberSoftClippedLast(read)
			if checkReadThisPoint(realPos, realEnd, pos) {
				rUncapture = append(rUncapture, read)
			}

			continue
		}
	}

	return rCapture, rUncapture
}

func getReadDepthwithoutSC(reads []*sam.Record, ignoreread []int, pos int) int {
	var count int

	for number, read := range reads {
		if foundOrderRead(number, ignoreread) {
			continue
		} else {
			if checkReadThisPoint(read.Pos, read.End(), pos) {
				count++
			}
			continue
		}
	}

	return count
}

func checkReadThisPoint(realPos, realEnd, pos int) bool {
	if realPos <= pos && pos <= realEnd {
		return true
	}
	return false
}

func getNumberSoftClippedFirst(read *sam.Record) int {
	if read.Cigar[0].Type().String() == "S" {
		return read.Cigar[0].Len()
	}
	return 0
}

func getNumberSoftClippedLast(read *sam.Record) int {
	if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {
		return read.Cigar[len(read.Cigar)-1].Len()
	}
	return 0
}

func getInfoSoftClippedFirst(reads []*sam.Record) int {
	total := 0

	for _, read := range reads {
		if read.Cigar[0].Type().String() == "S" {
			// total = total + read.Cigar[0].Len()
			total++
		}
	}
	return total
}

func getInfoSoftClippedLast(reads []*sam.Record) int {
	total := 0

	for _, read := range reads {
		if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {
			// total = total + read.Cigar[0].Len()
			total++
		}
	}
	return total
}

func getInfoSoftClipped(reads []*sam.Record) int {
	total := 0

	for _, read := range reads {
		if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {
			total++
		} else if read.Cigar[0].Type().String() == "S" {
			total++
		}
	}
	return total
}

func getLonggestHit(x *breakPointState) int {
	var maxHit int
	for _, hit := range x.longhit {
		if maxHit < hit {
			maxHit = hit
		}
	}
	return maxHit
}

func getMinLongHit(x *breakPointState) int {
	minHit := 10000
	for _, hit := range x.longhit {
		if minHit > hit {
			minHit = hit
		}
	}

	if minHit == 10000 {
		return 0
	}
	return minHit
}

func getAVGLongHit(x *breakPointState) int {
	totalHit := 0
	count := 0
	for _, hit := range x.longhit {
		// if totalHit > hit {
		count++
		totalHit = totalHit + hit
		// }
	}
	return int(totalHit / count)
}

func getmaxScHit(x *breakPointState) int {
	var maxHit int
	for _, hit := range x.softclippedhit {
		if maxHit < hit {
			maxHit = hit
		}
	}
	return maxHit
}

func getMinScHit(x *breakPointState) int {
	minHit := 10000
	for _, hit := range x.softclippedhit {
		if minHit > hit {
			minHit = hit
		}
	}

	if minHit == 10000 {
		return 0
	}
	return minHit
}

func getAvgScHit(x *breakPointState) int {
	totalHit := 0
	count := 0
	for _, hit := range x.softclippedhit {
		// if totalHit > hit {
		count++
		totalHit = totalHit + hit
		// }
	}
	return int(totalHit / count)
}

func getDepthRead(rCapture, rUncapture []*sam.Record) int {
	return len(rCapture) + len(rUncapture)
}

// getSumScSide L = last, F = first, ?? = 2 side
func getSumScSide(rCapture []*sam.Record, oren string) int {
	if oren == "L" {
		var maxHit int
		for _, read := range rCapture {

			if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {
				if read.Cigar[len(read.Cigar)-1].Len() > pileupCountScMoreThan {
					maxHit++
				}
			}
		}
		return maxHit
	} else if oren == "F" {
		var maxHit int
		for _, read := range rCapture {
			if read.Cigar[0].Type().String() == "S" {
				if read.Cigar[0].Len() > pileupCountScMoreThan {
					maxHit++
				}
			}
		}
		return maxHit
	} else {
		var maxHit int
		for _, read := range rCapture {
			if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {
				if read.Cigar[len(read.Cigar)-1].Len() > pileupCountScMoreThan {
					maxHit++
				}
			} else if read.Cigar[0].Type().String() == "S" {
				if read.Cigar[0].Len() > pileupCountScMoreThan {
					maxHit++
				}
			}
		}
		return maxHit
	}

}

// getScMaxSide L = last, F = first, ?? = 2 side
func getScMaxSide(rCapture []*sam.Record, oren string) int {
	if oren == "L" {
		var maxHit int
		for _, read := range rCapture {
			if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {
				if maxHit < read.Cigar[len(read.Cigar)-1].Len() {
					maxHit = read.Cigar[len(read.Cigar)-1].Len()
				}
			}
		}
		return maxHit
	} else if oren == "F" {
		var maxHit int
		for _, read := range rCapture {
			if read.Cigar[0].Type().String() == "S" {
				if maxHit < read.Cigar[0].Len() {
					maxHit = read.Cigar[0].Len()
				}
			}
		}
		return maxHit
	} else {
		var maxHit int
		for _, read := range rCapture {
			if read.Cigar[0].Type().String() == "S" {
				if maxHit < read.Cigar[0].Len() {
					maxHit = read.Cigar[0].Len()
				}
			} else if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {
				if maxHit < read.Cigar[len(read.Cigar)-1].Len() {
					maxHit = read.Cigar[len(read.Cigar)-1].Len()
				}
			}
		}
		return maxHit
	}

}

// getScMaxSide L = last, F = first, ?? = 2 side
func getScMinSide(rCapture []*sam.Record, oren string) int {
	if oren == "L" {
		maxHit := 10000
		for _, read := range rCapture {
			maxHit = getNumberSoftClippedLast(read)
		}
		if maxHit == 10000 {
			maxHit = 0
		}
		return maxHit
	} else if oren == "F" {
		maxHit := 10000
		for _, read := range rCapture {
			maxHit = getNumberSoftClippedFirst(read)
		}
		if maxHit == 10000 {
			maxHit = 0
		}
		return maxHit
	} else {
		maxHit := 10000
		for _, read := range rCapture {
			if read.Cigar[0].Type().String() == "S" {
				maxHit = getNumberSoftClippedFirst(read)
			} else if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {
				maxHit = getNumberSoftClippedLast(read)
			}
		}
		if maxHit == 10000 {
			maxHit = 0
		}
		return maxHit
	}

}

func getSumSCOverPoint(rCapture []*sam.Record, oren string, pos int) int {
	var count int
	if oren == "L" {
		for _, read := range rCapture {
			sc := getNumberSoftClippedLast(read)
			if sc > 3 {
				if read.End() < pos {
					count++
					// fmt.Println(read)
				}
			}
		}
	} else if oren == "F" {
		for _, read := range rCapture {
			sc := getNumberSoftClippedFirst(read)
			if sc > 3 {
				if read.Pos > pos {
					count++
				}
			}
		}
	} else {
		for _, read := range rCapture {
			scF := getNumberSoftClippedFirst(read)
			scL := getNumberSoftClippedLast(read)
			if scF > 3 {
				if read.Pos > pos {
					count++
				}
			}
			if scL > 3 {
				if read.End() < pos {
					count++
				}
			}
		}
	}

	return count
}

// getAVGScSide L = last, F = first, ?? = 2 side
func getAVGScSide(rCapture []*sam.Record, oren string) int {
	if oren == "L" {
		var totalHit int
		var count int
		for _, read := range rCapture {
			if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {
				totalHit = totalHit + read.Cigar[len(read.Cigar)-1].Len()
				count++
			}
		}
		if count == 0 {
			return 0
		}
		return int(totalHit / count)

	} else if oren == "F" {
		var totalHit int
		var count int
		for _, read := range rCapture {

			if read.Cigar[0].Type().String() == "S" {
				totalHit = totalHit + read.Cigar[0].Len()
				count++
			}
		}
		if count == 0 {
			return 0
		}
		return int(totalHit / count)

	} else {
		var totalHit int
		var count int
		for _, read := range rCapture {
			if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {
				totalHit = totalHit + read.Cigar[len(read.Cigar)-1].Len()
				count++
			} else if read.Cigar[0].Type().String() == "S" {
				totalHit = totalHit + read.Cigar[0].Len()
				count++
			}
		}
		if count == 0 {
			return 0
		}
		return int(totalHit / count)

	}

}

func getMapQMax(reads []*sam.Record) int {
	var MaxMapQ int
	for _, read := range reads {

		if MaxMapQ < int(read.MapQ) {
			MaxMapQ = int(read.MapQ)
		}
	}
	return MaxMapQ
}

func getMapQMin(reads []*sam.Record) int {
	MinMapQ := 10000
	for _, read := range reads {

		if MinMapQ > int(read.MapQ) {
			MinMapQ = int(read.MapQ)
		}
	}

	if MinMapQ == 10000 {
		return 0
	}

	return MinMapQ
}

func getMapQAvg(reads []*sam.Record) int {
	var sumMapQ int
	for _, read := range reads {
		sumMapQ = sumMapQ + int(read.MapQ)
	}

	if len(reads) == 0 {
		return 0
	}
	return sumMapQ / len(reads)

}

func getMatchOrientation(x *breakPointState) string {
	var matchO string
	if x.matchleft {
		matchO = "L"
	}
	if x.matchright {
		matchO = matchO + "F"
	}

	return matchO
}

func getOnlySCRead(recs []*sam.Record) []*sam.Record {
	var scread []*sam.Record

	for _, read := range recs {
		if getNumberSoftClippedLast(read) >= 3 {
			scread = append(scread, read)
		} else if getNumberSoftClippedFirst(read) >= 3 {
			scread = append(scread, read)
		}
	}

	return scread
}
