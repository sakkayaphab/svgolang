package bolt

func checkBetween(main, compare, negativelimitrange, pluslimitrange int) bool {
	if compare-negativelimitrange <= main && main <= compare+pluslimitrange {
		return true
	}

	return false
}
