package bolt

import (
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"regexp"
	"strconv"

	"github.com/biogo/hts/bam"
)

// func (task *Task) getbamReader() {
// 	bamread, err := os.Open(task.SamplePath)
// 	if err != nil {
// 		panic(err)
// 	}
// 	// defer bamread.Close()

// 	bamRead, err := bam.NewReader(bamread, 0)
// 	if err != nil {
// 		panic(err)
// 	}
// 	// defer bamRead.Close()

// 	task.bamReader = bamRead
// }

func (task *Task) getReadSize() {
	bamread, err := os.Open(task.SamplePath)
	if err != nil {
		panic(err)
	}
	defer bamread.Close()

	bamRead, err := bam.NewReader(bamread, 0)
	if err != nil {
		panic(err)
	}
	defer bamRead.Close()

	var readBP, count int
	FlagStat := []uint16{2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1}

	for {
		rec, err := bamRead.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		//Find Unmapped Read
		var FlagUnmapped bool
		TempFlag := uint16(rec.Flags)
		for i := 0; i < len(FlagStat); i++ {
			if TempFlag >= FlagStat[i] {
				TempFlag = TempFlag - FlagStat[i]
				if FlagStat[i] == 4 {
					// NumberUnmapped++
					FlagUnmapped = true
					// text := printRecord(rec)
					// writeSAM(unMappedSam, &text)
					break
				}
			}
		}
		if FlagUnmapped {
			continue
		}

		if count > 1000 {
			break
		}

		if len(rec.Cigar) == 1 {
			if rec.Cigar[0].Type().String() == "M" {
				readBP = rec.Len()
				break
			}
		}

		if readBP < rec.Len() {
			readBP = rec.Len()
		}

		count++
	}
	task.sampleStats.ReadLength = readBP
}

func (task *Task) getMeanSD() {
	bamread, err := os.Open(task.SamplePath)
	if err != nil {
		panic(err)
	}
	defer bamread.Close()

	bamRead, err := bam.NewReader(bamread, 0)
	if err != nil {
		panic(err)
	}
	defer bamRead.Close()

	var number int

	var SumSqureInsertSize, SumInsertSize float64
	var NumberCount int

	maxcount := 40000
	fmt.Println("\n► Making an estimate of ten thousand reads to calculate the mean and standard deviation")

	for {
		rec, err := bamRead.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		if rec.MatePos == -1 {
			continue
		}

		if rec.Ref.Name() != rec.MateRef.Name() {
			continue
		}

		number++

		insertsize := float64(rec.MatePos+task.sampleStats.ReadLength) - float64(rec.Pos)
		if insertsize < 0 {
			continue
		}
		if insertsize < 1000 {
			r, _ := regexp.Compile(`[\d]{0,}[A-Z]{1}`)
			temp := r.FindAllString(rec.Cigar.String(), -1)
			if len(temp) == 0 {
				continue
			}
			var skipPosition int
			if temp[0][len(temp[0])-1:] == "S" {
				skipPosition, err = strconv.Atoi(temp[0][:len(temp[0])-1])
				if err != nil {
					log.Fatal(err)
				}
			}
			if skipPosition != 0 {
				continue
			}
			NumberCount++
			SumSqureInsertSize = SumSqureInsertSize + math.Pow(insertsize, 2)
			SumInsertSize = SumInsertSize + insertsize
			// bar.Increment()
			continue
		}

		if NumberCount >= maxcount {
			break
		}

	}
	resultUpper := (float64(NumberCount) * SumSqureInsertSize) - math.Pow(SumInsertSize, 2)
	resultLower := float64(NumberCount * (NumberCount - 1))
	SDresult := math.Sqrt(resultUpper / resultLower)
	Mean := SumInsertSize / float64(NumberCount)
	fmt.Println("\nMean :", Mean)
	fmt.Println("SD :", SDresult)

	task.sampleStats.InsertSizeMean = int(Mean)
	task.sampleStats.InsertSizeSD = int(SDresult)
}
