package bolt

import (
	"sort"
	"strings"
	"sync"

	"github.com/biogo/hts/sam"
)

type variantRange struct {
	CHR       string
	MATECHR   string
	READPOS   int
	READEND   int
	READMATE  int
	TYPE      string
	Count     int
	Read      []*sam.Record
	SATag     int
	MaxMapQRP int
	MapQRP    []int
}

type breakpointTRANFinal struct {
	POS       int
	END       int
	SIMPOS    int
	SIMEND    int
	LEN       int
	ORIGINPOS int
}

type breakPointStateTRAN struct {
	CHR     string
	POS     int
	MATECHR string
	END     int
	HIT     int
}

func findTranslocation(variantsC chan readContainer, calculateFinish chan bool, wgSV *sync.WaitGroup, jbs jobsSplitter, task *Task) {
	defer wgSV.Done()

	var ovariantRange []variantRange
	var wgSP sync.WaitGroup
	variantScanner := make(chan variantRange)
	SyncRecordFinish := make(chan bool)
	for i := 0; i < task.CallerConfig.MaxConcurrentOfSR; i++ {
		wgSP.Add(1)
		go findTRABreakPoint(variantScanner, SyncRecordFinish, &wgSP, jbs, task, i)
	}

	// FlagStat := []uint16{2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1}
	// createFile(task.TempPath + "/tra/" + jbs.CHROM)
	createFile(task.OutputPath + temppath + "tra/" + jbs.CHROM + ".txt")

	// runTask := 0

	for {
		select {
		case variantList := <-variantsC:
			for i := 0; i < len(ovariantRange); i++ {
				ovariantRange[i].Read = append(ovariantRange[i].Read, variantList.Read...)
			}

			for m := 0; m < len(variantList.Read); m++ {

				sizeEvidenceIS := len(ovariantRange)
				for i := 0; i < sizeEvidenceIS; i++ {

					if ovariantRange[i].READPOS+int(float32(task.sampleStats.InsertSizeMean)) < variantList.Read[m].Pos {
						if ovariantRange[i].Count >= 10 {
							variantScanner <- ovariantRange[i]
							ovariantRange = append(ovariantRange[:i], ovariantRange[i+1:]...)
							sizeEvidenceIS--
							i--
							continue
						}
					}

					if len(ovariantRange[i].Read) > task.CallerConfig.LimitCacheReadPerEvidence {
						if ovariantRange[i].Count >= 4 {
							variantScanner <- ovariantRange[i]
							ovariantRange = append(ovariantRange[:i], ovariantRange[i+1:]...)
							sizeEvidenceIS--
							i--
							continue
						} else {
							ovariantRange = append(ovariantRange[:i], ovariantRange[i+1:]...)
							sizeEvidenceIS--
							i--
							continue
						}

					}

				}

				// Mate Translocation
				if variantList.Read[m].Ref.Name() == variantList.Read[m].MateRef.Name() {
					continue
				}

				var added bool
				for i := 0; i < len(ovariantRange); i++ {
					checkCHR := ovariantRange[i].MATECHR == variantList.Read[m].MateRef.Name()
					max := (task.sampleStats.ReadLength) + int(task.sampleStats.InsertSizeSD)
					checkMate := checkBetween(ovariantRange[i].READMATE, variantList.Read[m].MatePos, max, max)
					if checkCHR && checkMate {
						ovariantRange[i].Count++
						ovariantRange[i].MapQRP = append(ovariantRange[i].MapQRP, int(variantList.Read[m].MapQ))

						added = true
						break
					}
				}

				if !added {
					tempVR := variantRange{}
					tempVR.CHR = variantList.Read[m].Ref.Name()
					tempVR.Count = 1
					tempVR.READPOS = variantList.Read[m].Pos
					tempVR.READEND = variantList.Read[m].End()
					tempVR.READMATE = variantList.Read[m].MatePos
					tempVR.MATECHR = variantList.Read[m].MateRef.Name()
					tempVR.MaxMapQRP = int(variantList.Read[m].MapQ)
					tempVR.Read = append(tempVR.Read, variantList.Read...)
					tempVR.MapQRP = append(tempVR.MapQRP, int(variantList.Read[m].MapQ))
					ovariantRange = append(ovariantRange, tempVR)
				}

			}
		case <-calculateFinish:

			for i := 0; i < task.CallerConfig.MaxConcurrentOfSR; i++ {
				SyncRecordFinish <- true
			}

			wgSP.Wait()

			return
		}
	}
}

func findTRABreakPoint(variantsC chan variantRange, calculateFinish chan bool, wg *sync.WaitGroup, jbs jobsSplitter, task *Task, number int) {
	defer wg.Done()

	for {
		select {
		case variant := <-variantsC:

			var startPosAlign, endPosAlign int

			endPosAlign = variant.READMATE + task.sampleStats.ReadLength*6 + int(task.sampleStats.InsertSizeSD)
			startPosAlign = variant.READMATE - task.sampleStats.ReadLength*2 - int(task.sampleStats.InsertSizeSD)

			maxReferenceEND := task.getReferenceEND(variant.MATECHR)

			if maxReferenceEND <= 0 {
				continue
			}

			if endPosAlign > maxReferenceEND {
				endPosAlign = maxReferenceEND
			}

			if endPosAlign < 0 {
				continue
			}

			if startPosAlign < 0 {
				continue
			}

			seq, err := task.fastaReader.GetSequence(variant.MATECHR, startPosAlign, endPosAlign)
			if err != nil {
				return
			}

			bwt := newBWT()
			seq = strings.ToUpper(seq)
			bwt.Transform(seq)

			bp := make(map[simpleBreakpoint]breakpointinfo)

			for orderhit, read := range variant.Read {
				if len(read.Cigar) < 2 {
					continue
				}

				var mateCharNotCorrent bool
				if variant.MATECHR == read.MateRef.Name() {
					mateCharNotCorrent = true
				}

				if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {

					if getNumberSoftClippedLast(read) <= 3 {
						continue
					}

					if len(read.Seq.Seq) == 0 {
						continue
					}

					sizeSoftClippedLast := read.Cigar[len(read.Cigar)-1].Len()

					var sizeSoftClippedFirst int
					if read.Cigar[0].Type().String() == "S" {
						sizeSoftClippedFirst = read.Cigar[0].Len()
					}

					score := bwt.locateReverseRead(read, sizeSoftClippedLast)
					for _, x := range score {
						if x.HIT < 4 {

							continue
						}

						oneBP := bp[simpleBreakpoint{read.Seq.Length - x.HIT + read.Pos - sizeSoftClippedFirst, x.Pos + startPosAlign}]

						if oneBP.matechrnotcorrect {
							continue
						}

						if mateCharNotCorrent {
							oneBP.matechrnotcorrect = true
						}

						oneBP.hit++
						oneBP.longhit = append(oneBP.longhit, x.HIT)
						oneBP.softclippedhit = append(oneBP.softclippedhit, sizeSoftClippedLast)

						oneBP.orderhit = append(oneBP.orderhit, orderhit)
						oneBP.orientation = "L"
						// oneBP.alldepth = bufferReadSize

						bp[simpleBreakpoint{read.Seq.Length - x.HIT + read.Pos - sizeSoftClippedFirst, x.Pos + startPosAlign}] = oneBP

					}
				} else if read.Cigar[0].Type().String() == "S" {

					if getNumberSoftClippedFirst(read) <= 3 {
						continue
					}

					if len(read.Seq.Seq) == 0 {
						continue
					}

					sizeSoftClippedFirst := read.Cigar[0].Len()

					score := bwt.locateRead(read, sizeSoftClippedFirst)
					for _, x := range score {
						if x.HIT < 4 {
							continue
						}

						oneBP := bp[simpleBreakpoint{read.Pos - sizeSoftClippedFirst + x.HIT, endPosAlign - x.Pos}]
						if oneBP.matechrnotcorrect {
							continue
						}

						if mateCharNotCorrent {
							oneBP.matechrnotcorrect = true
						}
						oneBP.hit++
						oneBP.longhit = append(oneBP.longhit, x.HIT)
						oneBP.softclippedhit = append(oneBP.softclippedhit, sizeSoftClippedFirst)

						oneBP.orderhit = append(oneBP.orderhit, orderhit)
						oneBP.orientation = "F"

						bp[simpleBreakpoint{read.Pos - sizeSoftClippedFirst + x.HIT, endPosAlign - x.Pos}] = oneBP

					}
				}
			}

			var bks []breakPointState
			for i, x := range bp {
				if checkAddBreakpointState(&x, "TRA") {
					bks = append(bks, breakPointState{
						POS:            i.POS,
						END:            i.END,
						HIT:            x.hit,
						INSERTSIZE:     i.END - i.POS,
						ALLDEPTH:       x.alldepth,
						longhit:        x.longhit,
						softclippedhit: x.softclippedhit,
						orderhit:       x.orderhit,
						Orientation:    x.orientation,
						ReadPairCount:  variant.Count,
					})
				}
			}

			sort.Slice(bks, func(i, j int) bool { return (bks)[i].HIT > (bks)[j].HIT })

			for _, x := range bks {
				if x.Orientation == "F" {
					rCapture, rUncapture := getReadPileupPoint(variant.Read, x.orderhit, x.POS)
					if getLonggestHit(&x) <= 4 && x.HIT <= 2 {
						continue
					}

					var pt printTrain
					pt.SVType = "TRA"
					pt.Orientation = "F"
					pt.Chr = variant.CHR
					pt.Pos = x.POS
					pt.ChrEnd = variant.MATECHR
					pt.End = x.END
					pt.Hit = x.HIT
					pt.LongMaxHit = getLonggestHit(&x)
					pt.LongMinHit = getMinLongHit(&x)
					pt.LongAvgHit = getAVGLongHit(&x)
					pt.MaxScHit = getmaxScHit(&x)
					pt.MinScHit = getMinScHit(&x)
					pt.AvgScHit = getAvgScHit(&x)
					pt.SumScOppSide = getSumScSide(rUncapture, "F")
					pt.ScMaxOppSide = getScMaxSide(rUncapture, "F")
					pt.ScMinOppSide = getScMinSide(rUncapture, "F")
					pt.ScAvgOppSide = getAVGScSide(rUncapture, "F")
					pt.SumSCOverPointOppSide = getSumSCOverPoint(rUncapture, "L", x.POS)
					pt.SumScSameSide = getSumScSide(rUncapture, "L")
					pt.ScMaxSameSide = getScMaxSide(rUncapture, "L")
					pt.ScMinSameSide = getScMinSide(rUncapture, "L")
					pt.ScAvgSameSide = getAVGScSide(rUncapture, "L")
					pt.SumSCOverPointSameSide = getSumSCOverPoint(rUncapture, "F", x.POS)
					pt.ReadDepth = getDepthRead(rCapture, rUncapture)
					pt.CaptureMapQMax = getMapQMax(rCapture)
					pt.CaptureMapQMin = getMapQMin(rCapture)
					pt.CaptureMapQAvg = int(getMapQAvg(rCapture))
					pt.UncaptureMapQMax = getMapQMax(rUncapture)
					pt.UncaptureMapQMin = getMapQMin(rUncapture)
					pt.UncaptureMapQAvg = int(getMapQAvg(rUncapture))
					// only sc
					rUncaptureSC := getOnlySCRead(rUncapture)
					pt.UncaptureSCMapQMax = getMapQMax(rUncaptureSC)
					pt.UncaptureSCMapQMin = getMapQMin(rUncaptureSC)
					pt.UncaptureSCMapQAvg = int(getMapQAvg(rUncaptureSC))
					pt.MatchOrientation = getMatchOrientation(&x)
					pt.ReadPairCount = x.ReadPairCount
					pt.SVType = "TRA"
					// pt.MapQRP = evidence.MapQRP
					// pt.MapQRP = variant.MapQRP
					pt.MaxMapQRP = getMaxIntArray(variant.MapQRP)
					pt.AvgMapQRP = getAvgIntArray(variant.MapQRP)
					pt.MinMapQRP = getMinIntArray(variant.MapQRP)

					printTrainData(pt, task.OutputPath+temppath+"tra/"+jbs.CHROM)
				} else {
					rCapture, rUncapture := getReadPileupPoint(variant.Read, x.orderhit, x.POS)
					if getLonggestHit(&x) <= 4 && x.HIT <= 2 {
						continue
					}

					var pt printTrain
					pt.Orientation = "L"
					pt.Chr = variant.CHR
					pt.Pos = x.POS
					pt.ChrEnd = variant.MATECHR
					pt.End = x.END
					pt.Hit = x.HIT
					pt.LongMaxHit = getLonggestHit(&x)
					pt.LongMinHit = getMinLongHit(&x)
					pt.LongAvgHit = getAVGLongHit(&x)
					pt.MaxScHit = getmaxScHit(&x)
					pt.MinScHit = getMinScHit(&x)
					pt.AvgScHit = getAvgScHit(&x)
					pt.SumScOppSide = getSumScSide(rUncapture, "L")
					pt.ScMaxOppSide = getScMaxSide(rUncapture, "L")
					pt.ScMinOppSide = getScMinSide(rUncapture, "L")
					pt.ScAvgOppSide = getAVGScSide(rUncapture, "L")
					pt.SumSCOverPointOppSide = getSumSCOverPoint(rUncapture, "F", x.END)
					pt.SumScSameSide = getSumScSide(rUncapture, "F")
					pt.ScMaxSameSide = getScMaxSide(rUncapture, "F")
					pt.ScMinSameSide = getScMinSide(rUncapture, "F")
					pt.ScAvgSameSide = getAVGScSide(rUncapture, "F")
					pt.SumSCOverPointSameSide = getSumSCOverPoint(rUncapture, "L", x.END)
					pt.ReadDepth = getDepthRead(rCapture, rUncapture)

					pt.CaptureMapQMax = getMapQMax(rCapture)
					pt.CaptureMapQMin = getMapQMin(rCapture)
					pt.CaptureMapQAvg = int(getMapQAvg(rCapture))
					pt.UncaptureMapQMax = getMapQMax(rUncapture)
					pt.UncaptureMapQMin = getMapQMin(rUncapture)
					pt.UncaptureMapQAvg = int(getMapQAvg(rUncapture))
					// only sc
					rUncaptureSC := getOnlySCRead(rUncapture)
					pt.UncaptureSCMapQMax = getMapQMax(rUncaptureSC)
					pt.UncaptureSCMapQMin = getMapQMin(rUncaptureSC)
					pt.UncaptureSCMapQAvg = int(getMapQAvg(rUncaptureSC))
					pt.ReadPairCount = x.ReadPairCount
					pt.MatchOrientation = getMatchOrientation(&x)
					pt.MaxMapQRP = getMaxIntArray(variant.MapQRP)
					pt.AvgMapQRP = getAvgIntArray(variant.MapQRP)
					pt.MinMapQRP = getMinIntArray(variant.MapQRP)

					pt.SVType = "TRA"
					printTrainData(pt, task.OutputPath+temppath+"tra/"+jbs.CHROM)
				}

				break
			}

		case <-calculateFinish:
			return
		}
	}
}
