package bolt

import (
	"fmt"
	"io"
	"os"

	"github.com/biogo/hts/bam"
	"github.com/biogo/hts/bgzf"
)

type jobsSplitter struct {
	CHROM string
	Chunk bgzf.Chunk
}

func (task *Task) splitjobs(bamfile string) []jobsSplitter {
	f, err := os.Open(bamfile + ".bai")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fidx, err := os.Open(task.SamplePath + ".bai")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	bamindex, err := bam.ReadIndex(fidx)
	if err != nil {
		panic(err)
	}

	var jbs []jobsSplitter
	for i := 0; i < bamindex.NumRefs(); i++ {
		stat, ok := bamindex.ReferenceStats(i)
		if ok {
			tempjb := jobsSplitter{Chunk: stat.Chunk}
			jbs = append(jbs, tempjb)
		}
	}

	bamread, err := os.Open(bamfile)
	if err != nil {
		panic(err)
	}
	defer bamread.Close()

	bamRead, err := bam.NewReader(bamread, 0)
	if err != nil {
		panic(err)
	}
	defer bamRead.Close()

	for i := 0; i < len(jbs); i++ {
		bamRead.Seek(jbs[i].Chunk.Begin)
		rec, err := bamRead.Read()
		if err == io.EOF {
			panic(err)
		}
		jbs[i].CHROM = rec.Ref.Name()
	}

	for i := 0; i < len(jbs); i++ {
		bamRead.Seek(jbs[i].Chunk.Begin)
		rec, err := bamRead.Read()
		if err == io.EOF {
			panic(err)
		}
		// task.sampleStats.rangeCHROM = rec.Ref.Name()
		task.sampleStats.rangeCHROM = append(task.sampleStats.rangeCHROM, rangeCHROM{Name: rec.Ref.Name(), END: rec.Ref.Len()})
		fmt.Println(rec.Ref.Name(), rec.Ref.Len())
	}

	return jbs
}
