package bolt

import "testing"

func TestMATHABS(t *testing.T) {
	tables := []struct {
		Value    int
		Expected int
	}{
		{1, 1},
		{1000, 1000},
		{-1, 1},
		{0, 0},
		{-1000, 1000},
	}

	for _, table := range tables {
		total := abs(table.Value)
		if total != table.Expected {
			t.Errorf("reciprocal validation was incorrect, Value: %d, Expected: %d", table.Value, table.Expected)
		}
	}
}
