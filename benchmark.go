package bolt

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

const (
	benchVCFPosLimit = 0
	benchVCFEndLimit = 0
)

func runBenchmarkvcf() {
	// benchmark := dgv("/Users/sakkayaphab/Downloads/vcf/DGV.GoldStandard.July2015.hg19.NA12878.gff3")
	benchmark := dgv("/data/users/wichadak/kan/vcf/DGV.GS.2016.NA12878.gff3")
	// benchmark := bedBenchmark("/data/users/wichadak/kan/vcf/DGV.GS.2016.NA12878.gff3")
	// return
	// return

	// filepaht := "/data/users/wichadak/kan/vcf/mills_del.csv"
	// filepaht := "/data/users/wichadak/kan/vcf/mills_dup.csv"

	// filepaht := "/Users/sakkayaphab/Downloads/vcf/mills_del.csv"
	// filepaht := "/Users/sakkayaphab/Downloads/vcf/mills_dup.csv"

	// benchmark := mills(filepaht)

	// filepaht := "/data/users/wichadak/kan/trainingdata/survivor/survi.ins"
	// filepaht := "/data/users/wichadak/kan/golibrary/src/github.com/sakkayaphab/boltcdellyhiseqOnly"

	// filepaht := "/Users/sakkayaphab/Downloads/trainingdata/survivor/survi.ins"
	// filepaht := "/Users/sakkayaphab/go/src/github.com/sakkayaphab/mantainv20x"
	// benchmark := loadFileBenchmark(filepaht)

	// number := "1"

	// for i := 1; i < 21; i++ {
	// 	number := strconv.Itoa(i)
	// 	filepaht := "/data/users/wichadak/kan/SURVIVOR2/Debug/training" + number + ".vcf"

	// 	benchmark := loadFileBenchmark(filepaht)
	// 	buildTrainingDataset("/data/users/wichadak/kan/resultbackup/training"+number+"/temp/ins/all", benchmark)
	// }
	// return
	// filepaht := "/data/users/wichadak/kan/SURVIVOR2/Debug/training" + number + ".vcf"

	// benchmark := loadFileBenchmark(filepaht)
	// buildTrainingDataset("/data/users/wichadak/kan/resultbackup/training"+number+"/temp/tra/all", benchmark)
	// loadfileForANN(&benchmark, filetest, true)

	// Manta

	// filetest := "/Users/sakkayaphab/Downloads/manta/sur20x/results/variants/ins"
	// filetest := "/Users/sakkayaphab/Downloads/manta/hiseqdel.vcf"
	// filetest := "/data/users/wichadak/kan/manta-1.4.0.centos6_x86_64/bin/NA12878_S1/results/variants/dup"
	// filetest := "/data/users/wichadak/kan/manta-1.4.0.centos6_x86_64/bin/NA12878_S1/results/variants/del"

	//Delly
	// filetest := "/Users/sakkayaphab/Downloads/delly/hiseq/del.vcf"
	// filetest := "/Users/sakkayaphab/Downloads/delly/sur20x/ins.vcf"

	// filetest := "/Users/sakkayaphab/go/src/github.com/sakkayaphab/dellyhiseq"
	// filetest := "/data/users/wichadak/kan/delly/NA12878_S1/dup.vcf"
	// benchmark := loadFileBenchmark(filetest2)

	//Grom
	// filetest := "/Users/sakkayaphab/Downloads/GROM/Hiseq/del.vcf"
	// filetest := "/Users/sakkayaphab/Downloads/GROM/sur20x/ins"
	// filetest := "/data/users/wichadak/kan/GROM/result/NA12878S1/dup.vcf"
	//Softsearch
	// filetest := "/Users/sakkayaphab/Downloads/softsearch/1000simdel/del.vcf"

	//svaba
	// filetest := "/Users/sakkayaphab/Downloads/svaba/bin/sur20x/ins"

	//Wham
	// filetest := "/Users/sakkayaphab/Downloads/wham/20x/del"
	// filetest := "/Users/sakkayaphab/Downloads/wham/sur100x/inv"
	// filetest := "/Users/sakkayaphab/Downloads/wham/sur20x/ins"
	// filetest := "/data/users/wichadak/kan/wham/bin/S1.vcf"
	//Lumpy
	// filetest := "/Users/sakkayaphab/Downloads/lumpy/sur20x/ins"

	//pindel
	// filetest := "/data/users/wichadak/kan/pindel/result/sur100x/del.vcf"
	// filetest := "/data/users/wichadak/kan/pindel/result/sur100x/td.vcf"
	// filetest := "/data/users/wichadak/kan/pindel/result/sur100x/si.vcf"
	// filetest := "/data/users/wichadak/kan/pindel/result/sur100x/inv.vcf"
	// filetest := "/data/users/wichadak/kan/pindel/result/sur20x/rp.vcf"
	// filetest := "/data/users/wichadak/kan/pindel/result/S1/dup"
	// Bolt
	// filetest := "/Users/sakkayaphab/Downloads/result/sur20x/results/inv.vcf"
	// filetest := "/data/users/wichadak/kan/result/sur20x/results/dup.vcf"
	filetest := "/data/users/wichadak/kan/result/S1/results/del.vcf"
	// filetest := "/Users/sakkayaphab/Downloads/result/delhiseq.vcf"
	// benchmark2 := loadFileBenchmarkCustomPindel(filetest)
	// for _, x := range benchmark2 {
	// 	fmt.Println(x)
	// }

	benchmark2 := loadFileBenchmark(filetest)
	// fmt.Println(len(benchmark), len(benchmark2))
	recepicalBench(benchmark, benchmark2)

	// filetest := "/Users/sakkayaphab/Downloads/result/newallsim100x/dup/all"
	// buildTrainingDataset(filetest, benchmark)
	// limitRangePrint("/Users/sakkayaphab/Downloads/trainingdata/nn/originaldataset/traininv/all")

	// buildTrainingDataset("/Users/sakkayaphab/Downloads/result/simulated/ins/all", benchmark)
	// buildTrainingDataset("/Users/sakkayaphab/Downloads/result/S1/ins/all", benchmark)

	// benchmarkNormal(benchmark, benchmark2)
}

func benchmarkTRA(master, slave []benchmarkVCF) {
	master = removeDupbenchmarkVCF(master)
	slave = removeDupbenchmarkVCF(slave)
	bv1A, bv1N := compareNormalBench(&master, &slave, 1000)

	fmt.Println("Size Master :", len(master), "Size Slave :", len(slave))
	fmt.Println("Match:", len(bv1A), "Unmatch:", len(bv1N))

	var Vcount int
	var unmatch []benchmarkVCF
	for _, x := range master {
		if x.Verify {
			Vcount++
			// fmt.Println(x)
		} else {
			// fmt.Println(x)
			unmatch = append(unmatch, x)
		}
	}

	// sort.Slice(unmatch, func(i, j int) bool { return (unmatch)[i].CHR > (unmatch)[j].CHR })
	// for _, x := range unmatch {
	// 	fmt.Println(x)
	// }

	sort.Slice(bv1N, func(i, j int) bool { return (bv1N)[i].CHR > (bv1N)[j].CHR })
	for _, x := range bv1N {
		fmt.Println(x)
	}

	fmt.Println("Verify :", Vcount)
}

func loadfileForANN(benchmarkVCF *[]benchmarkVCF, filepath string, printOut bool) {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*1000)
	var match int
	var unmatch int
	var countCompare int
	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		if strings.Contains(string(line), "#") {
			continue
		}

		s := strings.Split(string(line), "\t")

		// fmt.Println(string(line))
		// chrS := s[0]

		// fmt.Println("---")
		chrS := s[1]
		posS := s[2]
		endS := s[4]

		posi, err := strconv.Atoi(posS)
		if err != nil {
			panic(err)
		}

		endi, err := strconv.Atoi(endS)
		if err != nil {
			panic(err)
		}

		// infos := strings.Split(s[7], ";")
		countCompare++
		var found bool
		for i := 0; i < len(*benchmarkVCF); i++ {
			// max := ((*benchmarkVCF)[i].END - (*benchmarkVCF)[i].POS)
			// max = max / 2
			max := 60
			if chrS == (*benchmarkVCF)[i].CHR {
				if checkBetween((*benchmarkVCF)[i].POS, posi, max, max) && checkBetween((*benchmarkVCF)[i].END, endi, max, max) {
					(*benchmarkVCF)[i].Verify = true
					found = true
					// fmt.Println(chrS, posi, endi)
					match++
					fmt.Println(string(line) + "\t" + "true")

					break
				}
			}
		}

		if !found {
			fmt.Println(string(line) + "\t" + "false")
			unmatch++
		}

	}
	fmt.Println("Size VCF master :", len(*benchmarkVCF), "test vcf:", countCompare)
	fmt.Println("Match :", match, "Unmatch :", unmatch)
	fmt.Println("-------------------------------")
}

func loadfileForStat(benchmarkVCF *[]benchmarkVCF, filepath string, printOut bool) {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*1000)
	var count int
	// var LongMaxHit int
	var countCompare int
	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		if strings.Contains(string(line), "#") {
			continue
		}

		s := strings.Split(string(line), "\t")

		// fmt.Println(string(line))
		// chrS := s[0]

		// fmt.Println("---")
		chrS := s[1]
		posS := s[2]
		endS := s[4]

		posi, err := strconv.Atoi(posS)
		if err != nil {
			log.Fatal(err)
			break
		}

		endi, err := strconv.Atoi(endS)
		if err != nil {
			log.Fatal(err)
			break
		}

		// infos := strings.Split(s[7], ";")
		countCompare++
		var found bool
		for i := 0; i < len(*benchmarkVCF); i++ {
			// max := ((*benchmarkVCF)[i].END - (*benchmarkVCF)[i].POS)
			// max = max / 2
			max := 5
			if chrS == (*benchmarkVCF)[i].CHR {
				if checkBetween((*benchmarkVCF)[i].POS, posi, max, max) && checkBetween((*benchmarkVCF)[i].END, endi, max, max) {
					(*benchmarkVCF)[i].Verify = true
					found = true

					//stat
					// endi, err := strconv.Atoi(s[4])
					// if err != nil {
					// 	log.Fatal(err)
					// 	break
					// }
					// if LongMaxHit<

					break
				}
			}
		}

		if !found {
			fmt.Println(string(line) + "\t" + "false")
		}

		count++

		if count%5000 == 0 {
			// fmt.Println("add to db", count)
		}

	}
	// fmt.Println("Size VCF master :", len(*benchmarkVCF), "test vcf:", countCompare)
	// fmt.Println("Match :", match, "Unmatch :", unmatch)
	// fmt.Println("-------------------------------")
}

func loadfileTestBench(benchmarkVCF *[]benchmarkVCF, filepath string, printOut bool) {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*10000)

	// var benchamrkvcf []benchmarkVCF
	// var match int
	var unmatch int
	var countCompare int
	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		if strings.Contains(string(line), "#") {
			continue
		}

		s := strings.Split(string(line), "\t")
		if len(s) < 8 {
			continue
		}
		chrS := s[0]

		posS := s[1]
		infos := strings.Split(s[7], ";")

		var posi, endi int
		for _, x := range infos {
			if strings.Split(x, "=")[0] == "END" {
				endS := strings.Split(x, "=")[1]
				posi, err = strconv.Atoi(posS)
				if err != nil {
					log.Fatal(err)
					break
				}

				endi, err = strconv.Atoi(endS)
				if err != nil {
					log.Fatal(err)
					break
				}

			}
		}

		if endi == 0 && posi == 0 {
			continue
		}

		countCompare++
		var found bool
		for i := 0; i < len(*benchmarkVCF); i++ {
			max := ((*benchmarkVCF)[i].END - (*benchmarkVCF)[i].POS)
			// max := 5
			if chrS == (*benchmarkVCF)[i].CHR {
				if checkBetween((*benchmarkVCF)[i].POS, posi, max, max) && checkBetween((*benchmarkVCF)[i].END, endi, max, max) {
					(*benchmarkVCF)[i].Verify = true
					found = true
					break
				}
			}
		}

		if !found && printOut {
			unmatch++
			// fmt.Println(string(line))
		}

	}

	fmt.Println("Size VCF master :", len(*benchmarkVCF), "test vcf:", countCompare)
	fmt.Println("Unmatch :", unmatch)
	fmt.Println("-------------------------------")
}

func newloadfileTestBench(benchmarkVCF *[]benchmarkVCF, filepath string, printOut bool) {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*10000)
	// var cachebenchmarkVCF []benchmarkVCF
	// var benchamrkvcf []benchmarkVCF
	// var count int
	// var match int
	// var unmatch int
	var countCompare int
	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		if strings.Contains(string(line), "#") {
			continue
		}

		s := strings.Split(string(line), "\t")
		if len(s) < 8 {
			continue
		}
		chrS := s[0]

		posS := s[1]
		infos := strings.Split(s[7], ";")
		countCompare++
		var found bool
		for _, x := range infos {
			if strings.Split(x, "=")[0] == "END" {
				endS := strings.Split(x, "=")[1]
				posi, err := strconv.Atoi(posS)
				if err != nil {
					log.Fatal(err)
					break
				}

				endi, err := strconv.Atoi(endS)
				if err != nil {
					log.Fatal(err)
					break
				}

				for i := 0; i < len(*benchmarkVCF); i++ {
					max := ((*benchmarkVCF)[i].END - (*benchmarkVCF)[i].POS)
					// max := 5
					if chrS == (*benchmarkVCF)[i].CHR {
						if checkBetween((*benchmarkVCF)[i].POS, posi, max, max) && checkBetween((*benchmarkVCF)[i].END, endi, max, max) {
							found = true
							break
						}
					}
				}

				if found {
					// cachebenchmarkVCF = append(cachebenchmarkVCF, (*benchmarkVCF)[i])

				}

			}
		}

	}
}

func checkLimitRangeVCF(pos, end, limitPos, limitEnd int) bool {
	difPos := end - pos

	// if difPos
	if difPos >= limitPos && difPos <= limitEnd {

	} else {
		return false
	}

	// if end >= limitPos && end <= limitEnd {

	// } else {
	// 	return false
	// }

	return true
}

type benchmarkVCF struct {
	CHR    string
	POS    int
	CHREND string
	END    int
	Verify bool
	Raw    string
}

func loadFileBenchmark(filepath string) []benchmarkVCF {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*10000)
	var benchamrkvcf []benchmarkVCF
	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		//Filter
		// if !(strings.Contains(string(line), "DUP")) {
		// 	continue
		// }

		if string(line)[0:1] == "#" {
			continue
		}

		s := strings.Split(string(line), "\t")
		chrS := s[0]
		posS := s[1]

		//insertion
		// posi, err := strconv.Atoi(posS)
		// if err != nil {
		// 	log.Fatal(err)
		// }
		// benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: chrS,
		// 	POS: posi,
		// 	END: posi})
		// continue

		// SV other type
		var endS string
		infos := strings.Split(s[7], ";")
		for _, x := range infos {

			if strings.Split(x, "=")[0] == "END" {
				endS = strings.Split(x, "=")[1]
				posi, err := strconv.Atoi(posS)
				if err != nil {
					log.Fatal(err)
				}
				endi, err := strconv.Atoi(endS)
				if err != nil {
					log.Fatal(err)
				}

				if benchVCFPosLimit != 0 && benchVCFEndLimit != 0 {
					if !checkLimitRangeVCF(posi, endi, benchVCFPosLimit, benchVCFEndLimit) {
						break
					}
				}

				if endi-posi <= 50 {
					continue
				}

				benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: chrS,
					POS: posi,
					END: endi,
					Raw: string(line),
				})

				// fmt.Println(chrS, posi, endi)
				break
			}
		}

	}

	return benchamrkvcf
}

func loadFileBenchmarkCustom(filepath string) []benchmarkVCF {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*10000)
	var benchamrkvcf []benchmarkVCF
	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		//Filter
		// if !(strings.Contains(string(line), "INS")) {
		// 	continue
		// }

		if string(line)[0:1] == "#" {
			continue
		}

		//svaba
		s := strings.Split(string(line), "\t")
		chrS := s[0]
		posS := s[1]

		posi, err := strconv.Atoi(posS)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println(s)
		_, end := getTRAOnlyChrPosition(s[4])
		benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: chrS,
			POS: posi,
			END: end})
		// ------ SV ABA ----- //

		//insertion
		// posi, err := strconv.Atoi(posS)
		// if err != nil {
		// 	log.Fatal(err)
		// }
		// benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: chrS,
		// 	POS: posi,
		// 	END: posi})

		// SV other type
		// var endS string
		// infos := strings.Split(s[7], ";")
		// for _, x := range infos {

		// 	if strings.Split(x, "=")[0] == "END" {
		// 		endS = strings.Split(x, "=")[1]
		// 		posi, err := strconv.Atoi(posS)
		// 		if err != nil {
		// 			log.Fatal(err)
		// 		}
		// 		endi, err := strconv.Atoi(endS)
		// 		if err != nil {
		// 			log.Fatal(err)
		// 		}

		// 		if benchVCFPosLimit != 0 && benchVCFEndLimit != 0 {
		// 			if !checkLimitRangeVCF(posi, endi, benchVCFPosLimit, benchVCFEndLimit) {
		// 				break
		// 			}
		// 		}

		// 		benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: chrS,
		// 			POS: posi,
		// 			END: endi})

		// 		// fmt.Println(chrS, posi, endi)
		// 		break
		// 	}
		// }

	}

	return benchamrkvcf
}

func loadFileBenchmarkCustomPindel(filepath string) []benchmarkVCF {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*10000)
	var benchamrkvcf []benchmarkVCF
	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		//Filter
		if strings.Contains(string(line), "0/0") {
			continue
		}

		if string(line)[0:1] == "#" {
			continue
		}

		//svaba
		s := strings.Split(string(line), "\t")
		chrS := s[0]
		posS := s[1]

		// posi, err := strconv.Atoi(posS)
		// if err != nil {
		// 	log.Fatal(err)
		// }

		//insertion
		posi, err := strconv.Atoi(posS)
		if err != nil {
			log.Fatal(err)
		}
		benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: chrS,
			POS: posi,
			END: posi})
		// continue

		// SV other type
		// var endS string
		// infos := strings.Split(s[7], ";")
		// for _, x := range infos {

		// 	if strings.Split(x, "=")[0] == "END" {
		// 		endS = strings.Split(x, "=")[1]
		// 		posi, err := strconv.Atoi(posS)
		// 		if err != nil {
		// 			log.Fatal(err)
		// 		}
		// 		endi, err := strconv.Atoi(endS)
		// 		if err != nil {
		// 			log.Fatal(err)
		// 		}

		// 		if benchVCFPosLimit != 0 && benchVCFEndLimit != 0 {
		// 			if !checkLimitRangeVCF(posi, endi, benchVCFPosLimit, benchVCFEndLimit) {
		// 				break
		// 			}
		// 		}

		// 		if endi-posi <= 50 {
		// 			continue
		// 		}

		// 		benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: chrS,
		// 			POS: posi,
		// 			END: endi})
		// 		fmt.Println(chrS, posi, endi)

		// 		break
		// 	}
		// }

	}

	return benchamrkvcf
}

func loadFileBenchmarkCustom2(filepath string) []benchmarkVCF {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*10000)
	var benchamrkvcf []benchmarkVCF
	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		//Filter
		// if !(strings.Contains(string(line), "INS")) {
		// 	continue
		// }

		if string(line)[0:1] == "#" {
			continue
		}

		s := strings.Split(string(line), "\t")
		chrS := s[0]
		posS := s[1]

		//insertion
		// posi, err := strconv.Atoi(posS)
		// if err != nil {
		// 	log.Fatal(err)
		// }
		// benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: chrS,
		// 	POS: posi,
		// 	END: posi})

		// SV other type
		// var endS string
		// infos := strings.Split(s[7], ";")
		// for _, x := range infos {

		// 	if strings.Split(x, "=")[0] == "END" {
		// 		endS = strings.Split(x, "=")[1]
		// 		posi, err := strconv.Atoi(posS)
		// 		if err != nil {
		// 			log.Fatal(err)
		// 		}
		// 		endi, err := strconv.Atoi(endS)
		// 		if err != nil {
		// 			log.Fatal(err)
		// 		}

		// 		if benchVCFPosLimit != 0 && benchVCFEndLimit != 0 {
		// 			if !checkLimitRangeVCF(posi, endi, benchVCFPosLimit, benchVCFEndLimit) {
		// 				break
		// 			}
		// 		}

		// 		benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: chrS,
		// 			POS: posi,
		// 			END: endi})

		// 		// fmt.Println(chrS, posi, endi)
		// 		break
		// 	}
		// }

		//svaba
		posi, err := strconv.Atoi(posS)
		if err != nil {
			log.Fatal(err)
		}

		// fmt.Println(s)
		// _, end := getTRAOnlyChrPosition(s[4])
		// benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: chrS,
		// 	POS: posi,
		// 	END: end})
		var end int
		var chrend string
		infos := strings.Split(s[7], ";")
		for _, x := range infos {

			if strings.Split(x, "=")[0] == "CHR2" {
				chrend = strings.Split(x, "=")[1]

			}

			if strings.Split(x, "=")[0] == "END" {
				endS := strings.Split(x, "=")[1]

				end, err = strconv.Atoi(endS)
				if err != nil {
					log.Fatal(err)
				}

			}

		}

		benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: chrS,
			POS:    posi,
			CHREND: chrend,
			END:    end})
	}

	return benchamrkvcf
}

func limitRangeVCF(filepath string, limitPos, limitEnd int) []benchmarkVCF {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*1000)
	var benchamrkvcf []benchmarkVCF
	var count int

	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		if string(line)[0:1] == "#" {
			continue
		}

		s := strings.Split(string(line), "\t")
		chrS := s[0]
		posS := s[1]
		var endS string
		infos := strings.Split(s[7], ";")
		for _, x := range infos {
			if strings.Split(x, "=")[0] == "END" {
				endS = strings.Split(x, "=")[1]
				posi, err := strconv.Atoi(posS)
				if err != nil {
					log.Fatal(err)
				}
				endi, err := strconv.Atoi(endS)
				if err != nil {
					log.Fatal(err)
				}

				if !checkLimitRangeVCF(posi, endi, limitPos, limitEnd) {
					break
				}

				benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: chrS,
					POS: posi,
					END: endi})

				fmt.Println(chrS, posi, endi, "=", endi-posi)
				break
			}
		}

		count++

		if count%5000 == 0 {
			// fmt.Println("add to db", count)
		}

	}

	fmt.Println("---- End -----")

	return benchamrkvcf
}

func loadFileBenchmarkSVsim(filepath string) []benchmarkVCF {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*10000)
	var benchamrkvcf []benchmarkVCF

	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		if string(line)[0:1] == "#" {
			continue
		}

		s := strings.Split(string(line), "\t")
		chrS := s[5]
		posS := s[6]
		// chrE_S := s[8]
		endS := s[9]

		posi, err := strconv.Atoi(posS)
		if err != nil {
			log.Fatal(err)
		}
		endi, err := strconv.Atoi(endS)
		if err != nil {
			log.Fatal(err)
		}
		benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: chrS,
			POS: posi,
			END: endi,
			// Raw: string(line),
		})

	}

	return benchamrkvcf
}

func mills(filepath string) []benchmarkVCF {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*1000)
	var benchamrkvcf []benchmarkVCF
	var count int

	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		s := strings.Split(string(line), ",")
		chrS := s[1]
		posS := s[2]
		// chrE_S := s[8]
		endS := s[3]

		posi, err := strconv.Atoi(posS)
		if err != nil {
			log.Fatal(err)
		}
		endi, err := strconv.Atoi(endS)
		if err != nil {
			log.Fatal(err)
		}
		benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: chrS,
			POS: posi,
			END: endi})

		count++

		if count%5000 == 0 {
			// fmt.Println("add to db", count)
		}

	}

	return benchamrkvcf
}

func manualAddfile() []benchmarkVCF {
	var benchamrkvcf []benchmarkVCF
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 7779626,
		END: 7781005})
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 21792935,
		END: 21795588})
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 22018768,
		END: 22029581})
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 22019332,
		END: 22021815})
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 22022031,
		END: 22023200})
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 22024922,
		END: 22029031})
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 29551436,
		END: 29553158})
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 29583864,
		END: 29587522})
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 35755902,
		END: 35758384})
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 39422463,
		END: 39432893})
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 41437793,
		END: 41441594})
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 53561554,
		END: 53562304})
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 55090085,
		END: 55092006})
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 55688387,
		END: 55689697})
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 58412085,
		END: 58413359})
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 66531207,
		END: 66536420})
	benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: "chr17",
		POS: 71741429,
		END: 71742638})
	return benchamrkvcf
}

func recepicalBench(master, slave []benchmarkVCF) {
	// master = removeDupbenchmarkVCF(master)
	// slave = removeDupbenchmarkVCF(slave)
	bv1A, bv1N := compareReciprocal(&master, &slave, 90)

	var Vcount int
	var unmatch []benchmarkVCF
	var match []benchmarkVCF
	for _, x := range master {
		if x.Verify {
			Vcount++
			// fmt.Println(x)
			match = append(match, x)
		} else {
			// fmt.Println(x)
			unmatch = append(unmatch, x)
		}
	}

	// sort.Slice(match, func(i, j int) bool { return (match)[i].CHR > (match)[j].CHR })
	// for _, x := range match {
	// 	fmt.Println(x.Raw)
	// }

	// sort.Slice(match, func(i, j int) bool { return (match)[i].CHR > (match)[j].CHR })
	// for _, x := range match {
	// 	fmt.Println(x)
	// }

	// sort.Slice(unmatch, func(i, j int) bool { return (unmatch)[i].CHR > (unmatch)[j].CHR })
	// for _, x := range unmatch {
	// 	fmt.Println(x.Raw)
	// }

	// sort.Slice(bv1N, func(i, j int) bool { return (bv1N)[i].CHR > (bv1N)[j].CHR })
	// for _, x := range bv1N {
	// 	fmt.Println(x)
	// }

	// sort.Slice(bv1A, func(i, j int) bool { return (bv1A)[i].CHR > (bv1A)[j].CHR })
	// for _, x := range bv1N {
	// 	if x.Verify {
	// 		fmt.Println(x)
	// 	}
	// }

	fmt.Println("========================================================")
	fmt.Println("Size Master :", len(master), "Size Slave :", len(slave))
	fmt.Println("Match:", len(bv1A), "Unmatch:", len(bv1N))
	fmt.Println("Verify :", Vcount)
}

//return match, noadd
func compareReciprocal(b1, b2 *[]benchmarkVCF, overlap int) ([]benchmarkVCF, []benchmarkVCF) {
	var temp []benchmarkVCF
	var tempNoAdd []benchmarkVCF
	for _, x := range *b2 {
		var added bool
		for y := 0; y < len(*b1); y++ {
			if x.CHR == (*b1)[y].CHR {
				if reciprocalValidation(x.POS, x.END, 70, (*b1)[y].POS, (*b1)[y].END) {
					temp = append(temp, x)
					added = true
					(*b1)[y].Verify = true
					// fmt.Println(x.Raw)
					break
				}
			}
		}

		if !added {
			tempNoAdd = append(tempNoAdd, x)
			// fmt.Println(x.Raw)
		}
	}

	return temp, tempNoAdd
}

func reciprocalValidation(pos, end, overlapped, TargetPos, TargetEnd int) bool {
	// fmt.Println("--------")

	// fmt.Println("POS:", pos)
	// fmt.Println("END:", end)
	// fmt.Println("OVERLAPPED:", overlapped)
	// fmt.Println("TargetPos:", TargetPos)
	// fmt.Println("TargetEnd:", TargetEnd)

	if pos == TargetPos && end == TargetEnd {
		return true
	} else if pos < TargetPos && end > TargetEnd {
		coverage := float64(TargetEnd-TargetPos) / float64(end-pos)
		if coverage < float64(overlapped)/100 {
			return false
		}

		return true
	} else if pos >= TargetPos && end > TargetEnd {

		coverage := float64(TargetEnd-pos) / float64(end-pos)
		if coverage < float64(overlapped)/100 {
			return false
		}

		return true
	} else if pos <= TargetPos && end <= TargetEnd {
		coverage := float64(end-TargetPos) / float64(end-pos)
		if coverage < float64(overlapped)/100 {
			return false
		}
		return true
	} else if pos >= TargetPos && end <= TargetEnd {
		coverage := float64(end-pos) / float64(TargetEnd-TargetPos)
		if coverage < float64(overlapped)/100 {
			return false
		}
		return true
	}

	log.Println("reciprocal error", pos, end, overlapped, TargetPos, TargetEnd)
	panic("reciprocal error")
}

func removeDupbenchmarkVCF(b1 []benchmarkVCF) []benchmarkVCF {
	var temp []benchmarkVCF

	for _, x := range b1 {
		var found bool
		for i := 0; i < len(temp); i++ {
			max := 100
			if temp[i].CHR == x.CHR {
				if checkBetween(temp[i].POS, x.POS, max, max) && checkBetween(temp[i].END, x.END, max, max) {
					found = true
					break
				}
			}
		}

		if !found {
			temp = append(temp, x)
		}
	}

	return temp
}

func benchmarkNormal(master, slave []benchmarkVCF) {
	master = removeDupbenchmarkVCF(master)
	slave = removeDupbenchmarkVCF(slave)
	bv1A, bv1N := compareNormalBench(&master, &slave, 20)

	fmt.Println("Size Master :", len(master), "Size Slave :", len(slave))
	fmt.Println("Match:", len(bv1A), "Unmatch:", len(bv1N))

	var Vcount int
	var unmatch []benchmarkVCF

	for _, x := range master {
		if x.Verify {
			Vcount++
			// fmt.Println(x)
		} else {
			// fmt.Println(x)
			unmatch = append(unmatch, x)
		}
	}

	// sort.Slice(unmatch, func(i, j int) bool { return (unmatch)[i].CHR > (unmatch)[j].CHR })
	// for _, x := range unmatch {
	// 	fmt.Println(x)
	// }

	// sort.Slice(bv1N, func(i, j int) bool { return (bv1N)[i].CHR > (bv1N)[j].CHR })
	// for _, x := range bv1N {
	// 	fmt.Println(x)
	// }

	fmt.Println("Verify :", Vcount)
}

func compareNormalBench(b1, b2 *[]benchmarkVCF, overlap int) ([]benchmarkVCF, []benchmarkVCF) {
	var temp []benchmarkVCF
	var tempNoAdd []benchmarkVCF
	for _, x := range *b2 {
		var added bool
		for y := 0; y < len(*b1); y++ {
			if x.CHR == (*b1)[y].CHR {
				max := overlap
				if checkBetween(x.POS, (*b1)[y].POS, max, max) {
					temp = append(temp, x)
					added = true
					(*b1)[y].Verify = true
					break
				}
			}
		}

		if !added {
			tempNoAdd = append(tempNoAdd, x)
		}
	}

	return temp, tempNoAdd
}

func getTRAOnlyChrPosition(text string) (string, int) {
	// fmt.Println(text)
	var firstChr int
	if text[0:1] == "[" || text[0:1] == "]" {
		firstChr = 1
	} else if text[1:2] == "[" || text[1:2] == "]" {
		firstChr = 2
	}

	var lastChr int
	if text[len(text)-1:] == "[" || text[len(text)-1:] == "]" {
		lastChr = len(text) - 1
	} else if text[len(text)-2:len(text)-1] == "[" || text[len(text)-2:len(text)-1] == "]" {
		lastChr = len(text) - 2
	}
	fmt.Println(text)
	s := strings.Split(text[firstChr:lastChr], ":")
	i, err := strconv.Atoi(s[1])
	if err != nil {
		// handle error
		fmt.Println(err)
		os.Exit(2)
	}

	return s[0], i

}

func benchmarkNormalTra(master, slave []benchmarkVCF) {
	master = removeDupbenchmarkVCF(master)
	slave = removeDupbenchmarkVCF(slave)
	bv1A, bv1N := compareTra(&master, &slave, 101)

	fmt.Println("Size Master :", len(master), "Size Slave :", len(slave))
	fmt.Println("Match:", len(bv1A), "Unmatch:", len(bv1N))

	var Vcount int
	var unmatch []benchmarkVCF
	for _, x := range master {
		if x.Verify {
			Vcount++
			// fmt.Println(x)
		} else {
			// fmt.Println(x)
			unmatch = append(unmatch, x)
		}
	}

	// sort.Slice(unmatch, func(i, j int) bool { return (unmatch)[i].CHR > (unmatch)[j].CHR })
	// for _, x := range unmatch {
	// 	fmt.Println(x)
	// }

	// sort.Slice(bv1N, func(i, j int) bool { return (bv1N)[i].CHR > (bv1N)[j].CHR })
	// for _, x := range bv1N {
	// 	fmt.Println(x)
	// }

	fmt.Println("Verify :", Vcount)
}

func compareTra(b1, b2 *[]benchmarkVCF, overlap int) ([]benchmarkVCF, []benchmarkVCF) {
	for x := 0; x < len(*b1); x++ {
		for y := 0; y < len(*b1); y++ {
			if (*b1)[x].CHR == (*b1)[y].CHR && (*b1)[x].CHREND == (*b1)[y].CHREND {
				max := overlap
				if checkBetween((*b1)[x].POS, (*b1)[y].POS, max, max) && checkBetween((*b1)[x].END, (*b1)[y].END, max, max) {
					// temp = append(temp, (*b1)[x])
					// added = true
					(*b1)[y].Verify = true
					(*b1)[x].Verify = true
					break
				}
			}
		}
	}

	for x := 0; x < len(*b1); x++ {
		for y := 0; y < len(*b1); y++ {
			if (*b1)[x].CHREND == (*b1)[y].CHR && (*b1)[x].CHR == (*b1)[y].CHREND {
				max := overlap
				if checkBetween((*b1)[x].END, (*b1)[y].POS, max, max) && checkBetween((*b1)[x].POS, (*b1)[y].END, max, max) {
					(*b1)[y].Verify = true
					(*b1)[x].Verify = true
					break
				}
			}
		}
	}

	var temp []benchmarkVCF
	var tempNoAdd []benchmarkVCF
	for _, x := range *b2 {
		if x.Verify {
			temp = append(temp, x)
		} else {
			tempNoAdd = append(tempNoAdd, x)
		}
	}

	return temp, tempNoAdd
}

// func bedBenchmark(path string) []benchmarkVCF {
// 	file, err := os.Open(path)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	defer file.Close()
// 	reader := bufio.NewReaderSize(file, 1024*100000)
// 	var benchamrkvcf []benchmarkVCF

// 	for {
// 		line, _, err := reader.ReadLine()

// 		if err == io.EOF {
// 			break
// 		}

// 		if strings.Contains(string(line), "#") {
// 			continue
// 		}

// 		if strings.Contains(string(line), "deletion") {
// 			continue
// 		}

// 		// s := strings.Split(string(line), "\t")
// 	}
// }

func dgv(path string) []benchmarkVCF {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*100000)
	var benchamrkvcf []benchmarkVCF

	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		s := strings.Split(string(line), "\t")
		// strings.Replace(string(line), " ", "\t", -1)
		infos := s[8]
		// fmt.Println(infos)
		info := strings.Split(infos, ";")
		var posi, endi int
		var validType bool
		for _, x := range info {
			// fmt.Println(x)
			if getinfoHeadDGV(x) == "inner_start" {
				posi, err = strconv.Atoi(getinfoValueDGV(x))
			}
			if getinfoHeadDGV(x) == "inner_end" {
				endi, err = strconv.Atoi(getinfoValueDGV(x))
			}
			if getinfoHeadDGV(x) == "variant_sub_type" {
				if getinfoValueDGV(x) == "Loss" {
					validType = true
				} else {
					break
				}
			}

			// if getinfoHeadDGV(x) == "variant_sub_type" {
			// 	if getinfoValueDGV(x) == "Gain" {
			// 		validType = true
			// 	} else {
			// 		break
			// 	}
			// }

		}

		if !validType {
			continue
		}

		if posi != 0 && endi != 0 {
			if endi-posi <= 50 {
				panic("stop")
			}

			benchamrkvcf = append(benchamrkvcf, benchmarkVCF{CHR: s[0],
				POS: posi,
				END: endi})
			// fmt.Println(s[0], posi, endi)
		}
		// panic("stop")

	}

	return benchamrkvcf
}

func getinfoHeadDGV(text string) string {
	return strings.Split(text, "=")[0]
}

func getinfoValueDGV(text string) string {
	return strings.Split(text, "=")[1]
}

func buildTrainingDataset(filepath string, origin []benchmarkVCF) {
	// return
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*10000)
	var count int
	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}
		count++
		text := string(line)
		pt := tsvtoPrintTrain(&text)

		var found bool
		for _, x := range origin {

			if x.CHR != pt.Chr {
				continue
			}
			max := 30
			if checkBetween(pt.Pos, x.POS, max, max) && checkBetween(pt.End, x.END, max, max) {

				// if checkBetween(pt.Pos, x.POS, max, max) {
				found = true
				pt.MLTrainPositive = true
				fmt.Println(ptsToTSV(&pt))
				break
			}

		}

		if !found {
			fmt.Println(ptsToTSV(&pt))
		}

	}

	// fmt.Println("count", count)

}
