package bolt

import (
	"bufio"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

func splitEOIndex(cacheData []string) ([]string, []string) {
	var Even, Odd []string

	var saveIndex []int
	for numberF, text := range cacheData {
		s := strings.Split(text, "\t")
		// orent := s[0]
		// if orent == "F" {
		// 	saveIndex = append(saveIndex, numberC)
		// }

		chr := s[1]
		Pos, err := strconv.Atoi(s[2])
		if err != nil {
			panic(err)
		}
		End, err := strconv.Atoi(s[4])
		if err != nil {
			panic(err)
		}
		var found bool

		for numberC, tCompare := range cacheData {
			sCom := strings.Split(tCompare, "\t")
			// COren := sCom[0]
			Cchr := sCom[1]
			CPos, err := strconv.Atoi(sCom[2])
			if err != nil {
				panic(err)
			}
			CEnd, err := strconv.Atoi(sCom[4])
			if err != nil {
				panic(err)
			}
			if Cchr != chr {
				continue
			}
			if tCompare == text {
				continue
			}
			if checkBetween(CPos, Pos, 100, 100) && checkBetween(CEnd, End, 100, 100) {
				found = true

				if foundOrderRead(numberF, saveIndex) {
					Even = append(Even, text)
				}
				saveIndex = append(saveIndex, numberF)
				saveIndex = append(saveIndex, numberC)

				// break
			}
		}

		if !found {
			Odd = append(Odd, text)
			// fmt.Println(text)
		}

	}

	return Even, Odd
}

func getTrainDataFromFiles(path string, svtype string) []*printTrain {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*10000)

	var bucket []*printTrain
	for {
		line, _, err := reader.ReadLine()
		if err == io.EOF {
			break
		}

		if svtype == "DEL" {
			text := string(line)
			pt := tsvtoPrintTrain(&text)

			bucket = append(bucket, &pt)
		} else if svtype == "INV" {
			text := string(line)
			pt := tsvtoPrintTrain(&text)
			bucket = append(bucket, &pt)

		} else if svtype == "TRA" {
			text := string(line)
			pt := tsvtoPrintTrain(&text)
			bucket = append(bucket, &pt)
		}

		text := string(line)
		pt := tsvtoPrintTrain(&text)
		bucket = append(bucket, &pt)

	}

	return bucket
}

func getListVariants(path string) []string {

	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*10000)

	var cacheData []string
	for {

		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		cacheData = append(cacheData, string(line))
	}

	return cacheData
}

func parsetoVCF(cacheData []string) []vCFFormat {

	var vcf []vCFFormat
	for _, text := range cacheData {
		text := string(text)
		pt := tsvtoPrintTrain(&text)

		var vcfTemp vCFFormat
		vcfTemp.POS = pt.Pos
		vcfTemp.CHROM = pt.Chr
		vcfTemp.FILTER = "PASS"
		vcfTemp.ALT = "."
		vcfTemp.REF = "."
		vcfTemp.ID = "BOLT"

		if pt.SVType == "TRA" {
			EndS := strconv.Itoa(pt.End)
			vcfTemp.ALT = `[` + pt.ChrEnd + `:` + EndS + `]`
			vcfTemp.INFO = "END=" + EndS + ";SVTYPE=BND"
			vcf = append(vcf, vcfTemp)
		} else {
			EndS := strconv.Itoa(pt.End)
			vcfTemp.INFO = "END=" + EndS + ";" +
				"SVTYPE=" + pt.SVType + ";"
			vcf = append(vcf, vcfTemp)
		}

	}
	return vcf
}
