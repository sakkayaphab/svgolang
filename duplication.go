package bolt

import (
	"fmt"
	"sort"
	"strings"
	"sync"

	"github.com/biogo/hts/sam"
)

func findDuplication(variantsC chan readContainer, calculateFinish chan bool, wgSV *sync.WaitGroup, jbs jobsSplitter, task *Task) {
	defer wgSV.Done()

	var eviCollect []evidentCollector
	SyncRecordFinish := make(chan bool)
	eviCollectBroadcast := make(chan evidentCollector)
	var wgSP sync.WaitGroup
	var muResult sync.Mutex
	for i := 0; i < task.CallerConfig.MaxConcurrentOfSR; i++ {
		wgSP.Add(1)
		go findDUPBreakPoint(eviCollectBroadcast, SyncRecordFinish, &wgSP, jbs, task, &muResult)
	}

	createFile(task.OutputPath + temppath + "dup/" + jbs.CHROM + ".txt")

	for {
		select {
		case variantList := <-variantsC:

			//append all read to evidence
			for i := 0; i < len(eviCollect); i++ {
				eviCollect[i].Read = append(eviCollect[i].Read, variantList.Read...)
			}

			//find evidence
			for m := 0; m < len(variantList.Read); m++ {
				// if FlagMultiSegment {
				sizeEvidenceIS := len(eviCollect)
				for i := 0; i < sizeEvidenceIS; i++ {
					if eviCollect[i].FirstIn {
						if eviCollect[i].POS+int(float32(task.sampleStats.InsertSizeMean)) < variantList.Read[m].Pos {
							if eviCollect[i].LEN >= 8 && eviCollect[i].InsertSize > 1000 {

								eviCollectBroadcast <- eviCollect[i]
								// fmt.Println("BC-F", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							} else if eviCollect[i].LEN >= 10 && eviCollect[i].InsertSize <= 1000 {
								eviCollectBroadcast <- eviCollect[i]
								// fmt.Println("BC-F", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							}
						}

						if len(eviCollect[i].Read) >= task.CallerConfig.LimitCacheReadPerEvidence {
							if eviCollect[i].LEN >= 3 {

								// fmt.Println("BC-F", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

								eviCollectBroadcast <- eviCollect[i]
								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							} else if eviCollect[i].LEN >= 3 && eviCollect[i].InsertSize > 1000 {
								// fmt.Println("BC-DF", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

								eviCollectBroadcast <- eviCollect[i]
								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							} else {
								// fmt.Println("BC-DF", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							}
						}

						// Lastin
					} else {
						if eviCollect[i].POS+int(float32(task.sampleStats.ReadLength)*4) < variantList.Read[m].Pos {
							if eviCollect[i].LEN >= 8 && eviCollect[i].InsertSize > 1000 {

								eviCollectBroadcast <- eviCollect[i]

								// fmt.Println("BC-L", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].POS-eviCollect[i].MATEPOS, eviCollect[i].InsertSize)

								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							} else if eviCollect[i].LEN >= 8 && eviCollect[i].InsertSize <= 1000 {
								eviCollectBroadcast <- eviCollect[i]

								// fmt.Println("BC-L", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].POS-eviCollect[i].MATEPOS, eviCollect[i].InsertSize)

								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							}
						}

						if len(eviCollect[i].Read) >= task.CallerConfig.LimitCacheReadPerEvidence {
							if eviCollect[i].LEN >= 4 {
								eviCollectBroadcast <- eviCollect[i]
								// fmt.Println("BC-L", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].POS-eviCollect[i].MATEPOS)

								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue

							} else {
								// fmt.Println("BC-L", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].POS-eviCollect[i].MATEPOS)

								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							}
						}
					}
				}

				findEvidenceDUP(variantList.Read[m], &eviCollect, &variantList, &jbs, task)
			}
		case <-calculateFinish:
			for i := 0; i < task.CallerConfig.MaxConcurrentOfSR; i++ {
				SyncRecordFinish <- true
			}

			eviCollectBroadcast = nil
			wgSP.Wait()

			return
		}
	}
}

func findEvidenceDUP(read *sam.Record, eviCollect *[]evidentCollector, variantList *readContainer, jbs *jobsSplitter, task *Task) {
	if read.Pos < read.MatePos && read.Strand() == -1 {
		if read.MateRef.Name() != read.Ref.Name() {
			return
		}
		// if read.Pos < 8197321+50 {
		// 	fmt.Println(read)
		// }

		sizeEvidenceIS := len(*eviCollect)
		var added bool
		for i := 0; i < sizeEvidenceIS; i++ {
			//grouping
			max := task.sampleStats.ReadLength * 3
			if checkBetween((*eviCollect)[i].POS, read.Pos, max, max) && checkBetween((*eviCollect)[i].MATEPOS, read.MatePos, max, max) {
				// eviCollect[i].MATEPOS = read[m].Rec.MatePos
				if !(*eviCollect)[i].Submited {
					(*eviCollect)[i].MapQRP = append((*eviCollect)[i].MapQRP, int(read.MapQ))

					(*eviCollect)[i].LEN++
					added = true
					break
				}
			}
		}

		if !added {
			// if getInsertSize(variantList.Read[m],flagsta)
			fmt.Println(read.Pos, read.MatePos)
			eviTemp := evidentCollector{
				CHR:     jbs.CHROM,
				POS:     read.Pos,
				MATEPOS: read.MatePos,
				LEN:     1}
			eviTemp.FirstIn = true
			eviTemp.MapQRP = append(eviTemp.MapQRP, int(read.MapQ))
			eviTemp.Read = append(eviTemp.Read, variantList.Read...)
			*eviCollect = append(*eviCollect, eviTemp)
		}
	} else if read.Pos > read.MatePos && read.Strand() == 1 {
		if read.MateRef.Name() != read.Ref.Name() {
			return
		}

		sizeEvidenceIS := len(*eviCollect)
		var added bool
		for i := 0; i < sizeEvidenceIS; i++ {
			//grouping
			max := task.sampleStats.ReadLength * 3
			if checkBetween((*eviCollect)[i].POS, read.Pos, max, max) && checkBetween((*eviCollect)[i].MATEPOS, read.MatePos, max, max) {
				if !(*eviCollect)[i].Submited {
					(*eviCollect)[i].LEN++
					(*eviCollect)[i].MapQRP = append((*eviCollect)[i].MapQRP, int(read.MapQ))

					added = true
					break
				}
			}
		}

		if !added {
			eviTemp := evidentCollector{
				CHR:     jbs.CHROM,
				POS:     read.Pos,
				MATEPOS: read.MatePos,
				LEN:     1}
			eviTemp.Read = append(eviTemp.Read, variantList.Read...)
			eviTemp.MapQRP = append(eviTemp.MapQRP, int(read.MapQ))
			*eviCollect = append(*eviCollect, eviTemp)
		}
	}
}

func findDUPBreakPoint(variantsC chan evidentCollector, calculateFinish chan bool, wg *sync.WaitGroup, jbs jobsSplitter, task *Task, muResult *sync.Mutex) {
	defer wg.Done()
	var count int
	var tempListPT []printTrain
	for {
		select {
		case variant := <-variantsC:
			var ptlist []printTrain
			// if variant.POS != 8232092 {
			// 	continue
			// }

			if variant.FirstIn {
				// fmt.Println("BC-F", variant.POS, variant.MATEPOS, "Read:", len(variant.Read), "LEN", variant.LEN, "INS", variant.MATEPOS-variant.POS)

				ptlist = findDUPBreakPointFirstIn(&variant, jbs, task)
			} else {
				ptlist = findDUPBreakPointLastIn(&variant, jbs, task)
			}

			if ptlist != nil {
				tempListPT = append(tempListPT, ptlist...)
				count++
			}

			if count > 20 {
				printTrainDataList(tempListPT, task.OutputPath+temppath+"dup/"+jbs.CHROM, muResult)
				tempListPT = nil
				count = 0
			}

		case <-calculateFinish:
			printTrainDataList(tempListPT, task.OutputPath+temppath+"dup/"+jbs.CHROM, muResult)
			tempListPT = nil
			count = 0
			return
		}
	}
}

func findDUPBreakPointLastIn(evidence *evidentCollector, jbs jobsSplitter, task *Task) []printTrain {
	var startPosAlign, endPosAlign int
	// Align
	startPosAlign = evidence.MATEPOS - task.sampleStats.ReadLength*4 - int(task.sampleStats.InsertSizeSD)
	endPosAlign = evidence.MATEPOS + task.sampleStats.ReadLength + int(task.sampleStats.InsertSizeSD)

	if endPosAlign > evidence.POS {
		endPosAlign = evidence.POS - task.sampleStats.ReadLength
	}

	if startPosAlign < 0 {
		startPosAlign = 0
	}

	maxReferenceEND := task.getReferenceEND(evidence.CHR)

	if maxReferenceEND <= 0 {
		return nil
	}

	if endPosAlign > maxReferenceEND {
		endPosAlign = maxReferenceEND
	}

	if startPosAlign > endPosAlign {
		return nil
	}

	seq, err := task.fastaReader.GetSequence(evidence.CHR, startPosAlign, endPosAlign)
	if err != nil {
		return nil
	}
	seq = strings.ToUpper(seq)
	bwt := newBWT()

	bwt.Transform(seq)
	bp := make(map[simpleBreakpoint]breakpointinfo)

	for orderhit, read := range evidence.Read {
		if getNumberSoftClippedLast(read) <= 2 {
			continue
		}

		if len(read.Seq.Seq) == 0 {
			continue
		}

		if read.Pos < evidence.POS-task.sampleStats.ReadLength*6 {
			continue
		}

		if read.Pos > evidence.POS+task.sampleStats.ReadLength*6 {
			continue
		}

		var plusFirstSoftclipped int
		if read.Cigar[0].Type().String() == "S" {
			plusFirstSoftclipped = read.Cigar[0].Len()
		}

		sizeSoftClipped := 10
		realSCsize := 0
		if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {
			sizeSoftClipped = read.Cigar[len(read.Cigar)-1].Len()
			realSCsize = read.Cigar[len(read.Cigar)-1].Len()
		}

		var score []bWTScore
		if sizeSoftClipped > setMaxRangeSkipBP {
			score = bwt.locateReverseRead(read, sizeSoftClipped-5)
		} else {
			score = bwt.locateReverseRead(read, sizeSoftClipped)
		}

		if realSCsize < 2 {
			continue
		}

		for _, x := range score {
			if x.HIT == task.sampleStats.ReadLength {
				continue
			}

			if x.HIT >= 4 {

			} else {
				continue
			}

			// if x.HIT < sizeSoftClipped {
			// 	continue
			// }

			// fmt.Println(string(read.Seq.Expand()))
			// fmt.Println("---->", startPosAlign+x.Pos+1, read.Seq.Length-x.HIT+read.Pos-plusFirstSoftclipped, x.HIT, x)
			// bp[simpleBreakpoint{read.Rec.Seq.Length - x.HIT + read.Rec.Pos - plusFirstSoftclipped, startPosAlign + x.Pos + 1}]++

			oneBP := bp[simpleBreakpoint{startPosAlign + x.Pos + 1, read.Seq.Length - x.HIT + read.Pos - plusFirstSoftclipped}]

			oneBP.hit++
			oneBP.longhit = append(oneBP.longhit, x.HIT)
			oneBP.softclippedhit = append(oneBP.softclippedhit, sizeSoftClipped)

			oneBP.orderhit = append(oneBP.orderhit, orderhit)
			// oneBP.alldepth = bufferReadSize

			bp[simpleBreakpoint{startPosAlign + x.Pos + 1, read.Seq.Length - x.HIT + read.Pos - plusFirstSoftclipped}] = oneBP
		}

	}

	var bks []breakPointState
	for i, x := range bp {
		if checkAddBreakpointState(&x, "DUP") {

			bks = append(bks, breakPointState{
				POS:            i.POS,
				END:            i.END,
				HIT:            x.hit,
				INSERTSIZE:     i.END - i.POS,
				ALLDEPTH:       x.alldepth,
				longhit:        x.longhit,
				softclippedhit: x.softclippedhit,
				orderhit:       x.orderhit,
				ReadPairCount:  evidence.LEN,
			})
		}
	}

	sort.Slice(bks, func(i, j int) bool { return (bks)[i].HIT > (bks)[j].HIT })
	var PTresults []printTrain

	for _, x := range bks {
		if x.HIT >= 2 {

		} else {
			continue
		}

		if !isInLimitRange(x.POS, x.END) {
			fmt.Println("#CODE0x2", "isInLimitRange DUP", x.POS, x.END)
			continue
		}

		x.SVType = "DUP"
		x.Orientation = "L"
		x.MapQRP = evidence.MapQRP

		aPT := printPileUp(evidence, &x, &jbs, task)
		PTresults = append(PTresults, aPT)

	}
	return PTresults
}

func findDUPBreakPointFirstIn(evidence *evidentCollector, jbs jobsSplitter, task *Task) []printTrain {
	var startPosAlign, endPosAlign int
	// Align
	endPosAlign = evidence.MATEPOS + task.sampleStats.ReadLength*6 + int(task.sampleStats.InsertSizeSD)
	startPosAlign = evidence.MATEPOS - task.sampleStats.ReadLength - int(task.sampleStats.InsertSizeSD)
	if startPosAlign < evidence.POS {
		startPosAlign = evidence.POS
	}

	if startPosAlign < 0 {
		startPosAlign = 0
	}

	maxReferenceEND := task.getReferenceEND(evidence.CHR)

	if maxReferenceEND <= 0 {
		return nil
	}

	if endPosAlign > maxReferenceEND {
		endPosAlign = maxReferenceEND
	}

	seq, err := task.fastaReader.GetSequence(evidence.CHR, startPosAlign, endPosAlign)
	if err != nil {
		return nil
	}
	seq = strings.ToUpper(seq)
	bwt := newBWT()
	// fmt.Println(evidence.CHR, startPosAlign, endPosAlign)
	bwt.Transform(seq)
	bp := make(map[simpleBreakpoint]breakpointinfo)

	for _, read := range evidence.Read {

		if getNumberSoftClippedFirst(read) <= 3 {
			continue
		}

		if len(read.Seq.Seq) == 0 {
			continue
		}

		if read.Pos < evidence.POS-task.sampleStats.ReadLength {
			continue
		}

		if read.Pos > evidence.POS+task.sampleStats.ReadLength*6 {
			continue
		}

		var firstSoftclipped int
		if read.Cigar[0].Type().String() == "S" {
			firstSoftclipped = read.Cigar[0].Len()
		}

		sizeSoftClipped := 4
		if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {
			sizeSoftClipped = read.Cigar[len(read.Cigar)-1].Len()
		}

		// score := bwt.locateRead(read, sizeSoftClipped)
		var score []bWTScore
		if sizeSoftClipped > setMaxRangeSkipBP {
			score = bwt.locateRead(read, sizeSoftClipped-5)
		} else {
			score = bwt.locateRead(read, sizeSoftClipped)
		}

		for orderhit, x := range score {

			if x.HIT >= 4 {

			} else {
				continue
			}

			oneBP := bp[simpleBreakpoint{read.Pos - firstSoftclipped + x.HIT + 1, endPosAlign - x.Pos}]

			oneBP.hit++
			oneBP.longhit = append(oneBP.longhit, x.HIT)
			oneBP.softclippedhit = append(oneBP.softclippedhit, sizeSoftClipped)

			oneBP.orderhit = append(oneBP.orderhit, orderhit)
			// oneBP.alldepth = bufferReadSize

			bp[simpleBreakpoint{read.Pos - firstSoftclipped + x.HIT + 1, endPosAlign - x.Pos}] = oneBP
		}

	}

	var bks []breakPointState
	for i, x := range bp {
		if checkAddBreakpointState(&x, "DUP") {
			// getRelativeReads(evidence.Read, &x, "F", i.POS, evidence.CHR, i.END, "F", task)
			bks = append(bks, breakPointState{
				POS:            i.POS,
				END:            i.END,
				HIT:            x.hit,
				INSERTSIZE:     i.END - i.POS,
				ALLDEPTH:       x.alldepth,
				longhit:        x.longhit,
				softclippedhit: x.softclippedhit,
				orderhit:       x.orderhit,
				ReadPairCount:  evidence.LEN,
			})
		}
	}

	sort.Slice(bks, func(i, j int) bool { return (bks)[i].HIT > (bks)[j].HIT })

	var PTresults []printTrain

	for _, x := range bks {

		if x.HIT >= 2 {

		} else {
			continue
		}

		if !isInLimitRange(x.POS, x.END) {
			fmt.Println("#CODE0x2", "isInLimitRange DUP", x.POS, x.END)
			continue
		}

		x.SVType = "DUP"
		x.Orientation = "F"
		x.MapQRP = evidence.MapQRP
		aPT := printPileUp(evidence, &x, &jbs, task)
		PTresults = append(PTresults, aPT)
	}
	return PTresults

}

func checkAddBreakpointState(bpi *breakpointinfo, svtype string) bool {
	if svtype == "DUP" {
		if bpi.hit >= 2 && getMaximumSliceInt(&bpi.longhit) >= 5 {
			return true
		}
	}

	if bpi.hit >= 2 && getMaximumSliceInt(&bpi.longhit) >= 7 {
		return true
	}

	return false
}

func getMaximumSliceInt(longhit *[]int) int {
	var maxlonghit int
	for i := 0; i < len(*longhit); i++ {
		if maxlonghit < (*longhit)[i] {
			maxlonghit = (*longhit)[i]
		}
	}

	return maxlonghit
}
