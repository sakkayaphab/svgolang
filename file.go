package bolt

import (
	"bufio"
	"fmt"
	"os"
)

func createFile(pathfile string) {
	// detect if file exists
	var _, err = os.Stat(pathfile)

	if os.IsNotExist(err) {
		var file, err = os.Create(pathfile)
		if err != nil {
			return
		}
		defer file.Close()
	} else {
		err = os.Remove(pathfile)
		if err != nil {
			return
		}

		var file, err = os.Create(pathfile)
		if err != nil {
			return
		}
		defer file.Close()
	}

}

func genarateTempOutput(filename string, variant *[]string) {

	f, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	w := bufio.NewWriter(f)

	// fmt.Fprintln(w, "#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO")

	for i := 0; i < len(*variant); i++ {
		// tempQual := "."
		// pos := strconv.Itoa((*vcf)[i].POS)
		fmt.Fprintln(w, (*variant)[i])
	}
	w.Flush()
}

func createTempFile(task *Task) {
	//Remove all temp
	// os.RemoveAll(task.OutputPath)
	//Add file
	err := os.MkdirAll(task.OutputPath, os.ModePerm)
	if err != nil {
		panic(err)
	}

	err = os.MkdirAll(task.OutputPath+"/analysis", os.ModePerm)
	if err != nil {
		panic(err)
	}
	err = os.MkdirAll(task.OutputPath+"/results", os.ModePerm)
	if err != nil {
		panic(err)
	}

	err = os.MkdirAll(task.OutputPath+temppath[:len(temppath)-1], os.ModePerm)
	if err != nil {
		panic(err)
	}

	// err = os.MkdirAll(task.OutputPath+temppath+"indel", os.ModePerm)
	// if err != nil {
	// 	panic(err)
	// }
	err = os.MkdirAll(task.OutputPath+temppath+"inv", os.ModePerm)
	if err != nil {
		panic(err)
	}
	err = os.MkdirAll(task.OutputPath+temppath+"tra", os.ModePerm)
	if err != nil {
		panic(err)
	}
	// err = os.MkdirAll(task.OutputPath+temppath+"snv", os.ModePerm)
	// if err != nil {
	// 	panic(err)
	// }
	err = os.MkdirAll(task.OutputPath+temppath+"ins", os.ModePerm)
	if err != nil {
		panic(err)
	}
	err = os.MkdirAll(task.OutputPath+temppath+"del", os.ModePerm)
	if err != nil {
		panic(err)
	}
	err = os.MkdirAll(task.OutputPath+temppath+"dup", os.ModePerm)
	if err != nil {
		panic(err)
	}

	err = os.MkdirAll(task.OutputPath+analysispath+"inv", os.ModePerm)
	if err != nil {
		panic(err)
	}
	err = os.MkdirAll(task.OutputPath+analysispath+"tra", os.ModePerm)
	if err != nil {
		panic(err)
	}
	// err = os.MkdirAll(task.OutputPath+temppath+"snv", os.ModePerm)
	// if err != nil {
	// 	panic(err)
	// }
	err = os.MkdirAll(task.OutputPath+analysispath+"ins", os.ModePerm)
	if err != nil {
		panic(err)
	}
	err = os.MkdirAll(task.OutputPath+analysispath+"del", os.ModePerm)
	if err != nil {
		panic(err)
	}
	err = os.MkdirAll(task.OutputPath+analysispath+"dup", os.ModePerm)
	if err != nil {
		panic(err)
	}
}
