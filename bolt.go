// Package bolt for structural variants
//  func main() {
//      task := bolt.New()
//      task.SamplePath = "/home/xxx/sample/NA12878.hiseq.wgs.bwa.raw.bam"
//      task.ReferencePath = "/home/xxx/reference/Homo_sapiens_assembly18.fasta"
//      task.OutputPath = "/home/xxx/sample/result"
//      task.Execute()
//  }
package bolt

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"sync"

	"github.com/sakkayaphab/fastalib"
)

const (
	setMaxRangeSkipBP     = 30
	pileupCountScMoreThan = 2
	temppath              = "/temp/"
	analysispath          = "/analysis/"
)

//Task is the main structure of this library.
//You should create this object from  New() method,It will provides some configulations.
type Task struct {
	// New Task Name
	Name string
	// Sample path (BAM file)
	SamplePath string
	// Reference path (Fasta file)
	ReferencePath string
	// Output path (folder)
	OutputPath  string
	fastaReader *fastalib.FastaReader
	// bamReader    *bam.Reader
	// bamIndex     *bam.Index
	sampleStats  sampleStats
	jobsSplitter []jobsSplitter
	mode         string
	// Config parameter in caller
	CallerConfig CallerConfig
}

// CallerConfig config type of variants
type CallerConfig struct {
	DeletionConfig            DeletionConfig
	LimitCacheReadPerEvidence int
	PEMaxSpanCollect          int
	MaxConcurrentOfSR         int
	MaxConcurrentChr          int
}

// DeletionConfig config deletion
type DeletionConfig struct {
	//small mean less than 250 bp
	PEMinimumCountSmall  int
	PEMinimumCountMedium int
	PEMinimumCountLarge  int
}

type rangeCHROM struct {
	Name string
	END  int
}

type sampleStats struct {
	ReadLength     int
	InsertSizeMean int
	InsertSizeSD   int
	rangeCHROM     []rangeCHROM
}

// New creates a new task with default configuration
func New() *Task {
	var task Task
	task.CallerConfig.DeletionConfig.PEMinimumCountSmall = 4
	task.CallerConfig.DeletionConfig.PEMinimumCountMedium = 4
	task.CallerConfig.DeletionConfig.PEMinimumCountLarge = 4
	task.CallerConfig.LimitCacheReadPerEvidence = 3000
	// task.CallerConfig.PEMaxSpanCollect = 1000
	task.CallerConfig.MaxConcurrentOfSR = 20
	task.CallerConfig.MaxConcurrentChr = 20
	return &task
}

// Execute task from task
func (task *Task) Execute() {

	concurrent := 24
	task.fastaReader, _ = fastalib.New(task.ReferencePath)
	createTempFile(task)

	err := (task.fastaReader).InitfaIndex(task.fastaReader.FastaFile + ".fai")
	if err != nil {
		panic(err)
	}
	task.getReadSize()
	task.getMeanSD()

	jbs := task.splitjobs(task.SamplePath)

	jbsC := make(chan jobsSplitter)

	if task.CallerConfig.MaxConcurrentChr > 0 {
		concurrent = task.CallerConfig.MaxConcurrentChr
	}

	if concurrent > len(jbs) {
		concurrent = len(jbs)
	}

	var wgChr sync.WaitGroup
	for i := 0; i < concurrent; i++ {
		wgChr.Add(1)
		go readBam(jbsC, task.SamplePath, &wgChr, i, task)
	}
	for i := 0; i < len(jbs)+concurrent; i++ {
		if i < len(jbs) {
			jbsC <- jbs[i]
		} else {
			temp := jobsSplitter{CHROM: "close"}
			jbsC <- temp
		}
	}

	wgChr.Wait()

	// sortTSVfiles(task.OutputPath, task)
	// convertVCF(task.OutputPath, task)
}

// ExecuteDebug for debugging
func (task *Task) ExecuteDebug() {
	// configtest(task)
	task.mode = "DEBUG"

	// testNN()
	// return
	// gziptest()
	// bwt()
	// startCompress("/data/users/wichadak/kan/result/Hiseq/del/chr1.txt", "compress.gz")
	// startML()
	// testFUN()
	// var cigars []sam.Cigar
	// cigar := sam.NewCigarOp(sam.CigarMatch, 101)
	// cigars = append(cigar, cigars)
	// rec, err := sam.NewRecord("0", nil, nil, 0, 500, 101, 60, cigar, nil, nil, nil)
	// traindataPrepare()
	// return
	// if false {
	if true {
		task.sampleStats.ReadLength = 101
		// manta()
		// mlRun()
		// convertVCF("/Users/sakkayaphab/Downloads/result/S1", task)
		// convertVCF("/Users/sakkayaphab/Downloads/result/del1000", task)
		// convertVCF("/Users/sakkayaphab/Downloads/result/dup1000", task)
		// convertVCF("/Users/sakkayaphab/Downloads/result/inv1000")
		// convertVCF("/Users/sakkayaphab/Downloads/result/newallsim100x/", task)
		// convertVCF("/Users/sakkayaphab/Downloads/result/Hiseq", task)
		// convertVCF("/data/users/wichadak/kan/result/Hiseq", task)
		// convertVCF("/Users/sakkayaphab/Downloads/result/newallsim100x", task)
		// convertVCF("/Users/sakkayaphab/Downloads/result/Hiseq", task)
		// sortTSVfiles("/data/users/wichadak/kan/result/sur100x", task)
		// convertVCF("/data/users/wichadak/kan/result/sur100x", task)

		// sortTSVfiles("/data/users/wichadak/kan/result/sur20x", task)
		// convertVCF("/data/users/wichadak/kan/result/sur20x", task)

		// sortTSVfiles("/data/users/wichadak/kan/result/Hiseq", task)
		// convertVCF("/data/users/wichadak/kan/result/Hiseq", task)

		sortTSVfiles("/data/users/wichadak/kan/result/S1", task)
		convertVCF("/data/users/wichadak/kan/result/S1", task)

		// sortTSVfiles("/Users/sakkayaphab/Downloads/result/sur20x", task)
		// convertVCF("/Users/sakkayaphab/Downloads/result/sur20x", task)
		runBenchmarkvcf()
		// convertVCF("/Users/sakkayaphab/Downloads/result/S1")
		// statTrain("/Users/sakkayaphab/Downloads/result/S1/deltrue")
		// readFile("/Users/sakkayaphab/Downloads/result/S1/del/chr1.txt")
		return
	}

	// MD
	task.OutputPath = "temp"
	//new20x
	// task.SamplePath = "/data/users/wichadak/kan/wgsim/survi.20x.bam"
	// task.ReferencePath = "/data/users/wichadak/kan/reference/ucsc_hg19.fa"
	// Hiseq
	task.SamplePath = "/data/users/wichadak/kan/sample/NA12878.hiseq.wgs.bwa.raw.bam"
	task.ReferencePath = "/data/users/wichadak/kan/reference/Homo_sapiens_assembly18.fasta"

	task.fastaReader, _ = fastalib.New(task.ReferencePath)
	createTempFile(task)

	err := (task.fastaReader).InitfaIndex(task.fastaReader.FastaFile + ".fai")
	if err != nil {
		panic(err)
	}

	task.getReadSize()
	task.getMeanSD()

	jbs := task.splitjobs(task.SamplePath)

	jbsC := make(chan jobsSplitter)

	concurrent := 1
	if concurrent > len(jbs) {
		concurrent = len(jbs)
	}
	var wgChr sync.WaitGroup
	for i := 0; i < concurrent; i++ {
		wgChr.Add(1)
		go readBam(jbsC, task.SamplePath, &wgChr, i, task)
	}
	setPos := 0
	for i := range jbs {
		if jbs[i].CHROM == "chr1" {
			setPos = i
		}
	}
	for i := setPos; i < len(jbs)+concurrent; i++ {
		if i < len(jbs) {
			jbsC <- jbs[i]
		} else {
			temp := jobsSplitter{CHROM: "close"}
			jbsC <- temp
		}
	}

	wgChr.Wait()

}

func configtest(task *Task) {
	task.mode = "DEBUG"
	task.sampleStats.ReadLength = 101
	// sortTSVfiles("/data/users/wichadak/kan/result/Hiseq", task)
	convertVCF("/data/users/wichadak/kan/result/Hiseq", task)
}

func traindataPrepare() {
	path := "/data/users/wichadak/kan/golibrary/src/github.com/sakkayaphab/trainingdata/traindup.txt"
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*10000)
	count := 0
	for {
		line, _, err := reader.ReadLine()
		if err == io.EOF {
			break
		}

		text := string(line)
		pt := tsvtoPrintTrain(&text)
		if pt.MLTrainPositive == false && (pt.LongMaxHit < 16 || pt.MaxMapQRP != 60) {
			fmt.Println(text)
			count++
		}

		if count >= 68677 {
			break
		}
	}
}
