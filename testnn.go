package bolt

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
)

func testNN() {
	file, err := os.Open("/Users/sakkayaphab/Downloads/result/chrX.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := bufio.NewReaderSize(file, 1024*10000)

	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		var oddString []string
		oddString = append(oddString, string(line))
		task := New()
		task.sampleStats.ReadLength = 101

		yesVariantodd, _ := filterWithNN(&oddString, task, "DEL")

		fmt.Println(yesVariantodd)
	}

}

// func testSort() {
// 	file, err := os.Open("/Users/sakkayaphab/Downloads/result/chrX.txt")
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	defer file.Close()
// 	reader := bufio.NewReaderSize(file, 1024*10000)

// 	for {
// 		line, _, err := reader.ReadLine()

// 		if err == io.EOF {
// 			break
// 		}

// 		var oddString []string
// 		oddString = append(oddString, string(line))
// 		task := New()
// 		task.sampleStats.ReadLength = 101

// 		yesVariantodd, _ := filterWithNN(&oddString, task, "DEL")

// 		fmt.Println(yesVariantodd)
// 	}
// }
