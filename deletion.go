package bolt

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strings"
	"sync"

	"github.com/biogo/hts/sam"
)

type evidentCollector struct {
	ID          int
	FirstIn     bool
	CHR         string
	InsertSize  int
	POS         int
	MATEPOS     int
	LEN         int
	Submited    bool
	Collected   bool
	Read        []*sam.Record
	SATag       int
	InsUnmapped bool
	MapQRP      []int
}

func findDeletion(variantsC chan readContainer, calculateFinish chan bool, wgSV *sync.WaitGroup, jbs jobsSplitter, task *Task) {
	defer wgSV.Done()
	var wgSP sync.WaitGroup

	//Find Breakpoint
	var eviCollect []evidentCollector
	var muResult sync.Mutex
	SyncRecordFinish := make(chan bool)
	eviCollectBroadcast := make(chan evidentCollector)

	for i := 0; i < task.CallerConfig.MaxConcurrentOfSR; i++ {
		wgSP.Add(1)
		go findDelBreakPoint(eviCollectBroadcast, SyncRecordFinish, &wgSP, jbs, task, &muResult)
	}
	createFile(task.OutputPath + temppath + "del/" + jbs.CHROM + ".txt")
	var id int

	for {
		select {
		case variantList := <-variantsC:
			// getsplitreadData(variantList.Read)
			//append all read to evidence
			for i := 0; i < len(eviCollect); i++ {
				eviCollect[i].Read = append(eviCollect[i].Read, variantList.Read...)
			}

			// for i := 0; i < len(eviCollect); i++ {
			// 	eviCollect[i].Read = append(eviCollect[i].Read, variantList.SCRead...)
			// }

			//find evidence
			for m := 0; m < len(variantList.Read); m++ {
				sizeEvidenceIS := len(eviCollect)
				for i := 0; i < sizeEvidenceIS; i++ {
					if eviCollect[i].FirstIn {
						if eviCollect[i].POS+int(float32(task.sampleStats.InsertSizeMean)) < variantList.Read[m].Pos {
							if eviCollect[i].LEN >= 10 && eviCollect[i].InsertSize > 1000 {

								eviCollectBroadcast <- eviCollect[i]
								// fmt.Println("BC-F", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							} else if eviCollect[i].LEN >= 10 && eviCollect[i].InsertSize <= 1000 {
								eviCollectBroadcast <- eviCollect[i]
								// fmt.Println("BC-F", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							}
						}

						if len(eviCollect[i].Read) >= task.CallerConfig.LimitCacheReadPerEvidence {
							if eviCollect[i].LEN >= 3 && eviCollect[i].InsertSize < 1000 {

								// fmt.Println("BC-F", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

								eviCollectBroadcast <- eviCollect[i]
								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							} else if eviCollect[i].LEN >= 2 && eviCollect[i].InsertSize > 1000 {

								// fmt.Println("BC-F", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

								eviCollectBroadcast <- eviCollect[i]
								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							} else {
								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								// fmt.Println("BC-FDD", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].MATEPOS-eviCollect[i].POS)

								sizeEvidenceIS--
								i--
								continue
							}

						}

						// Lastin
					} else {
						if eviCollect[i].POS+int(float32(task.sampleStats.ReadLength)*2) < variantList.Read[m].Pos {
							if eviCollect[i].LEN >= 10 && eviCollect[i].InsertSize > 1000 {

								eviCollectBroadcast <- eviCollect[i]

								// fmt.Println("BC-L", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].POS-eviCollect[i].MATEPOS, eviCollect[i].InsertSize)

								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							} else if eviCollect[i].LEN >= 10 && eviCollect[i].InsertSize <= 1000 {
								eviCollectBroadcast <- eviCollect[i]

								// fmt.Println("BC-L", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].POS-eviCollect[i].MATEPOS, eviCollect[i].InsertSize)

								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							}
						}

						if len(eviCollect[i].Read) >= task.CallerConfig.LimitCacheReadPerEvidence {
							if eviCollect[i].LEN >= 5 {
								eviCollectBroadcast <- eviCollect[i]
								// fmt.Println("BC-L", eviCollect[i].POS, eviCollect[i].MATEPOS, "Read:", len(eviCollect[i].Read), "LEN", eviCollect[i].LEN, "INS", eviCollect[i].POS-eviCollect[i].MATEPOS)

								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue

							} else {
								eviCollect = append(eviCollect[:i], eviCollect[i+1:]...)
								sizeEvidenceIS--
								i--
								continue
							}
						}
					}
				}

				if variantList.Read[m].Ref.Name() != variantList.Read[m].MateRef.Name() {
					continue
				}

				if variantList.Read[m].Pos <= variantList.Read[m].MatePos && variantList.Read[m].Strand() == 1 {
					insertSize := int((variantList.Read[m].MatePos + task.sampleStats.ReadLength) - variantList.Read[m].Pos)
					// fmt.Println(id, insertSize, task.sampleStats.InsertSizeMean+task.sampleStats.InsertSizeSD)
					// //check if there insert size is abnormal
					if insertSize >= task.sampleStats.InsertSizeMean+task.sampleStats.InsertSizeSD {
						sizeEvidenceIS := len(eviCollect)
						var added bool
						for i := 0; i < sizeEvidenceIS; i++ {
							//grouping
							max := (task.sampleStats.ReadLength) + int(task.sampleStats.InsertSizeSD)

							if checkBetween(int(eviCollect[i].InsertSize), int(insertSize), max, max) {
								if eviCollect[i].FirstIn {
									eviCollect[i].LEN++
									eviCollect[i].MapQRP = append(eviCollect[i].MapQRP, int(variantList.Read[m].MapQ))

									added = true
									break
								}
							}
						}

						if !added {
							id++
							eviTemp := evidentCollector{ID: id, InsertSize: insertSize,
								CHR:     jbs.CHROM,
								POS:     variantList.Read[m].Pos,
								MATEPOS: variantList.Read[m].MatePos,
								// MaxMapQRP: int(variantList.Read[m].MapQ),
								LEN: 1}
							eviTemp.FirstIn = true
							eviTemp.MapQRP = append(eviTemp.MapQRP, int(variantList.Read[m].MapQ))
							eviTemp.Read = append(eviTemp.Read, variantList.Read...)
							// eviTemp.Read = append(eviTemp.Read, variantList.SCRead...)

							eviCollect = append(eviCollect, eviTemp)
						}
					}
				} else if variantList.Read[m].Pos > variantList.Read[m].MatePos && variantList.Read[m].Strand() == -1 {
					insertSize := int((variantList.Read[m].Pos + task.sampleStats.ReadLength) - variantList.Read[m].MatePos)

					// check if there insert size is abnormal
					if insertSize >= task.sampleStats.InsertSizeMean+task.sampleStats.InsertSizeSD {

						sizeEvidenceIS := len(eviCollect)
						var added bool
						for i := 0; i < sizeEvidenceIS; i++ {
							//grouping
							max := task.sampleStats.ReadLength * 3
							if checkBetween(eviCollect[i].POS, variantList.Read[m].Pos, max, max) && checkBetween(eviCollect[i].MATEPOS, variantList.Read[m].MatePos, max, max) {
								eviCollect[i].LEN++
								eviCollect[i].MapQRP = append(eviCollect[i].MapQRP, int(variantList.Read[m].MapQ))

								added = true
								break
							}
						}

						if !added {
							id++
							eviTemp := evidentCollector{ID: id, InsertSize: insertSize,
								CHR:     jbs.CHROM,
								POS:     variantList.Read[m].Pos,
								MATEPOS: variantList.Read[m].MatePos,
								LEN:     1}
							eviTemp.FirstIn = false
							eviTemp.MapQRP = append(eviTemp.MapQRP, int(variantList.Read[m].MapQ))
							eviTemp.Read = append(eviTemp.Read, variantList.Read...)
							// eviTemp.Read = append(eviTemp.Read, variantList.SCRead...)

							eviCollect = append(eviCollect, eviTemp)
						}
					}
				}
			}

		case <-calculateFinish:
			for i := 0; i < task.CallerConfig.MaxConcurrentOfSR; i++ {
				SyncRecordFinish <- true
			}

			eviCollectBroadcast = nil
			wgSP.Wait()

			return
		}
	}
}

type vCFFormatExtended struct {
	CHROM  string
	POS    int
	END    int
	ID     string
	REF    string
	ALT    string
	QUAL   int
	FILTER string
	INFO   string
}

type simpleBreakpoint struct {
	POS int
	END int
}

type breakpointinfo struct {
	hit               int
	longhit           []int
	orderhit          []int
	read              []*sam.Record
	softclippedhit    []int
	softclippedunhit  []int
	alldepth          int
	orientation       string
	matechrnotcorrect bool
	matchleft         bool
	matchright        bool
}

type breakPointState struct {
	SVType           string
	Orientation      string
	POS              int
	END              int
	INSERTSIZE       int
	HIT              int
	ALLDEPTH         int
	orderhit         []int
	longhit          []int
	softclippedhit   []int
	softclippedunhit []int
	unhitreadNoSC    int
	unhitreadSC      int
	hitreadNoSC      int
	hitreadSC        int
	ReadPairCount    int
	MaxMapQRP        int
	matchleft        bool
	matchright       bool
	MapQRP           []int
}

func findDelBreakPoint(variantsC chan evidentCollector, SyncRecordFinish chan bool, wg *sync.WaitGroup, jbs jobsSplitter, task *Task, muResult *sync.Mutex) {
	defer wg.Done()
	var count int
	var tempListPT []printTrain

	for {
		select {
		case variant := <-variantsC:
			// if variant.POS == 220440471 {

			// } else {
			// 	continue
			// }

			// if variant.FirstIn {
			// 	if variant.MATEPOS-variant.POS > 3000 {
			// 		fmt.Println("BC", variant.FirstIn, variant.POS, variant.MATEPOS, "Read:", len(variant.Read), "LEN", variant.LEN, "INS", variant.MATEPOS-variant.POS)

			// 	}

			// } else {
			// 	// fmt.Println("BC", variant.FirstIn, variant.MATEPOS, variant.POS, "Read:", len(variant.Read), "LEN", variant.LEN, "INS", variant.MATEPOS-variant.POS)

			// }

			var ptlist []printTrain
			if variant.FirstIn {
				ptlist = findDelBreakPointFirstIn(&variant, jbs, task)
			} else {
				ptlist = findDelBreakPointLastIn(&variant, jbs, task)
			}

			if ptlist != nil {
				tempListPT = append(tempListPT, ptlist...)
				count++
			}

			if count > 10 {
				// for _, x := range tempListPT {
				// 	fmt.Println(ptsToTSV(&x))
				// }
				printTrainDataList(tempListPT, task.OutputPath+temppath+"del/"+jbs.CHROM, muResult)
				tempListPT = nil
				count = 0

			}
		case <-SyncRecordFinish:
			printTrainDataList(tempListPT, task.OutputPath+temppath+"del/"+jbs.CHROM, muResult)
			tempListPT = nil
			count = 0
			return
		}
	}
}

// func WriteToFile(i int, f *os.File, w *sync.WaitGroup, muResult *sync.Mutex) {
// 	mu.Lock()
// 	defer mu.Unlock()
// 	// etc...
// }

func findDelBreakPointLastIn(evidence *evidentCollector, jbs jobsSplitter, task *Task) []printTrain {
	// fmt.Println(evidence.CHR, evidence.POS, evidence.MATEPOS)

	var startPosAlign, endPosAlign int
	// Align
	startPosAlign = evidence.MATEPOS - task.sampleStats.ReadLength*1 - int(task.sampleStats.InsertSizeSD)
	endPosAlign = evidence.MATEPOS + task.sampleStats.ReadLength*15 + int(task.sampleStats.InsertSizeSD)

	if endPosAlign > evidence.POS {
		endPosAlign = evidence.POS - task.sampleStats.ReadLength
	}

	if startPosAlign < 0 {
		startPosAlign = 0
	}

	maxReferenceEND := task.getReferenceEND(evidence.CHR)

	if maxReferenceEND <= 0 {
		return nil
	}

	if endPosAlign > maxReferenceEND {
		endPosAlign = maxReferenceEND
	}

	seq, err := task.fastaReader.GetSequence(evidence.CHR, startPosAlign, endPosAlign)
	if err != nil {
		return nil
	}
	// fmt.Println(evidence.CHR, startPosAlign, endPosAlign)
	// if startPosAlign != 242571167 {
	// 	return
	// }
	seq = strings.ToUpper(seq)
	bwt := newBWT()

	bwt.Transform(seq)
	bp := make(map[simpleBreakpoint]breakpointinfo)

	for orderhit, read := range evidence.Read {
		if getNumberSoftClippedFirst(read) <= 3 {
			continue
		}

		if len(read.Seq.Seq) == 0 {
			continue
		}

		if read.Pos < evidence.POS-task.sampleStats.ReadLength*6 {
			continue
		}

		if read.Pos > evidence.POS+task.sampleStats.ReadLength*2 {
			continue
		}

		var plusFirstSoftclipped int
		if read.Cigar[0].Type().String() == "S" {
			plusFirstSoftclipped = read.Cigar[0].Len()
		}

		if plusFirstSoftclipped <= 4 {
			continue
		}

		PsizeSoftClipped := plusFirstSoftclipped
		if PsizeSoftClipped == 0 {
			PsizeSoftClipped = 10
		}

		var score []bWTScore
		// if PsizeSoftClipped > setMaxRangeSkipBP {
		// 	score = bwt.locateRead(read, PsizeSoftClipped-5)
		// } else {
		score = bwt.locateRead(read, PsizeSoftClipped)
		// }

		// if read.Pos != 53368191 {
		// 	continue
		// }
		// fmt.Println(read)

		for _, x := range score {
			// if x.HIT == task.sampleStats.ReadLength {
			// 	continue
			// }

			if x.HIT >= 4 {

			} else {
				continue
			}

			// fmt.Println(">>>>>>>>", endPosAlign-x.Pos, read.Pos-plusFirstSoftclipped+x.HIT+1, x)
			oneBP := bp[simpleBreakpoint{endPosAlign - x.Pos, read.Pos - plusFirstSoftclipped + x.HIT + 1}]

			oneBP.hit++
			oneBP.longhit = append(oneBP.longhit, x.HIT)
			if plusFirstSoftclipped != 0 {
				oneBP.softclippedhit = append(oneBP.softclippedhit, plusFirstSoftclipped)
			}
			oneBP.orderhit = append(oneBP.orderhit, orderhit)

			bp[simpleBreakpoint{endPosAlign - x.Pos, read.Pos - plusFirstSoftclipped + x.HIT + 1}] = oneBP
		}

	}

	var bks []breakPointState
	for i, x := range bp {
		if checkAddBreakpointState(&x, "DEL") {
			// getRelativeReads(evidence.Read, &x, "F", i.END, evidence.CHR, i.POS, "F", task)
			bks = append(bks, breakPointState{
				POS:            i.POS,
				END:            i.END,
				HIT:            x.hit,
				INSERTSIZE:     i.END - i.POS,
				ALLDEPTH:       x.alldepth,
				longhit:        x.longhit,
				softclippedhit: x.softclippedhit,
				orderhit:       x.orderhit,
				ReadPairCount:  evidence.LEN,
			})
		}
	}

	sort.Slice(bks, func(i, j int) bool { return (bks)[i].HIT > (bks)[j].HIT })
	var PTresults []printTrain

	for _, x := range bks {
		if x.HIT >= 2 {

		} else {
			continue
		}

		if x.END-x.POS < 50 {
			continue
		}

		if !isInLimitRange(x.POS, x.END) {
			continue
		}

		// if !checkBetween(int(evidence.InsertSize), x.END-x.POS, int((task.sampleStats.InsertSizeMean * 3)), int((task.sampleStats.InsertSizeMean * 3))) {
		// 	continue
		// }

		var MaxLongHit int
		for _, max := range x.longhit {
			if MaxLongHit < max {
				MaxLongHit = max
			}
		}

		// refPos := strings.ToUpper(gofasta.GetSequence(evidence.CHR, x.POS-MaxLongHit, x.POS, task.fastaReader))
		// refEnd := strings.ToUpper(gofasta.GetSequence(evidence.CHR, x.END-MaxLongHit-1, x.END-1, task.fastaReader))

		// if refPos == refEnd {
		// 	continue
		// }

		x.SVType = "DEL"
		x.Orientation = "L"
		x.MapQRP = evidence.MapQRP
		aPT := printPileUp(evidence, &x, &jbs, task)
		PTresults = append(PTresults, aPT)
		// break
	}
	return PTresults
}

func findDelBreakPointFirstIn(evidence *evidentCollector, jbs jobsSplitter, task *Task) []printTrain {
	var startPosAlign, endPosAlign int

	// Align
	endPosAlign = evidence.MATEPOS + task.sampleStats.ReadLength*2 + int(task.sampleStats.InsertSizeSD)
	startPosAlign = evidence.MATEPOS - task.sampleStats.ReadLength*7 - int(task.sampleStats.InsertSizeSD)
	if startPosAlign < evidence.POS {
		startPosAlign = evidence.POS
	}

	if startPosAlign < 0 {
		startPosAlign = 0
	}

	maxReferenceEND := task.getReferenceEND(evidence.CHR)

	if maxReferenceEND <= 0 {
		return nil
	}

	if endPosAlign > maxReferenceEND {
		endPosAlign = maxReferenceEND
	}

	seq, err := task.fastaReader.GetSequence(evidence.CHR, startPosAlign, endPosAlign)
	if err != nil {
		return nil
	}
	seq = strings.ToUpper(seq)
	bwt := newBWT()
	// if startPosAlign != 233671956 {
	// 	return
	// }
	fmt.Println(evidence.CHR, startPosAlign, endPosAlign)
	bwt.Transform(seq)
	bp := make(map[simpleBreakpoint]breakpointinfo)

	// var orderhit int
	for orderhit, read := range evidence.Read {
		if getNumberSoftClippedLast(read) <= 3 {
			continue
		}

		if read.Seq.Length == 0 {
			continue
		}

		var plusFirstSoftclipped int
		if read.Cigar[0].Type().String() == "S" {
			plusFirstSoftclipped = read.Cigar[0].Len()
		}

		sizeSoftClipped := 0
		if read.Cigar[len(read.Cigar)-1].Type().String() == "S" {
			sizeSoftClipped = read.Cigar[len(read.Cigar)-1].Len()
		}

		if sizeSoftClipped <= 4 {
			continue
		}

		var score []bWTScore
		if sizeSoftClipped > 16 {
			score = bwt.locateReverseRead(read, sizeSoftClipped-4)
		} else {
			score = bwt.locateReverseRead(read, sizeSoftClipped)
		}

		// if read.End() != 220440788 {
		// 	continue
		// }
		// fmt.Println(read)
		for _, x := range score {
			if x.HIT >= 4 {

			} else {
				continue
			}

			// fmt.Println(simpleBreakpoint{read.Seq.Length - x.HIT + read.Pos - plusFirstSoftclipped, startPosAlign + x.Pos + 1})

			oneBP := bp[simpleBreakpoint{read.Seq.Length - x.HIT + read.Pos - plusFirstSoftclipped, startPosAlign + x.Pos + 1}]

			oneBP.hit++
			oneBP.longhit = append(oneBP.longhit, x.HIT)
			if sizeSoftClipped != 0 {
				oneBP.softclippedhit = append(oneBP.softclippedhit, sizeSoftClipped)
			}

			oneBP.orderhit = append(oneBP.orderhit, orderhit)

			bp[simpleBreakpoint{read.Seq.Length - x.HIT + read.Pos - plusFirstSoftclipped, startPosAlign + x.Pos + 1}] = oneBP
		}
	}

	var bks []breakPointState
	for i, x := range bp {
		if checkAddBreakpointState(&x, "DEL") {

			// getRelativeReads(evidence.Read, &x, "L", i.POS, evidence.CHR, i.END, "L", task)
			bks = append(bks, breakPointState{
				POS:            i.POS,
				END:            i.END,
				HIT:            x.hit,
				INSERTSIZE:     i.END - i.POS,
				ALLDEPTH:       x.alldepth,
				longhit:        x.longhit,
				softclippedhit: x.softclippedhit,
				orderhit:       x.orderhit,
				ReadPairCount:  evidence.LEN,
			})
		}
	}

	sort.Slice(bks, func(i, j int) bool { return (bks)[i].HIT > (bks)[j].HIT })

	var PTresults []printTrain
	for _, x := range bks {
		if x.HIT >= 2 {

		} else {
			continue
		}

		if x.END-x.POS < 50 {
			continue
		}

		if !isInLimitRange(x.POS, x.END) {
			continue
		}
		// startCompare := int(evidence.InsertSize) - int(task.sampleStats.InsertSizeMean)
		// endCompare := int(evidence.InsertSize) + int(task.sampleStats.InsertSizeMean)
		// if !checkBetween(int(evidence.InsertSize), x.END-x.POS, startCompare, endCompare) {
		// 	continue
		// }

		var MaxLongHit int
		for _, max := range x.longhit {
			if MaxLongHit < max {
				MaxLongHit = max
			}
		}
		// refPos := strings.ToUpper(gofasta.GetSequence(evidence.CHR, x.POS, x.POS+MaxLongHit, task.fastaReader))
		// refEnd := strings.ToUpper(gofasta.GetSequence(evidence.CHR, x.END-1, x.END+MaxLongHit-1, task.fastaReader))

		// if refPos == refEnd {
		// 	continue
		// }

		x.SVType = "DEL"
		x.Orientation = "F"
		x.MapQRP = evidence.MapQRP
		// _, rUncapture := getReadPileupPoint(evidence.Read, x.orderhit, x.POS)
		// pos, _ := getMostEndHit(rCapture)
		// var longsc int
		// for _, read := range rUncapture {
		// 	scl := getNumberSoftClippedLast(read)
		// 	if scl >= 3 {
		// 		fmt.Println(read)
		// 		if scl > getLonggestHit(x) {
		// 			fmt.Println(scl)
		// 		}
		// 	}
		// }

		aPT := printPileUp(evidence, &x, &jbs, task)
		// fmt.Println(aPT.getString())
		PTresults = append(PTresults, aPT)

	}

	return PTresults
}

type printTrain struct {
	SVType                 string
	Orientation            string
	Chr                    string
	Pos                    int
	ChrEnd                 string
	End                    int
	Hit                    int
	LongMaxHit             int
	LongMinHit             int
	LongAvgHit             int
	MaxScHit               int
	MinScHit               int
	AvgScHit               int
	SumScOppSide           int
	ScMaxOppSide           int
	ScMinOppSide           int
	ScAvgOppSide           int
	SumSCOverPointOppSide  int
	SumScSameSide          int
	ScMaxSameSide          int
	ScMinSameSide          int
	ScAvgSameSide          int
	SumSCOverPointSameSide int
	ReadDepth              int
	ReadDepthNextPos       int
	ReadDepthPreviousPos   int
	CaptureMapQMax         int
	CaptureMapQMin         int
	CaptureMapQAvg         int
	UncaptureMapQMax       int
	UncaptureMapQMin       int
	UncaptureMapQAvg       int
	UncaptureSCMapQMax     int
	UncaptureSCMapQMin     int
	UncaptureSCMapQAvg     int
	ReadPairCount          int
	MatchOrientation       string
	MaxMapQRP              int
	AvgMapQRP              int
	MinMapQRP              int
	MLTrainPositive        bool
	Skip                   bool
}

type doublePrintTrain struct {
	A *printTrain
	B *printTrain
}

func printTrainData(pt printTrain, filename string) {

	f, err := os.OpenFile(filename+".txt", os.O_APPEND|os.O_WRONLY, 0600)
	defer f.Close()
	if err != nil {
		panic(err)
	}
	w := bufio.NewWriter(f)

	fmt.Fprintln(w, ptsToTSV(&pt))

	w.Flush()
}

func foundOrderRead(number int, ignoreread []int) bool {
	var found bool
	for ig := 0; ig < len(ignoreread); ig++ {
		if ignoreread[ig] == number {
			found = true
			break
		}
	}
	return found
}

func printTrainDataList(pt []printTrain, filename string, muResult *sync.Mutex) {
	muResult.Lock()
	defer muResult.Unlock()
	f, err := os.OpenFile(filename+".txt", os.O_APPEND|os.O_WRONLY, 0600)
	defer f.Close()
	if err != nil {
		panic(err)
	}
	w := bufio.NewWriter(f)

	for _, x := range pt {
		fmt.Println(ptsToStringMarshal(&x))
		fmt.Fprintln(w, ptsToTSV(&x))
		w.Flush()
	}

}

func getMostEndHit(reads []*sam.Record) (int, int) {
	bp := make(map[int]int)
	for _, read := range reads {
		bp[read.End()]++
	}

	var temppos, temphit int
	for pos, hit := range bp {
		if hit > temphit {
			temphit = hit
			temppos = pos
		}
	}

	return temppos, temphit
}

func getMostPosHit(reads []*sam.Record) (int, int) {
	bp := make(map[int]int)
	for _, read := range reads {
		bp[read.Pos]++
	}

	var temppos, temphit int
	for pos, hit := range bp {
		if hit > temphit {
			temphit = hit
			temppos = pos
		}
	}

	return temppos, temphit
}
