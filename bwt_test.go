package bolt

import "testing"

func TestSEQREVERSECOMPLEMENT(t *testing.T) {
	tables := []struct {
		seq    string
		result string
	}{
		{"AATAAATATTCGCCCTACAGGAAAAGTAAACACCAAGGAAATGTTTCAACATTCCTAGGTAGTAA", "TTACTACCTAGGAATGTTGAAACATTTCCTTGGTGTTTACTTTTCCTGTAGGGCGAATATTTATT"},
		{"TCAAAGTTTCCAGCACTTTGAGATCCTTGTCTTATAAAATACTACAGCAGCTATAGTTTAAAAAT", "ATTTTTAAACTATAGCTGCTGTAGTATTTTATAAGACAAGGATCTCAAAGTGCTGGAAACTTTGA"},
	}

	for _, table := range tables {
		total := seqReverseComplement(table.seq)
		if total != table.result {
			t.Errorf("reverse complement was incorrect, got: %s, want: %s.", table.seq, table.result)
		}
	}
}
