package bolt

import (
	"runtime"
	"runtime/debug"
)

func startClassify(svnametype, pathlist string, task *Task) []string {
	hq := true
	switch svnametype {
	case "dup":
		// return convertTrainTypeDup(svnametype, pathlist, task, hq)
	case "inv":
		// return convertTrainTypeInv(svnametype, pathlist, task, hq)
	case "del":
		return convertTrainTypeDel(svnametype, pathlist, task, hq)
	case "ins":
		// return convertTrainTypeIns(svnametype, pathlist, task, hq)
	default:
		// return convertTrainTypeAll(svnametype, pathlist, task, hq)
	}
	freemem()

	return nil
}

func getTrainDataFromLists(variants *[]string) []*printTrain {
	var pts []*printTrain

	for _, list := range *variants {
		pt := tsvtoPrintTrain(&list)
		pts = append(pts, &pt)
	}

	return pts
}

func filterHighConfidenct(pt []*printTrain, svtype string) ([]*printTrain, []*printTrain) {

	var hc, nohc []*printTrain
	for _, x := range pt {
		if svtype == "TRA" {
			if x.LongMaxHit < 10 {
				continue
			}

			if x.MaxMapQRP == 0 {
				continue
			}

			if x.CaptureMapQMax == 0 {
				continue
			}

			if x.CaptureMapQMin == 0 {
				continue
			}

			if x.CaptureMapQAvg < 20 {
				continue
			}

			// if x.SumScSameSide >= 14 || x.SumScOppSide >= 14{}

			hc = append(hc, x)
			continue
		}

		if svtype == "DUP" {
			if x.SumScSameSide >= 7 && x.ScMaxSameSide > 50 {
				continue
			}

			if x.CaptureMapQMin < 5 {
				continue
			}
		}

		pass := true
		if x.Hit <= 2 {
			pass = false
		}

		if x.UncaptureMapQMin == 0 {
			pass = false
		}

		if x.SumScSameSide >= 11 {
			pass = false
		}

		if x.MinMapQRP == 0 {
			pass = false
		}

		if x.CaptureMapQMin == 0 {
			pass = false
		}

		if x.ReadDepthNextPos < 3 {
			pass = false
		}

		if x.ReadDepthPreviousPos < 3 {
			pass = false
		}

		if x.SumScOppSide > 2 && x.UncaptureMapQMax > 30 {
			pass = false
		}

		if x.CaptureMapQMax == 0 {
			pass = false
		}

		if x.LongMaxHit <= 10 {
			pass = false
		}

		if x.ReadDepthNextPos <= 3 || x.ReadDepthPreviousPos <= 3 {
			pass = false
		}

		if pass {
			hc = append(hc, x)
		} else {
			nohc = append(nohc, x)
		}

	}

	return hc, nohc
}

func filterHighConfidenctDEL(pt []*printTrain) ([]*printTrain, []*printTrain) {
	var hc, nohc []*printTrain
	for _, x := range pt {
		pass := true
		// if x.Hit <= 4 {
		// 	pass = false
		// }

		// if x.End-x.Pos < 100 {
		// 	pass = false
		// }

		if x.LongMaxHit < 10 {
			pass = false
		}

		if x.CaptureMapQAvg < 10 {
			pass = false
		}

		if x.End-x.Pos > 150000 {
			pass = false
		}

		if pass {
			hc = append(hc, x)
		} else {
			nohc = append(nohc, x)
		}

	}

	return hc, nohc
}

func filterHighConfidenctINV(pt []*printTrain) ([]*printTrain, []*printTrain) {
	var hc, nohc []*printTrain
	for _, x := range pt {
		pass := true
		if x.Hit <= 2 {
			pass = false
		}

		if x.UncaptureMapQMin == 0 {
			pass = false
		}

		// if x.SumScSameSide >= 11 {
		// 	pass = false
		// }

		// if x.MaxMapQRPint == 0 {
		// 	pass = false
		// }

		if x.CaptureMapQMin == 0 {
			pass = false
		}

		// if x.ReadDepthNextPos < 3 {
		// 	pass = false
		// }

		// if x.ReadDepthPreviousPos < 3 {
		// 	pass = false
		// }

		// if x.SumScOppSide > 2 && x.UncaptureMapQMax > 30 {
		// 	pass = false
		// }

		if x.CaptureMapQMax == 0 {
			pass = false
		}

		if x.LongMaxHit <= 20 {
			pass = false
		}

		// if x.ReadDepthNextPos <= 3 || x.ReadDepthPreviousPos <= 3 {
		// 	pass = false
		// }

		if pass {
			hc = append(hc, x)
		} else {
			nohc = append(nohc, x)
		}

	}

	return hc, nohc
}

func removeSmallSize(pts []printTrain) []printTrain {
	var tempPT []printTrain
	for i := 0; i < len(pts); i++ {
		if pts[i].Chr == pts[i].ChrEnd {
			if pts[i].End-pts[i].Pos >= 50 {
				tempPT = append(tempPT, pts[i])
			}
		}
	}
	return tempPT
}

func removeDupAdvance(pts []printTrain) []printTrain {
	var tempPT []printTrain
	for i := 0; i < len(pts); i++ {
		// var found bool
		var foundCount int
		for j := 0; j < len(pts); j++ {

			if pts[i].Chr != pts[j].Chr {
				continue
			}

			if checkBetween(pts[i].Pos, pts[j].Pos, 100, 100) {
				foundCount++
			}

			if checkBetween(pts[i].End, pts[j].End, 100, 100) {
				foundCount++
			}
		}
		if foundCount < 3 {
			tempPT = append(tempPT, pts[i])
		}
	}
	return tempPT
}

func splitEvenOddDataPT(pts []*printTrain, svtype string) (*[]doublePrintTrain, []*printTrain) {
	var even []doublePrintTrain
	var odd []*printTrain

	for nX, x := range pts {
		if x.Skip {
			continue
		}

		var added bool
		var evenadded bool
		for i := nX + 1; i < len(pts); i++ {
			var maxCover = 10
			if svtype != "INS" {
				if x.End-x.Pos < 200 {
					maxCover = 5
				} else {
					maxCover = 10
				}
			} else if svtype != "TRA" {
				if x.End-x.Pos < 200 {
					maxCover = 5
				} else {
					maxCover = 10
				}
			}

			if x.Pos+maxCover < (pts)[i].Pos {
				break
			}

			if (pts)[i].Skip {
				continue
			}

			if (pts)[i].Chr != x.Chr {
				break
			}

			if checkBetween(x.Pos, (pts)[i].Pos, maxCover, maxCover) && checkBetween(x.End, (pts)[i].End, maxCover, maxCover) {
				if (pts)[i].Orientation == x.Orientation {
					(pts)[i].Skip = true
					continue
				}

				if !evenadded {
					even = append(even, doublePrintTrain{x, (pts)[i]})
					evenadded = true
				}
				(pts)[i].Skip = true
				added = true
				continue
			}
		}

		if !added {
			odd = append(odd, x)
		}
	}

	return &even, odd
}

func convertTrainTypeDup(name string, pathlist string, task *Task, hq bool) []string {
	var tempResult []string
	// variantLists := getListVariants(pathlist)

	pts := getTrainDataFromFiles(pathlist, "DUP")
	countList = countList + len(pts)
	// return nil
	// var ptso []bucketPrintTrain
	// even, odd := splitBucketEvenOddDataPT(&ptso)

	// pts = cleanDupData(&pts)
	// return nil

	// odd := cleanDupData(&pts)
	// var even []printTrain
	even, odd := splitEvenOddDataPT(pts, "DUP")
	// odd = cleanDupData(&pts)
	// return nil
	hc, odd := filterHighConfidenct(odd, "DUP")
	var oddHCString []string
	for _, x := range hc {
		oddHCString = append(oddHCString, ptsToTSV(x))
	}

	//	with NN
	var oddString []string
	for _, x := range odd {
		oddString = append(oddString, ptsToTSV(x))
	}

	yesVariantodd, _ := filterWithNN(&oddString, task, "DUP")
	odd = getTrainDataFromLists(&yesVariantodd)
	// odd = removeDUPSinglePos(odd)

	var tempResultOdd []string
	for _, x := range odd {
		// if x.MaxMapQRP < 0 {
		// 	continue
		// }

		// if x.MinMapQRP == 0 {
		// 	continue
		// }

		// if x.AvgMapQRP < 10 {
		// 	continue
		// }

		// if x.Hit <= 2 {
		// 	continue
		// }

		if x.End-x.Pos < 50 {
			continue
		}

		if x.End-x.Pos > 100000 {
			continue
		}

		// if x.LongMaxHit < 30 {
		// 	continue
		// }

		// if x.ReadDepthNextPos < 4 {
		// 	continue
		// }

		// if x.ReadDepthPreviousPos < 4 {
		// 	continue
		// }

		// if x.SumScSameSide >= 14 && x.UncaptureSCMapQAvg >= 40 {
		// 	continue
		// }
		// tempResultOdd = append(tempResultOdd, ptsToTSV(x))
	}
	oddPTResult := getTrainDataFromLists(&tempResultOdd)
	oddPTResult = removeDUPbyPos(oddPTResult)
	// oddPTResult = removeDUPSinglePos(oddPTResult)
	var LastSSOdd []string
	for _, x := range oddPTResult {
		LastSSOdd = append(LastSSOdd, ptsToTSV(x))
	}

	//Even
	// var neweven []doublePrintTrain
	// for _, x := range *even {
	// 	if x.A.AvgMapQRP < 5 && x.B.AvgMapQRP < 5 {
	// 		break
	// 	} else {
	// 		neweven = append(neweven, x)
	// 	}
	// }

	// *even = neweven

	//Even
	// even = removeSmallSize(even)
	var tempResultEven []string
	for _, x := range *even {
		if x.A.CaptureMapQMax == 0 {
			continue
		}

		// if x.A.CaptureMapQMax

		if x.A.End-x.A.Pos < 50 {
			continue
		}

		if x.A.End-x.A.Pos > 100000 {
			continue
		}

		if x.A.SumScSameSide > 8 {
			continue
		}

		tempResultEven = append(tempResultEven, ptsToTSV(x.A))

	}

	evenPTResult := getTrainDataFromLists(&tempResultEven)
	evenPTResult = removeDUPbyPos(evenPTResult)
	// evenPTResult = removeDUPSinglePos(evenPTResult)
	var LastSSEven []string
	for _, x := range evenPTResult {
		LastSSEven = append(LastSSEven, ptsToTSV(x))
	}

	//Append result
	tempResult = append(tempResult, LastSSOdd...)
	tempResult = append(tempResult, LastSSEven...)
	tempResult = append(tempResult, oddHCString...)

	return tempResult

}

func removeSkipPTS(pts []*printTrain) {
	for _, x := range pts {
		x.Skip = false
	}
}

func removeDuplicationPosition(pts []*printTrain) []*printTrain {
	removeSkipPTS(pts)
	var ptsTemp []*printTrain
	for nx, x := range pts {
		if x.Skip {
			continue
		}

		// var add bool
		for j := nx + 1; j < len(ptsTemp); j++ {
			if x.Pos+30 < ptsTemp[j].Pos {
				break
			}

			if checkBetween(x.Pos, ptsTemp[j].Pos, 30, 30) && checkBetween(x.End, ptsTemp[j].End, 30, 30) {
				// found = true
				// break
				ptsTemp[j].Skip = true
			}
		}

		// if !add {
		ptsTemp = append(ptsTemp, x)
		x.Skip = true
		// }
	}
	// sizeptsI := len(pts)

	// for i := 0; i < sizeptsI; i++ {
	// 	var found bool
	// 	for j := 0; j < len(ptsTemp); j++ {

	// 		if (pts)[i].Chr != ptsTemp[j].Chr {
	// 			continue
	// 		}

	// 		if checkBetween((pts)[i].Pos, ptsTemp[j].Pos, 100, 100) && checkBetween((pts)[i].End, ptsTemp[j].End, 100, 100) {
	// 			found = true
	// 			break
	// 		}
	// 	}

	// 	if !found {
	// 		ptsTemp = append(ptsTemp, (pts)[i])
	// 	}
	// }

	return ptsTemp
}

func removeDUPbyPos(pts []*printTrain) []*printTrain {
	var ptsTemp []*printTrain
	sizeptsI := len(pts)
	for i := 0; i < sizeptsI; i++ {
		var found bool
		for j := 0; j < len(ptsTemp); j++ {

			if (pts)[i].Chr != ptsTemp[j].Chr {
				continue
			}

			if checkBetween((pts)[i].Pos, ptsTemp[j].Pos, 100, 100) && checkBetween((pts)[i].End, ptsTemp[j].End, 100, 100) {
				found = true
				break
			}
		}

		if !found {
			ptsTemp = append(ptsTemp, (pts)[i])
		}
	}

	return ptsTemp
}

func removeDUPbyPosOren(pts []printTrain) []printTrain {
	var ptsTemp []printTrain
	sizeptsI := len(pts)
	for i := 0; i < sizeptsI; i++ {
		var found bool
		for j := 0; j < len(ptsTemp); j++ {
			if pts[i].Orientation == ptsTemp[j].Orientation {
				continue
			}

			if pts[i].Chr != ptsTemp[j].Chr {
				continue
			}

			if checkBetween(pts[i].Pos, ptsTemp[j].Pos, 100, 100) && checkBetween(pts[i].End, ptsTemp[j].End, 100, 100) {
				found = true
				break
			}
		}

		if !found {
			ptsTemp = append(ptsTemp, pts[i])
		}
	}
	return ptsTemp
}

func convertTrainTypeDel(name string, pathlist string, task *Task, hq bool) []string {
	var tempResult []string

	// variantLists := getListVariants(pathlist)
	pts := getTrainDataFromFiles(pathlist, "DEL")
	countList = countList + len(pts)
	// var ptso []bucketPrintTrain
	even, odd := splitEvenOddDataPT(pts, "DEL")
	pts = nil

	// hc, odd := filterHighConfidenctDEL(odd)
	// hc = removeDuplicationPosition(hc)
	// var LastSSOddHC []string
	// for _, x := range hc {
	// 	LastSSOddHC = append(LastSSOddHC, ptsToTSV(x))
	// }

	var oddString []string
	for _, x := range odd {
		oddString = append(oddString, ptsToTSV(x))
	}

	// yesVariantodd, _ := filterWithNN(&oddString, task, "DEL")
	// odd = getTrainDataFromLists(&yesVariantodd)

	var tempResultOdd []string
	for _, x := range odd {

		// if x.CaptureMapQMax == 0 {
		// 	continue
		// }

		// if x.MaxMapQRP==0

		// if x.CaptureMapQAvg <= 10 {
		// 	continue
		// }

		// if x.Pos == 8347930 {
		// 	fmt.Println(x)
		// }

		// if x.SumScOppSide > 10 {
		// 	continue
		// }

		// if x.LongMaxHit < 10 {
		// 	continue
		// }

		// if x.Hit <= 2 {
		// 	continue
		// }

		// if x.End-x.Pos < 50 {
		// 	continue
		// }

		// if x.End-x.Pos > 300000 {
		// 	continue
		// }

		// if x.ReadDepth > 300 {
		// 	continue
		// }

		tempResultOdd = append(tempResultOdd, ptsToTSV(x))
	}

	oddPTResult := getTrainDataFromLists(&tempResultOdd)
	oddPTResult = removeDUPbyPos(oddPTResult)
	// oddPTResult = removeDUPSinglePos(oddPTResult)
	var LastSSOdd []string
	for _, x := range oddPTResult {
		LastSSOdd = append(LastSSOdd, ptsToTSV(x))
	}

	//Even
	// var neweven []doublePrintTrain
	// for _, x := range *even {
	// 	if x.A.CaptureMapQMax == 0 || x.B.CaptureMapQMax == 0 {
	// 		continue
	// 	} else {
	// 		neweven = append(neweven, x)
	// 	}
	// }
	// *even = neweven

	var tempResultEven []string
	for _, x := range *even {
		// if x.A.CaptureMapQMax == 0 || x.B.CaptureMapQMax == 0 {
		// 	continue
		// }

		// if x.A.LongMaxHit < 10 || x.B.LongMaxHit < 10 {
		// 	continue
		// }

		// if x.A.SumSCOverPointSameSide > 10 && x.B.SumSCOverPointSameSide > 10 {
		// 	continue
		// }

		// if x.A.End-x.A.Pos < 500 {
		// 	if x.A.LongMaxHit < 20 || x.B.LongMaxHit < 20 {
		// 		continue
		// 	}
		// }

		// if x.A.ReadDepth > 500 || x.B.ReadDepth > 500 {
		// 	continue
		// }

		// if x.A.End-x.A.Pos < 50 {
		// 	continue
		// }

		// if x.A.End-x.A.Pos > 300000 {
		// 	continue
		// }

		tempResultEven = append(tempResultEven, ptsToTSV(x.A))
	}

	evenPTResult := getTrainDataFromLists(&tempResultEven)
	// evenPTResult = removeDUPbyPos(evenPTResult)
	evenPTResult = removeDuplicationPosition(evenPTResult)

	var LastSSEven []string
	for _, x := range evenPTResult {
		LastSSEven = append(LastSSEven, ptsToTSV(x))
	}

	//Append result
	tempResult = append(tempResult, LastSSOdd...)
	tempResult = append(tempResult, LastSSEven...)
	// tempResult = append(tempResult, LastSSOddHC...)
	return tempResult

	// PTResult := getTrainDataFromLists(&tempResult)
	// PTResult = removeDuplicationPosition(PTResult)

	// var Final []string
	// for _, x := range PTResult {
	// 	Final = append(Final, ptsToTSV(x))
	// }
	// fmt.Println(len(tempResult))
	// return Final
}

func convertTrainTypeInv(name string, pathlist string, task *Task, hq bool) []string {
	var tempResult []string

	pts := getTrainDataFromFiles(pathlist, "INV")
	countList = countList + len(pts)
	// return nil
	// var ptso []bucketPrintTrain
	// even, odd := splitBucketEvenOddDataPT(&ptso)
	even, odd := splitEvenOddDataPT(pts, "INV")
	// fmt.Println(len(even), len(odd))

	// even, odd := splitBucketEvenOddDataPT(pts)
	// hc, odd := filterHighConfidenctINV(odd)
	// var oddHCString []string
	// for _, x := range hc {
	// 	oddHCString = append(oddHCString, ptsToTSV(x))
	// }

	var oddString []string
	for _, x := range odd {
		oddString = append(oddString, ptsToTSV(x))
	}

	yesVariantodd, _ := filterWithNN(&oddString, task, "INV")
	odd = getTrainDataFromLists(&yesVariantodd)

	var tempResultOdd []string
	for _, x := range odd {
		// if x.CaptureMapQMax == 0 {
		// 	continue
		// }

		// if x.MinMapQRP == 0 {
		// 	continue
		// }

		// if x.SumScOppSide > 20 {
		// 	continue
		// }

		// if x.Hit <= 2 {
		// 	continue
		// }

		if x.End-x.Pos < 50 {
			continue
		}

		if x.End-x.Pos > 100000 {
			continue
		}

		tempResultOdd = append(tempResultOdd, ptsToTSV(x))
	}

	oddPTResult := getTrainDataFromLists(&tempResultOdd)
	oddPTResult = removeDUPbyPos(oddPTResult)
	// oddPTResult = removeDUPSinglePos(oddPTResult)
	var LastSSOdd []string
	for _, x := range oddPTResult {
		LastSSOdd = append(LastSSOdd, ptsToTSV(x))
	}

	//Even
	// var neweven []doublePrintTrain
	// for _, x := range *even {
	// 	if x.A.CaptureMapQMax == 0 || x.B.CaptureMapQMax == 0 {
	// 		break
	// 	} else {
	// 		neweven = append(neweven, x)
	// 	}
	// }

	// *even = neweven

	// even = removeSmallSize(even)
	var tempResultEven []string
	for _, x := range *even {

		if x.A.CaptureMapQMax < 10 && x.B.CaptureMapQMax < 10 {
			continue
		}

		// if x.A.CaptureMapQAvg < 5 && x.B.CaptureMapQAvg < 5 {
		// 	continue
		// }

		// if x.A.ReadDepthNextPos == 0 {
		// 	continue
		// }
		// if x.B.CaptureMapQMax == 0 {
		// 	continue
		// }

		// if x.A.MaxMapQRP == 0 && x.B.MaxMapQRP == 0 {
		// 	continue
		// }

		if x.A.End-x.A.Pos < 50 {
			continue
		}

		if x.A.End-x.A.Pos > 100000 {
			continue
		}

		tempResultEven = append(tempResultEven, ptsToTSV(x.A))

	}

	evenPTResult := getTrainDataFromLists(&tempResultEven)
	evenPTResult = removeDUPbyPos(evenPTResult)
	// evenPTResult = (evenPTResult)

	var LastSSEven []string
	for _, x := range evenPTResult {
		LastSSEven = append(LastSSEven, ptsToTSV(x))
	}

	//Append result
	tempResult = append(tempResult, LastSSOdd...)
	tempResult = append(tempResult, LastSSEven...)
	// tempResult = append(tempResult, oddHCString...)

	return tempResult
}

func convertTrainTypeIns(name string, pathlist string, task *Task, hq bool) []string {
	// if hq {
	var tempResult []string

	// variantLists := getListVariants(pathlist)

	pts := getTrainDataFromFiles(pathlist, "INS")
	countList = countList + len(pts)
	even, odd := splitEvenOddDataPT(pts, "INS")

	var oddString []string
	for _, x := range odd {
		oddString = append(oddString, ptsToTSV(x))
	}

	yesVariantodd, _ := filterWithNN(&oddString, task, "INS")
	odd = getTrainDataFromLists(&yesVariantodd)

	var tempResultOdd []string
	for _, x := range odd {

		if x.MinMapQRP == 0 {
			continue
		}

		if x.MaxMapQRP == 0 {
			continue
		}

		tempResultOdd = append(tempResultOdd, ptsToTSV(x))

	}

	oddPTResult := getTrainDataFromLists(&tempResultOdd)
	oddPTResult = removeDUPbyPos(oddPTResult)
	// oddPTResult = removeDUPSinglePos(oddPTResult)
	var LastSSOdd []string
	for _, x := range oddPTResult {
		LastSSOdd = append(LastSSOdd, ptsToTSV(x))
	}

	// even = removeSmallSize(even)
	var tempResultEven []string
	for _, x := range *even {
		// if x.ReadPairCount < 3 {
		// 	continue
		// }
		// if x.CaptureMapQMax == 0 {
		// 	continue
		// }

		tempResultEven = append(tempResultEven, ptsToTSV(x.A))
	}

	evenPTResult := getTrainDataFromLists(&tempResultEven)
	evenPTResult = removeDUPbyPos(evenPTResult)

	var LastSSEven []string
	for _, x := range evenPTResult {
		LastSSEven = append(LastSSEven, ptsToTSV(x))
	}

	//Append result
	tempResult = append(tempResult, LastSSOdd...)
	tempResult = append(tempResult, LastSSEven...)

	return tempResult

}

func convertTrainTypeAll(name string, pathlist string, task *Task, hq bool) []string {
	var tempResult []string

	// variantLists := getListVariants(pathlist)
	pts := getTrainDataFromFiles(pathlist, "TRA")
	countList = countList + len(pts)
	even, odd := splitEvenOddDataPT(pts, "TRA")

	// return nil
	// var ptso []bucketPrintTrain
	// even, odd := splitBucketEvenOddDataPT(&ptso)

	// even, odd := splitBucketEvenOddDataPT(pts)

	hc, odd := filterHighConfidenct(odd, "TRA")
	var oddHCString []string
	for _, x := range hc {
		oddHCString = append(oddHCString, ptsToTSV(x))
	}

	var oddString []string
	for _, x := range odd {
		oddString = append(oddString, ptsToTSV(x))
	}

	yesVariantodd, _ := filterWithNN(&oddString, task, "TRA")
	odd = getTrainDataFromLists(&yesVariantodd)

	var tempResultOdd []string
	for _, x := range odd {

		if x.LongMaxHit < 20 {
			continue
		}

		tempResultOdd = append(tempResultOdd, ptsToTSV(x))

	}

	oddPTResult := getTrainDataFromLists(&tempResultOdd)
	oddPTResult = removeDUPbyPos(oddPTResult)
	// oddPTResult = removeDUPSinglePos(oddPTResult)
	var LastSSOdd []string
	for _, x := range oddPTResult {
		LastSSOdd = append(LastSSOdd, ptsToTSV(x))
	}

	//Even
	var neweven []doublePrintTrain
	for _, x := range *even {
		// for numbery, y := range even {
		// if numberx == numbery {
		// 	continue
		// }
		// if checkBetween(x.Pos, y.Pos, 5, 5) && checkBetween(x.End, y.End, 5, 5) {
		if x.A.CaptureMapQMax == 0 || x.B.CaptureMapQMax == 0 {
			break
			// 		// } else if x.CaptureMapQAvg <= 15 || y.CaptureMapQAvg <= 15 {
			// 		// 	break
			// 		// } else if x.SumScOppSide > 15 || y.SumScOppSide > 15 {
			// 		// 	break
			// 		// } else if x.SumScOppSide > 10 || y.SumScOppSide > 10 {
			// 		// 	break
		} else {
			neweven = append(neweven, x)
		}
		// }
		// }
	}

	even = &neweven

	var tempResultEven []string
	for _, x := range *even {
		if x.A.Hit > 10 && x.B.Hit > 10 {
			continue
		}
		tempResultEven = append(tempResultEven, ptsToTSV(x.A))
	}

	evenPTResult := getTrainDataFromLists(&tempResultEven)
	evenPTResult = removeDUPbyPos(evenPTResult)

	var LastSSEven []string
	for _, x := range evenPTResult {
		LastSSEven = append(LastSSEven, ptsToTSV(x))
	}

	//Append result
	tempResult = append(tempResult, LastSSOdd...)
	tempResult = append(tempResult, LastSSEven...)
	tempResult = append(tempResult, oddHCString...)

	// fmt.Println(len(tempResult))
	return tempResult
}

func freemem() {
	runtime.GC()
	debug.FreeOSMemory()
}
