package fastalib

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

func (task *FastaReader) GetSequence(name string, start, end int) (string, error) {
	var BufferOffsetForward int64

	BufferOffsetForward, err := (task).getOffset(name)
	if err != nil {
		panic(err)
	}

	FastaLengthRow, err := (task).getLinebases(name)
	if err != nil {
		panic(err)
	}
	BufferOffsetForward = BufferOffsetForward + int64((start/FastaLengthRow)*(FastaLengthRow+1))
	var approx int
	approx = (start / FastaLengthRow) * FastaLengthRow //984100
	row := int(math.Ceil((float64(end) - float64(approx)) / float64(FastaLengthRow)))

	if row == 0 {
		row = 1
	}
	seq := (task).getSeqOffsetNback(name, row, BufferOffsetForward)
	// return seq[start-approx : end-approx], errors.New("read lenght problem")
	return seq[start-approx : end-approx], nil
}

func (fastareader *FastaReader) getSeqOffsetNback(name string, row int, offset int64) string {
	var buffer bytes.Buffer
	fileREF := fastareader.FastaFile
	fileRead, err := os.Open(fileREF)
	if err != nil {
		panic(err)
	}

	defer fileRead.Close()

	_, err = fileRead.Seek(offset, 0)
	if err != nil {
		panic(err)
	}

	var CurrentRow int
	scanner := bufio.NewScanner(fileRead)
	for scanner.Scan() {
		CurrentRow++
		buffer.WriteString(scanner.Text())
		if CurrentRow >= row {
			break
		}
	}
	return buffer.String()
}

func New(faFile string) (*FastaReader, error) {
	return &FastaReader{FastaFile: faFile}, nil
}

type FastaReader struct {
	FastaFile string
	faIndex   []faIndex
}

type faIndex struct {
	Name      string
	Length    int64
	Offset    int64
	Linebases int64
	Linewidth int64
}

func (FastaReader *FastaReader) AutoInitfaIndex() error {
	err := FastaReader.InitfaIndex(FastaReader.FastaFile + ".fai")
	if err != nil {
		FastaReader.GenaratefaIndex()
		return errors.New("auto fasta index error")
	}

	return nil
}

func (FastaReader *FastaReader) InitfaIndex(file string) error {
	fileRead, err := os.Open(file)
	if err != nil {
		return err
	}
	defer fileRead.Close()

	scanner := bufio.NewScanner(fileRead)

	var faindex []faIndex

	for scanner.Scan() {
		if len(strings.Fields(scanner.Text())) == 5 {
			s := strings.Split(scanner.Text(), "\t")
			Length, err := stringToInt64(s[1])
			if err != nil {
				return err
			}
			Offset, err := stringToInt64(s[2])
			if err != nil {
				return err
			}
			Linebases, err := stringToInt64(s[3])
			if err != nil {
				return err
			}
			Linewidth, err := stringToInt64(s[4])
			if err != nil {
				return err
			}

			temp := faIndex{s[0], Length, Offset, Linebases, Linewidth}

			faindex = append(faindex, temp)

		} else {
			return fmt.Errorf("File index has invalid")
		}
	}

	FastaReader.faIndex = faindex

	return nil
}

func (FastaReader *FastaReader) GenaratefaIndex() error {
	fileRead, err := os.Open(FastaReader.FastaFile)
	if err != nil {
		return err
	}
	// make sure it gets closed
	defer fileRead.Close()

	scanner := bufio.NewScanner(fileRead)
	var faindex []faIndex
	var LENGTH, OFFSET int64
	var LINEBASES int64
	var LINEWIDTH int64
	firstrun := true
	firstsave := true
	var FirstName string
	var FirstOffset int64
	var NextOffset int64
	var NextName string
	// var FinalName string
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), ">") {
			if firstrun {
				FirstName = scanner.Text()[1:]
				FirstOffset = int64(len(scanner.Text())) + 1
				firstrun = false
				OFFSET = FirstOffset
			} else {
				if firstsave {
					temp := faIndex{FirstName, LENGTH, FirstOffset, LINEBASES, LINEWIDTH}
					faindex = append(faindex, temp)
					firstsave = false
					OFFSET = OFFSET + int64(len(scanner.Text())) + 1
					NextOffset = OFFSET
					NextName = scanner.Text()[1:]
					LENGTH = 0
					LINEBASES = 0
				} else {
					temp := faIndex{NextName, LENGTH, NextOffset, LINEBASES, LINEWIDTH}
					faindex = append(faindex, temp)
					OFFSET = OFFSET + int64(len(scanner.Text())) + 1
					NextOffset = OFFSET
					NextName = scanner.Text()[1:]
					LENGTH = 0
					LINEBASES = 0
				}
			}
		} else {
			if LINEBASES == 0 {
				LINEBASES = int64(len(scanner.Text()))
				LINEWIDTH = LINEBASES + 1
			}
			LENGTH = LENGTH + int64(len(scanner.Text()))
			OFFSET = OFFSET + int64(len(scanner.Text())) + 1
		}
	}
	temp := faIndex{NextName, LENGTH, NextOffset, LINEBASES, LINEWIDTH}
	faindex = append(faindex, temp)

	FastaReader.faIndex = faindex
	FastaReader.writefaIndex()

	return nil
}

func (FastaReader *FastaReader) writefaIndex() {
	file, err := os.Create(FastaReader.FastaFile + ".fai")
	if err != nil {
		log.Fatal("Cannot create file", err)
	}
	defer file.Close()

	w := bufio.NewWriter(file)
	for i := 0; i < len(FastaReader.faIndex); i++ {
		fmt.Fprintln(w, FastaReader.faIndex[i].Name+"\t"+strconv.FormatInt(FastaReader.faIndex[i].Length, 10)+"\t"+strconv.FormatInt(FastaReader.faIndex[i].Offset, 10)+"\t"+strconv.FormatInt(FastaReader.faIndex[i].Linebases, 10)+"\t"+strconv.FormatInt(FastaReader.faIndex[i].Linewidth, 10))
	}
	w.Flush()
}

func stringToInt64(str string) (int64, error) {
	i, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		return 0, err
	}
	return int64(i), nil
}

func (FastaReader *FastaReader) getOffset(name string) (int64, error) {
	for i := 0; i < len(FastaReader.faIndex); i++ {
		if FastaReader.faIndex[i].Name == name {
			return FastaReader.faIndex[i].Offset, nil
		}
	}
	return 0, fmt.Errorf("not found name of this reference index")
}

func (FastaReader *FastaReader) GetLength(name string) (int64, error) {
	for i := 0; i < len(FastaReader.faIndex); i++ {
		if FastaReader.faIndex[i].Name == name {
			return FastaReader.faIndex[i].Length, nil
		}
	}
	return 0, fmt.Errorf("not found name of this reference index")
}

func (FastaReader *FastaReader) getLinebases(name string) (int, error) {
	for i := 0; i < len(FastaReader.faIndex); i++ {
		if FastaReader.faIndex[i].Name == name {
			return int(FastaReader.faIndex[i].Linebases), nil
		}
	}
	return 0, fmt.Errorf("not found name of this reference index")
}

func (FastaReader *FastaReader) getLinewidth(name string) (int64, error) {
	for i := 0; i < len(FastaReader.faIndex); i++ {
		if FastaReader.faIndex[i].Name == name {
			return FastaReader.faIndex[i].Linewidth, nil
		}
	}
	return 0, fmt.Errorf("not found name of this reference index")
}
