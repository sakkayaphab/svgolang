# fastalib (Fasta library for go)
[![GoDoc](https://img.shields.io/badge/go-documentation-blue.svg)](https://godoc.org/github.com/sakkayaphab/fastalib)
[![License](https://img.shields.io/packagist/l/doctrine/orm.svg)](https://github.com/sakkayaphab/bolt/blob/master/LICENSE)
[![Go Report Card](https://goreportcard.com/badge/github.com/sakkayaphab/fastalib)](https://goreportcard.com/report/github.com/sakkayaphab/fastalib)
[![Build Status](https://travis-ci.org/sakkayaphab/fastalib.svg?branch=master)](https://travis-ci.org/sakkayaphab/fastalib)

a library for parsing FASTA file in Golang. This library supports fast random access by calculation of offset and index.


## Features
- Fast random access
- Support generating index (faidx)



## Installation

```sh
go get github.com/sakkayaphab/fastalib
```


## License

[MIT](LICENSE)


[Go environment]: https://golang.org/doc/install
[Bolt release page]: https://golang.org/doc/install

